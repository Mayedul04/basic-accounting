﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.AppCodes;

namespace BasicAccounts.AppCodes
{
    class Group
    {
        private int _groupID;
        private int _parentGroupID;
        private int _natureID;
        private string _cashFlowType;
        private string _groupName;
        private Boolean _affectonGrossProfit;
        private Boolean _status;

        Validations validation = new Validations();

        public int GroupID
        {
            get { return _groupID; }
            set { validation.RequiredValidation(value, "ID"); this._groupID = value; }
        }
        public int ParentGroupID
        {
            get { return _parentGroupID; }
            set { validation.RequiredValidation(value, "Parent ID"); this._parentGroupID = value; }
        }

        public int NatureID
        {
            get { return _natureID; }
            set { validation.RequiredValidation(value, "Nature "); this._natureID = value; }
        }

        public string CashFlowType
        {
            get { return _cashFlowType; }
            set { validation.RequiredValidation(value, "Cash Flow Type"); this._cashFlowType = value; }
        }

        public string GroupName
        {
            get { return _groupName; }
            set
            {
                validation.RequiredValidation(value, "Group Name");
                this._groupName = value;
            }
        }
        public Boolean AffectonGrossProfit
        {
            get { return _affectonGrossProfit; }
            set
            {
                if (value)
                {
                    _affectonGrossProfit = value;
                }
                else _affectonGrossProfit = false;
            }
        }
       
        public Boolean Status
        {
            get { return _status; }
            set
            {
                if (value)
                {
                    _status = value;
                }
                else _status = true;
            }
        }
    }
}
