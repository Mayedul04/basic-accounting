﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.AppCodes;

namespace BasicAccounts.AppCodes
{
    class Voucher
    {
        private Int64 _voucherID;
        private int _voucherType;
        private string _voucherNo;
        private DateTime _postingDate;
        private string _narration;

        Validations validation = new Validations();

        public Int64 VoucherID
        {
            get { return this._voucherID; }
            set
            {
                validation.RequiredValidation(value, "Voucher ID ");
                this._voucherID = value;
            }
        }

        public int VoucherType
        {
            get { return this._voucherType; }
            set
            {
                validation.RequiredValidation(value, "Voucher Type ");
                this._voucherType = value;
            }
        }

        public string VoucherNo
        {
            get { return this._voucherNo; }
            set
            {
                validation.RequiredValidation(value, "Voucher No");
                this._voucherNo = value;
            }
        }

        public DateTime PostingDate
        {
            get { return this._postingDate; }
            set
            {
                validation.RequiredValidation(value, "Posting Date");
                this._postingDate = value;
            }
        }

        public string Narration
        {
            get { return this._narration; }
            set
            {
                this._narration = value;
            }
        }
    }
}
