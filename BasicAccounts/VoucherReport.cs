﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasicAccounts
{
    public partial class VoucherReport : Form
    {
        static string connetionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connetionString);
        private decimal debittotal = 0;
        private decimal credittotal = 0;
        private int TypeID;
        public VoucherReport(int typeid)
        {
            InitializeComponent();
            this.TypeID = typeid;
            if (TypeID == 1)
                lblVoucherType.Text = "Cash Payment Voucher Report";
            else if(TypeID==2)
                lblVoucherType.Text = "Bank Payment Voucher Report";
            else if(TypeID==3)
                lblVoucherType.Text = "Cash Receipt Voucher Report";
            else if(TypeID==4)
                lblVoucherType.Text = "Bank Receipt Voucher Report";
            else if(TypeID==5)
                lblVoucherType.Text = "Contra Voucher Report";
            else if(TypeID==6)
                lblVoucherType.Text = "Journal Report";
            else
                lblVoucherType.Text = "Voucher Report";
        }

        private void VoucherReport_Load(object sender, EventArgs e)
        {
            string postingdate="";
            string vouchernumber = "";
            decimal debitvalue = 0;
            decimal creditvalue = 0;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            sql = "Select PostingDate, VoucherNo, LedgerName, DebitAmount, CreditAmount from View_Voucher_Report Where(VoucherType=@VoucherType)";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("VoucherType", SqlDbType.Int, 32).Value = TypeID;
            SqlDataReader reader = adapter.SelectCommand.ExecuteReader();
            
            if (reader.HasRows)
            {
                
                while (reader.Read())
                {
                    debitvalue = decimal.Parse(reader["DebitAmount"].ToString());
                    creditvalue = decimal.Parse(reader["CreditAmount"].ToString());
                    if (reader["PostingDate"].ToString() != postingdate)
                    {
                        DataGridViewRow row = (DataGridViewRow)grdVoucherRepors.Rows[0].Clone();
                        //DataGridViewBand bandparent = row;
                        row.Cells[0].Value =DateTime.Parse(reader["PostingDate"].ToString()).ToString("dd-MM-yyyy");
                       /// grdVoucherRepors.Rows.Add(row);
                        if (reader["VoucherNo"].ToString() != vouchernumber)
                        {
                            row.Cells[1].Value = reader["VoucherNo"].ToString();
                            
                            grdVoucherRepors.Rows.Add(row);
                            
                            DataGridViewRow row2 = (DataGridViewRow)grdVoucherRepors.Rows[0].Clone();
                            row2.Cells[2].Value = reader["LedgerName"].ToString();
                            if (debitvalue > 0)
                            {
                                row2.Cells[3].Value = debitvalue;
                                row2.Cells[4].Value = "";
                            }
                            else
                            {
                                row2.Cells[3].Value = "";
                                row2.Cells[4].Value = creditvalue;
                            }
                            
                            grdVoucherRepors.Rows.Add(row2);
                        }
                        else
                        {
                            DataGridViewRow row1= (DataGridViewRow)grdVoucherRepors.Rows[0].Clone();
                            row1.Cells[2].Value = reader["LedgerName"].ToString();
                            if (debitvalue > 0)
                            {
                                row1.Cells[3].Value = debitvalue;
                                row1.Cells[4].Value = "";
                            }
                            else
                            {
                                row1.Cells[3].Value = "";
                                row1.Cells[4].Value = creditvalue;
                            }
                            grdVoucherRepors.Rows.Add(row1);
                        }
                    }
                    else
                    {
                        if (reader["VoucherNo"].ToString() != vouchernumber)
                        {
                            DataGridViewRow row = (DataGridViewRow)grdVoucherRepors.Rows[0].Clone();
                            row.Cells[1].Value = reader["VoucherNo"].ToString();
                            grdVoucherRepors.Rows.Add(row);

                            DataGridViewRow row2 = (DataGridViewRow)grdVoucherRepors.Rows[0].Clone();
                            row2.Cells[2].Value = reader["LedgerName"].ToString();
                            if (debitvalue > 0)
                            {
                                row2.Cells[3].Value = debitvalue;
                                row2.Cells[4].Value = "";
                            }
                            else
                            {
                                row2.Cells[3].Value = "";
                                row2.Cells[4].Value = creditvalue;
                            }
                            grdVoucherRepors.Rows.Add(row2);
                        }
                        else
                        {
                            DataGridViewRow row = (DataGridViewRow)grdVoucherRepors.Rows[0].Clone();
                            row.Cells[2].Value = reader["LedgerName"].ToString();
                            if (debitvalue > 0)
                            {
                                row.Cells[3].Value = debitvalue;
                                row.Cells[4].Value = "";
                            }
                            else
                            {
                                row.Cells[3].Value = "";
                                row.Cells[4].Value = creditvalue;
                            }
                            grdVoucherRepors.Rows.Add(row);
                        }
                    }
                    postingdate = reader["PostingDate"].ToString();
                    vouchernumber = reader["VoucherNo"].ToString();
                    debittotal += debitvalue;
                    credittotal += creditvalue;
                }
            }
            
            reader.Close();
            connection.Close();
            lblDebitTotal.Text = debittotal.ToString("0.00");
            lblCreditTotal.Text = credittotal.ToString("0.00");
            grdVoucherRepors.AllowUserToAddRows = false;
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            grdVoucherRepors.Rows.Clear();
            grdVoucherRepors.AllowUserToAddRows = true;
            debittotal = 0;
            credittotal = 0;
            string postingdate = "";
            string vouchernumber = "";
            decimal debitvalue = 0;
            decimal creditvalue = 0;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            sql = "Select PostingDate, VoucherNo, LedgerName, DebitAmount, CreditAmount from View_Voucher_Report Where(VoucherType=@VoucherType and PostingDate BETWEEN '" + dtpOpenningDate.Value.ToString("MM-dd-yyyy") + "' AND '" + dtpClosingDate.Value.ToString("MM-dd-yyyy") + "')";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("VoucherType", SqlDbType.Int, 32).Value = TypeID;
            SqlDataReader reader = adapter.SelectCommand.ExecuteReader();


            if (reader.HasRows)
            {

                while (reader.Read())
                {
                    debitvalue = decimal.Parse(reader["DebitAmount"].ToString());
                    creditvalue = decimal.Parse(reader["CreditAmount"].ToString());
                    if (reader["PostingDate"].ToString() != postingdate)
                    {
                        DataGridViewRow row = (DataGridViewRow)grdVoucherRepors.Rows[0].Clone();
                        //DataGridViewBand bandparent = row;
                        row.Cells[0].Value = DateTime.Parse(reader["PostingDate"].ToString()).ToString("dd-MM-yyyy");
                        /// grdVoucherRepors.Rows.Add(row);
                        if (reader["VoucherNo"].ToString() != vouchernumber)
                        {
                            row.Cells[1].Value = reader["VoucherNo"].ToString();
                            grdVoucherRepors.Rows.Add(row);

                            DataGridViewRow row2 = (DataGridViewRow)grdVoucherRepors.Rows[0].Clone();
                            row2.Cells[2].Value = reader["LedgerName"].ToString();
                            if (debitvalue > 0)
                            {
                                row2.Cells[3].Value = debitvalue;
                                row2.Cells[4].Value = "";
                            }
                            else
                            {
                                row2.Cells[3].Value = "";
                                row2.Cells[4].Value = creditvalue;
                            }

                            grdVoucherRepors.Rows.Add(row2);
                        }
                        else
                        {
                            DataGridViewRow row1 = (DataGridViewRow)grdVoucherRepors.Rows[0].Clone();
                            row1.Cells[2].Value = reader["LedgerName"].ToString();
                            if (debitvalue > 0)
                            {
                                row1.Cells[3].Value = debitvalue;
                                row1.Cells[4].Value = "";
                            }
                            else
                            {
                                row1.Cells[3].Value = "";
                                row1.Cells[4].Value = creditvalue;
                            }
                            grdVoucherRepors.Rows.Add(row1);
                        }
                    }
                    else
                    {
                        if (reader["VoucherNo"].ToString() != vouchernumber)
                        {
                            DataGridViewRow row = (DataGridViewRow)grdVoucherRepors.Rows[0].Clone();
                            row.Cells[1].Value = reader["VoucherNo"].ToString();
                            grdVoucherRepors.Rows.Add(row);

                            DataGridViewRow row2 = (DataGridViewRow)grdVoucherRepors.Rows[0].Clone();
                            row2.Cells[2].Value = reader["LedgerName"].ToString();
                            if (debitvalue > 0)
                            {
                                row2.Cells[3].Value = debitvalue;
                                row2.Cells[4].Value = "";
                            }
                            else
                            {
                                row2.Cells[3].Value = "";
                                row2.Cells[4].Value = creditvalue;
                            }
                            grdVoucherRepors.Rows.Add(row2);
                        }
                        else
                        {
                            DataGridViewRow row = (DataGridViewRow)grdVoucherRepors.Rows[0].Clone();
                            row.Cells[2].Value = reader["LedgerName"].ToString();
                            if (debitvalue > 0)
                            {
                                row.Cells[3].Value = debitvalue;
                                row.Cells[4].Value = "";
                            }
                            else
                            {
                                row.Cells[3].Value = "";
                                row.Cells[4].Value = creditvalue;
                            }
                            grdVoucherRepors.Rows.Add(row);
                        }
                    }
                    postingdate = reader["PostingDate"].ToString();
                    vouchernumber = reader["VoucherNo"].ToString();
                    debittotal += debitvalue;
                    credittotal += creditvalue;
                }
            }

            reader.Close();
            connection.Close();
            lblDebitTotal.Text = debittotal.ToString("0.00");
            lblCreditTotal.Text = credittotal.ToString("0.00");
            grdVoucherRepors.AllowUserToAddRows = false;
        }

        private void dtpOpenningDate_ValueChanged(object sender, EventArgs e)
        {
            dtpClosingDate.Value = dtpOpenningDate.Value.AddDays(+30);
        }
    }
}
