﻿using BasicAccounts.AppCodes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasicAccounts
{
    public partial class Profit_Loss : Form
    {
        static string connetionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connetionString);
        private string reporttype = "normal";
        private decimal debittotal = 0;
        private decimal credittotal = 0;
        private int subgroupcount = 0;
        private List<object> GropupLevel1 = new List<Object>();
        private List<object> GropupLevel2 = new List<Object>();
        private Boolean isLiabilities = true;

        private Decimal cDebitGTotal = 0;
        private Decimal cCreditGTotal = 0;
        public Profit_Loss()
        {
            InitializeComponent();
        }

        private void Profit_Loss_Load(object sender, EventArgs e)
        {
            int parentid = 0;
            string parentname = "";
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            #region Income
            sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID and NatureID=@NatureID";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = 1;
            adapter.SelectCommand.Parameters.Add("NatureID", SqlDbType.Int, 32).Value = 4;
            SqlDataReader reader1 = adapter.SelectCommand.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    parentname = reader1["GroupName"].ToString();
                    parentid = int.Parse(reader1["GroupID"].ToString());
                    Parent parent = new Parent();
                    parent.Id = parentid;
                    parent.Name = parentname;
                    GropupLevel1.Add(parent);
                }
            }

            connection.Close();
            foreach (Parent items in GropupLevel1)
            {
                LoadSubGroups(items, 2);
            }
            GropupLevel1.Clear();
            subgroupcount = 0;
            #endregion
            DataGridViewRow row2 = (DataGridViewRow)grdProfitLoss.Rows[0].Clone();
            row2.Cells[0].Value = "Grand Total";
            row2.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            row2.Cells[1].Value = credittotal;
            row2.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            grdProfitLoss.Rows.Add(row2);


            DataGridViewRow row1 = (DataGridViewRow)grdProfitLoss.Rows[0].Clone();
            row1.Cells[0].Value = "Expenses";
            row1.Cells[1].Value = "Amount";
            row1.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            row1.Cells[0].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            row1.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            row1.Cells[1].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdProfitLoss.Rows.Add(row1);

            #region Expenses
            sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID and NatureID=@NatureID";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = 1;
            adapter.SelectCommand.Parameters.Add("NatureID", SqlDbType.Int, 32).Value = 3;
            SqlDataReader reader2 = adapter.SelectCommand.ExecuteReader();
            if (reader2.HasRows)
            {
                while (reader2.Read())
                {
                    parentname = reader2["GroupName"].ToString();
                    parentid = int.Parse(reader2["GroupID"].ToString());
                    Parent parent = new Parent();
                    parent.Id = parentid;
                    parent.Name = parentname;
                    GropupLevel1.Add(parent);
                }
            }
            isLiabilities = false;
            connection.Close();
            foreach (Parent items in GropupLevel1)
            {
                LoadSubGroups(items, 2);
            }
            #endregion

            DataGridViewRow row = (DataGridViewRow)grdProfitLoss.Rows[0].Clone();
            row.Cells[0].Value = "Grand Total";
            row.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            row.Cells[1].Value = debittotal;
            row.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            grdProfitLoss.Rows.Add(row);

            grdProfitLoss.AllowUserToAddRows = false;
            if (debittotal - credittotal != 0)
                AddProfitLossBalance();
        }
        private void LoadSubGroups(Parent parent, int level)
        {

            int parentid = 0;
            string parentname = "";
            int i = 0;
            SqlDataAdapter adapter1 = new SqlDataAdapter();
            string sql = null;
            sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID";
            connection.Open();
            adapter1.SelectCommand = new SqlCommand(sql, connection);
            adapter1.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = parent.Id;
            SqlDataReader reader1 = adapter1.SelectCommand.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    parentname = reader1["GroupName"].ToString();
                    parentid = int.Parse(reader1["GroupID"].ToString());
                    Parent parent1 = new Parent();
                    parent1.Id = parentid;
                    parent1.Name = parentname;
                    GropupLevel2.Add(parent1);
                }
            }
            else
            {
                reader1.Close();
                connection.Close();
                LoadLedgers(parent, 1);
            }

            connection.Close();
            if (GropupLevel2.Count > 0)
            {
                if (grdProfitLoss.Rows.Count == 0 && reporttype != "normal")
                    this.grdProfitLoss.Rows.Add(parent.Name);
                else
                {
                    DataGridViewRow row1 = (DataGridViewRow)grdProfitLoss.Rows[0].Clone();
                    row1.Cells[0].Value = parent.Name;
                    grdProfitLoss.Rows.Add(row1);
                }
                subgroupcount = 0;
                foreach (Parent items in GropupLevel2)
                {
                    LoadLedgers(items, 2);

                }
                GropupLevel2.Clear();
            }
        }

        private void LoadLedgers(Parent parent, int level)
        {
            #region Load Data
            Decimal rowDebitTotal = 0;
            Decimal rowCreditTotal = 0;
            Decimal cDebitTotal = 0;
            Decimal cCreditTotal = 0;
            Decimal difference = 0;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            if (reporttype == "normal")
                sql = "Select TDebit,TCredit,LedgerName from  View_BalanceSheet Where (GroupID =@GroupID) and TCredit-TDebit!=0";
            else
            {
                sql = " SELECT  dbo.View_Periodic_Trail_Balance_2.LedgerName, dbo.View_Periodic_Trail_Balance_2.GroupID, dbo.View_Periodic_Trail_Balance_2.TDebit ,  dbo.View_Periodic_Trail_Balance_2.TCredit , dbo.Groups.NatureID ";
                sql += " FROM   dbo.Groups INNER JOIN dbo.View_Periodic_Trail_Balance_2 ON dbo.Groups.GroupID = dbo.View_Periodic_Trail_Balance_2.GroupID  Where (View_Periodic_Trail_Balance_2.GroupID =@GroupID) and TCredit- TDebit!=0";
            }
                
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("GroupID", SqlDbType.Int, 32).Value = parent.Id;
            SqlDataReader reader = adapter.SelectCommand.ExecuteReader();
            subgroupcount++;

            if (reader.HasRows)
            {
                if (grdProfitLoss.Rows.Count == 0 && reporttype != "normal")
                    this.grdProfitLoss.Rows.Add(parent.Name);
                else
                {
                    DataGridViewRow row1 = (DataGridViewRow)grdProfitLoss.Rows[0].Clone();
                    if (level == 2)
                        row1.Cells[0].Value = "     " + parent.Name;
                    else
                        row1.Cells[0].Value = parent.Name;
                    grdProfitLoss.Rows.Add(row1);
                }
                var loop = true;
                while (loop)
                {
                    loop = reader.Read();
                    if (loop)
                    {
                        DataGridViewRow row = (DataGridViewRow)grdProfitLoss.Rows[0].Clone();
                        row.Cells[0].Value = "          " + reader["LedgerName"].ToString();
                        rowDebitTotal = decimal.Parse(reader["TDebit"].ToString());
                        rowCreditTotal = decimal.Parse(reader["TCredit"].ToString());
                        if (isLiabilities == true)
                        {
                            if (rowCreditTotal > rowDebitTotal)
                            {
                                difference = rowCreditTotal - rowDebitTotal;
                                row.Cells[1].Value = difference;
                            }

                            else
                            {
                                difference = rowDebitTotal - rowCreditTotal;
                                if (difference != 0)
                                    row.Cells[1].Value = "(-)" + difference;
                                else
                                    row.Cells[1].Value = "";

                            }
                        }
                        else
                        {
                            if (rowDebitTotal > rowCreditTotal)
                            {
                                difference = rowDebitTotal - rowCreditTotal;
                                row.Cells[1].Value = difference;
                            }

                            else
                            {
                                difference = rowCreditTotal - rowDebitTotal;
                                if (difference != 0)
                                    row.Cells[1].Value = "(-)" + difference;
                                else
                                    row.Cells[1].Value = "";

                            }
                        }

                        grdProfitLoss.Rows.Add(row);

                        difference = 0;

                        cDebitTotal += rowDebitTotal;
                        cCreditTotal += rowCreditTotal;
                    }
                    else
                    {
                        DataGridViewRow row = (DataGridViewRow)grdProfitLoss.Rows[0].Clone();
                        if (level == 2)
                            row.Cells[0].Value = "    Sub Total :";
                        else
                            row.Cells[0].Value = "Group Total :";
                        row.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        row.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        if (isLiabilities == true)
                        {
                            if (cCreditTotal > cDebitTotal)
                            {
                                difference = cCreditTotal - cDebitTotal;
                                row.Cells[1].Value = difference;
                                credittotal += difference;
                            }

                            else
                            {
                                difference = cDebitTotal - cCreditTotal;
                                if (difference != 0)
                                    row.Cells[1].Value = "(-)" + difference;
                                else
                                    row.Cells[1].Value = "";
                                credittotal -= difference;
                            }
                            
                        }
                        else
                        {
                            if (cDebitTotal > cCreditTotal)
                            {
                                difference = cDebitTotal - cCreditTotal;
                                row.Cells[1].Value = difference;
                                debittotal += difference;
                            }

                            else
                            {
                                difference = cCreditTotal - cDebitTotal;
                                if (difference != 0)
                                    row.Cells[1].Value = "(-)" + difference;
                                else
                                    row.Cells[1].Value = "";
                                debittotal -= difference;
                            }
                            
                        }


                        grdProfitLoss.Rows.Add(row);

                        difference = 0;
                        if (level == 2)
                        {
                            cDebitGTotal += cDebitTotal;
                            cCreditGTotal += cCreditTotal;

                        }

                        //debittotal += cDebitTotal;
                        //credittotal += cCreditTotal;

                    }
                }

            }
            else
            {
                reader.Close();
                connection.Close();
                int rowcount = grdProfitLoss.Rows.Count;
                if (level == 2)  //This Dipest Group Don't have any ledger, so Remove
                {

                    if (GropupLevel2.Count == 1)
                    {
                        if (rowcount > 1)
                        {
                            grdProfitLoss.Rows.RemoveAt(rowcount - 2);
                        }
                    }

                }

            }
            if (GropupLevel2.Count > 1)
            {
                if (subgroupcount == GropupLevel2.Count)
                {
                    DataGridViewRow row2 = (DataGridViewRow)grdProfitLoss.Rows[1].Clone();
                    row2.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    row2.Cells[0].Value = "Group Total";
                    row2.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    if (isLiabilities == true)
                    {
                        if (cCreditGTotal > cDebitGTotal)
                        {
                            difference = cCreditGTotal - cDebitGTotal;
                            row2.Cells[1].Value = difference;
                        }

                        else
                        {
                            difference = cDebitGTotal - cCreditGTotal;
                            if (difference != 0)
                                row2.Cells[1].Value = "(-)" + difference;
                            else
                                row2.Cells[1].Value = "";
                        }
                        credittotal += difference;
                    }
                    else
                    {
                        if (cDebitGTotal > cCreditGTotal)
                        {
                            difference = cDebitGTotal - cCreditGTotal;
                            row2.Cells[1].Value = difference;
                        }

                        else
                        {
                            difference = cCreditGTotal - cDebitGTotal;
                            if (difference != 0)
                                row2.Cells[1].Value = "(-)" + difference;
                            else
                                row2.Cells[1].Value = "";
                        }
                        debittotal += difference;
                    }

                    grdProfitLoss.Rows.Add(row2);

                    difference = 0;
                    cDebitGTotal = 0;
                    cCreditGTotal = 0;
                }
            }


            connection.Close();
            #endregion
        }
        private  void AddProfitLossBalance()
        {
            try
            {
                Ledger ledger = new Ledger();
                ledger.LedgerID = 4023;
                Decimal openingvalue = debittotal - credittotal;
                if (openingvalue > 0)
                    ledger.OpeningDebit = openingvalue;
                else
                    ledger.OpeningCredit = credittotal-debittotal;
                

                SqlDataAdapter adapter = new SqlDataAdapter();
                string sql = null;
                sql = "Update  Ledgers set  OpeningDebit=@OpeningDebit, OpeningCredit=@OpeningCredit where LedgerID=@LedgerID";

                connection.Open();
                adapter.InsertCommand = new SqlCommand(sql, connection);
                adapter.InsertCommand.Parameters.Add("@LedgerID", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@LedgerID"].Value = ledger.LedgerID;
                adapter.InsertCommand.Parameters.Add("@OpeningDebit", SqlDbType.Real);
                adapter.InsertCommand.Parameters["@OpeningDebit"].Value = ledger.OpeningDebit;
                adapter.InsertCommand.Parameters.Add("@OpeningCredit", SqlDbType.Real);
                adapter.InsertCommand.Parameters["@OpeningCredit"].Value = ledger.OpeningCredit;
               
                adapter.InsertCommand.ExecuteNonQuery();
                connection.Close();
              //  MessageBox.Show("Ledger have been Updated !! ");
                // UpdateTreeView();
               

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                if (connection.State.ToString() == "open")
                {
                    connection.Close();
                }

            }
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            string sql2 = null;
            string sql3 = null;
            Main mainform = new Main();
            DateTime startperiod = mainform.getStartPeriod;
            DateTime startdate = DateTime.Parse(dtpOpenningDate.Value.ToString());
            
            sql2 += "ALTER View [View_Periodic_Trail_Balance_2] as ";
            sql2 += " Select dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, dbo.Ledgers.LedgerName, SUM(dbo.Voucher_Contents.DebitAmount) AS TDebit, SUM(dbo.Voucher_Contents.CreditAmount) AS TCredit, dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status, 'T' as TType ";
            sql2 += " FROM            dbo.Voucher_Contents INNER JOIN  dbo.Ledgers ON dbo.Voucher_Contents.LedgerID = dbo.Ledgers.LedgerID  ";
            sql2 += " WHERE        (dbo.Voucher_Contents.PostingDate BETWEEN '" + dtpOpenningDate.Value.ToString("yyyy-MM-dd") + "' AND '" + dtpClosingDate.Value.ToString("yyyy-MM-dd") + "') ";
            sql2 += " GROUP BY dbo.Ledgers.LedgerName,dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status, dbo.Ledgers.OpeningDebit,dbo.Ledgers.OpeningCredit  ";
            

            connection.Open();
            adapter.InsertCommand = new SqlCommand(sql2, connection);
            adapter.InsertCommand.ExecuteNonQuery();
            connection.Close();

            reporttype = "periodic";
            grdProfitLoss.Rows.Clear();
            GropupLevel1.Clear();
            GropupLevel2.Clear();
            debittotal = 0;
            credittotal = 0;
            int parentid = 0;
            string parentname = "";

            #region Income
            sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID and NatureID=@NatureID";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = 1;
            adapter.SelectCommand.Parameters.Add("NatureID", SqlDbType.Int, 32).Value = 4;
            SqlDataReader reader1 = adapter.SelectCommand.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    parentname = reader1["GroupName"].ToString();
                    parentid = int.Parse(reader1["GroupID"].ToString());
                    Parent parent = new Parent();
                    parent.Id = parentid;
                    parent.Name = parentname;
                    GropupLevel1.Add(parent);
                }
            }
            isLiabilities = true;
            connection.Close();
            foreach (Parent items in GropupLevel1)
            {
                LoadSubGroups(items, 2);
            }
            GropupLevel1.Clear();
           
            subgroupcount = 0;
            #endregion
            if (grdProfitLoss.RowCount != 0)
            {
                DataGridViewRow row2 = (DataGridViewRow)grdProfitLoss.Rows[0].Clone();
                row2.Cells[0].Value = "Grand Total";
                row2.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                row2.Cells[1].Value = credittotal;
                row2.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                grdProfitLoss.Rows.Add(row2);
            }

            if (grdProfitLoss.RowCount == 0)
            {
                grdProfitLoss.Rows.Add("Expenses","Amount");
                //grdProfitLoss.Rows[0].Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                //grdProfitLoss.Rows[0].Cells[0].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //grdProfitLoss.Rows[0].Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                //grdProfitLoss.Rows[0].Cells[1].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            else
            {
                DataGridViewRow row1 = (DataGridViewRow)grdProfitLoss.Rows[0].Clone();
                row1.Cells[0].Value = "Expenses";
                row1.Cells[1].Value = "Amount";
                row1.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                row1.Cells[0].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                row1.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                row1.Cells[1].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                grdProfitLoss.Rows.Add(row1);
            }
            
            #region Expenses
            sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID and NatureID=@NatureID";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = 1;
            adapter.SelectCommand.Parameters.Add("NatureID", SqlDbType.Int, 32).Value = 3;
            SqlDataReader reader2 = adapter.SelectCommand.ExecuteReader();
            if (reader2.HasRows)
            {
                while (reader2.Read())
                {
                    parentname = reader2["GroupName"].ToString();
                    parentid = int.Parse(reader2["GroupID"].ToString());
                    Parent parent = new Parent();
                    parent.Id = parentid;
                    parent.Name = parentname;
                    GropupLevel1.Add(parent);
                }
            }
            isLiabilities = false;
            connection.Close();
            foreach (Parent items in GropupLevel1)
            {
                LoadSubGroups(items, 2);
            }
            #endregion
            if (grdProfitLoss.RowCount != 0)
            {
                DataGridViewRow row = (DataGridViewRow)grdProfitLoss.Rows[0].Clone();
                row.Cells[0].Value = "Grand Total";
                row.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                row.Cells[1].Value = debittotal;
                row.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                grdProfitLoss.Rows.Add(row);
            }
                

            if (debittotal - credittotal != 0)
                AddProfitLossBalance();
            grdProfitLoss.AllowUserToAddRows = false;
        }

        private void dtpOpenningDate_ValueChanged(object sender, EventArgs e)
        {
            dtpClosingDate.Value = dtpOpenningDate.Value.AddDays(+30);
        }
    }
}
