﻿using BasicAccounts.AppCodes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasicAccounts
{
    public partial class Companies : Form
    {
        static string connetionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connetionString);
        public Companies()
        {
            InitializeComponent();
        }
        public static List<string> GetCountryList()
        {
            List<string> cultureList = new List<string>();

            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);


            foreach (CultureInfo culture in cultures)
            {
                RegionInfo region = new RegionInfo(culture.LCID);

                if (!(cultureList.Contains(region.EnglishName)))
                {
                    cultureList.Add(region.EnglishName);
                }
            }
            cultureList.Sort();
            cultureList.Insert(0, "Please select Country");
            return cultureList;
        }
        private void Companies_Load(object sender, EventArgs e)
        {
            UpdateCompanyList();
            SqlDataAdapter cmd1 = new SqlDataAdapter("Select FullName,CurrencyID from Currency where Status=1", connection);
            DataSet currency = new System.Data.DataSet();
            cmd1.Fill(currency, "Currency");
            DataTable dt1 = new DataTable();
            dt1 = currency.Tables[0];
            ddlCurrency.DataSource = dt1;

            sdsCountry.DataSource = GetCountryList();
            ddlCountry.DataSource = sdsCountry.DataSource;
        }

        private void ClearField()
        {
            txtCompanyID.Text = "";
            txtCompanyName.Text = "";
            txtMailingName.Text = "";
            txtCmpPhone.Text = "";
            txtCmpEmail.Text = "";
            txtCmpMobile.Text = "";
            txtCmpAddress.Text = "";
            ddlCurrency.SelectedIndex = 0;
            ddlCountry.SelectedIndex = 0;
            dtpFinancialStart.Value = DateTime.Today;
            dtpBooksBeginning.Value = DateTime.Today;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearField();
        }

        private void UpdateCompanyList()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select * from Company where Status=1", connection);
            DataSet natures = new System.Data.DataSet();
            cmd.Fill(natures, "Company");
            DataTable dt = new DataTable();
            dt = natures.Tables[0];
            ListCompany.DataSource = dt;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Company comp = new Company();
                comp.CompanyName = txtCompanyName.Text;
                comp.MailingName = txtMailingName.Text;
                comp.Address = txtCmpAddress.Text;
                comp.EmailID = txtCmpEmail.Text;
                comp.Mobile = txtCmpMobile.Text;
                comp.Phone = txtCmpPhone.Text;
                comp.FinancialYearStart = dtpFinancialStart.Value;
                comp.BooksBeginning = dtpBooksBeginning.Value;
                comp.Country = ddlCountry.SelectedItem.ToString();
                comp.Currency = int.Parse(ddlCurrency.SelectedValue.ToString());
                comp.AutoBackUp = chkAutoBackup.Checked;
                comp.Status = chkCmpStatus.Checked;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string sql = null;
                sql = "INSERT INTO Company (CompanyName,Address,EmailID,Phone,Currency,Mobile,AutoBackUp,MailingName,Country,FinancialYearStart ,BooksBeginning,Status) values (@CompanyName,@Address,@EmailID,@Phone,@Currency,@Mobile,@AutoBackUp,@MailingName,@Country,@FinancialYearStart ,@BooksBeginning,@Status)";

                connection.Open();
                adapter.InsertCommand = new SqlCommand(sql, connection);
                adapter.InsertCommand.Parameters.Add("@Currency", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@Currency"].Value = comp.Currency;
                adapter.InsertCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar, 80);
                adapter.InsertCommand.Parameters["@CompanyName"].Value = comp.CompanyName;
                adapter.InsertCommand.Parameters.Add("@Address", SqlDbType.NVarChar, 250);
                adapter.InsertCommand.Parameters["@Address"].Value = comp.Address;
                adapter.InsertCommand.Parameters.Add("@EmailID", SqlDbType.NVarChar, 25);
                adapter.InsertCommand.Parameters["@EmailID"].Value = comp.EmailID;
                adapter.InsertCommand.Parameters.Add("@Phone", SqlDbType.NVarChar, 15);
                adapter.InsertCommand.Parameters["@Phone"].Value = comp.Phone;
                adapter.InsertCommand.Parameters.Add("@Mobile", SqlDbType.NVarChar, 15);
                adapter.InsertCommand.Parameters["@Mobile"].Value = comp.Mobile;
                adapter.InsertCommand.Parameters.Add("@MailingName", SqlDbType.NVarChar, 50);
                adapter.InsertCommand.Parameters["@MailingName"].Value = comp.MailingName;
                adapter.InsertCommand.Parameters.Add("@Country", SqlDbType.NVarChar, 50);
                adapter.InsertCommand.Parameters["@Country"].Value = comp.Country;
                adapter.InsertCommand.Parameters.Add("@FinancialYearStart", SqlDbType.Date);
                adapter.InsertCommand.Parameters["@FinancialYearStart"].Value = comp.FinancialYearStart;
                adapter.InsertCommand.Parameters.Add("@BooksBeginning", SqlDbType.Date);
                adapter.InsertCommand.Parameters["@BooksBeginning"].Value = comp.BooksBeginning;
                adapter.InsertCommand.Parameters.Add("@AutoBackUp", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@AutoBackUp"].Value = comp.AutoBackUp;
                adapter.InsertCommand.Parameters.Add("@Status", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@Status"].Value = comp.Status;
                adapter.InsertCommand.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("New Row Inserted !! ");
                UpdateCompanyList();
                ClearField();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                if (connection.State.ToString() == "open")
                {
                    connection.Close();
                }

            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Company comp = new Company();
                comp.CompanyID = int.Parse(txtCompanyID.Text);
                comp.CompanyName = txtCompanyName.Text;
                comp.MailingName = txtMailingName.Text;
                comp.Address = txtCmpAddress.Text;
                comp.EmailID = txtCmpEmail.Text;
                comp.Mobile = txtCmpMobile.Text;
                comp.Phone = txtCmpPhone.Text;
                comp.FinancialYearStart = dtpFinancialStart.Value;
                comp.BooksBeginning = dtpBooksBeginning.Value;
                comp.Country = ddlCountry.SelectedItem.ToString();
                comp.Currency = int.Parse(ddlCurrency.SelectedValue.ToString());
                comp.AutoBackUp = chkAutoBackup.Checked;
                comp.Status = chkCmpStatus.Checked;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string sql = null;
                sql = "Update  Company set CompanyName=@CompanyName,Address=@Address,EmailID=@EmailID,Phone=@Phone,Currency=@Currency,Mobile=@Mobile,AutoBackUp=@AutoBackUp,MailingName=@MailingName,Country=@Country,FinancialYearStart=@FinancialYearStart,BooksBeginning=@BooksBeginning,Status=@Status where CompanyID=@CompanyID";

                connection.Open();
                adapter.InsertCommand = new SqlCommand(sql, connection);
                adapter.InsertCommand.Parameters.Add("@CompanyID", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@CompanyID"].Value = comp.CompanyID;
                adapter.InsertCommand.Parameters.Add("@Currency", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@Currency"].Value = comp.Currency;
                adapter.InsertCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar, 80);
                adapter.InsertCommand.Parameters["@CompanyName"].Value = comp.CompanyName;
                adapter.InsertCommand.Parameters.Add("@Address", SqlDbType.NVarChar, 250);
                adapter.InsertCommand.Parameters["@Address"].Value = comp.Address;
                adapter.InsertCommand.Parameters.Add("@EmailID", SqlDbType.NVarChar, 25);
                adapter.InsertCommand.Parameters["@EmailID"].Value = comp.EmailID;
                adapter.InsertCommand.Parameters.Add("@Phone", SqlDbType.NVarChar, 15);
                adapter.InsertCommand.Parameters["@Phone"].Value = comp.Phone;
                adapter.InsertCommand.Parameters.Add("@Mobile", SqlDbType.NVarChar, 15);
                adapter.InsertCommand.Parameters["@Mobile"].Value = comp.Mobile;
                adapter.InsertCommand.Parameters.Add("@MailingName", SqlDbType.NVarChar, 50);
                adapter.InsertCommand.Parameters["@MailingName"].Value = comp.MailingName;
                adapter.InsertCommand.Parameters.Add("@Country", SqlDbType.NVarChar, 50);
                adapter.InsertCommand.Parameters["@Country"].Value = comp.Country;
                adapter.InsertCommand.Parameters.Add("@FinancialYearStart", SqlDbType.Date);
                adapter.InsertCommand.Parameters["@FinancialYearStart"].Value = comp.FinancialYearStart;
                adapter.InsertCommand.Parameters.Add("@BooksBeginning", SqlDbType.Date);
                adapter.InsertCommand.Parameters["@BooksBeginning"].Value = comp.BooksBeginning;
                adapter.InsertCommand.Parameters.Add("@AutoBackUp", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@AutoBackUp"].Value = comp.AutoBackUp;
                adapter.InsertCommand.Parameters.Add("@Status", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@Status"].Value = comp.Status;
                adapter.InsertCommand.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Information is Updated !! ");
                // UpdateTreeView();
                UpdateCompanyList();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                if (connection.State.ToString() == "open")
                {
                    connection.Close();
                }

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do You Want to Delete?", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result.Equals(DialogResult.OK))
            {
                try
                {
                    Company comp = new Company();
                    comp.CompanyID = int.Parse(txtCompanyID.Text);
                    comp.Status = chkCmpStatus.Checked;
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    string sql = null;
                    sql = "Update Company set Status=@Status where CompanyID=@CompanyID";

                    connection.Open();
                    adapter.InsertCommand = new SqlCommand(sql, connection);
                    adapter.InsertCommand.Parameters.Add("@CompanyID", SqlDbType.Int, 32);
                    adapter.InsertCommand.Parameters["@CompanyID"].Value = comp.CompanyID;
                    adapter.InsertCommand.Parameters.Add("@Status", SqlDbType.Bit);
                    adapter.InsertCommand.Parameters["@Status"].Value = false;
                    adapter.InsertCommand.ExecuteNonQuery();
                    connection.Close();
                    MessageBox.Show("Company have been Deleted !! ");
                    // UpdateTreeView();
                    UpdateCompanyList();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                    if (connection.State.ToString() == "open")
                    {
                        connection.Close();
                    }

                }
            }
        }

        private void ListCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListCompany.SelectedItem != null)
            {
                Company comp = new Company();
                comp.CompanyID = (Int32.Parse((ListCompany.SelectedItem as DataRowView)["CompanyID"].ToString()));
                comp.Currency = (Int32.Parse((ListCompany.SelectedItem as DataRowView)["Currency"].ToString()));
                comp.FinancialYearStart = (DateTime.Parse((ListCompany.SelectedItem as DataRowView)["FinancialYearStart"].ToString()));
                comp.BooksBeginning = (DateTime.Parse((ListCompany.SelectedItem as DataRowView)["BooksBeginning"].ToString()));
                comp.Country = ((ListCompany.SelectedItem as DataRowView)["Country"].ToString());
                comp.Address = ((ListCompany.SelectedItem as DataRowView)["Address"].ToString());
                comp.MailingName = ((ListCompany.SelectedItem as DataRowView)["MailingName"].ToString());
                comp.Mobile = ((ListCompany.SelectedItem as DataRowView)["Mobile"].ToString().Trim());
                comp.CompanyName = ((ListCompany.SelectedItem as DataRowView)["CompanyName"].ToString());
                comp.Phone = ((ListCompany.SelectedItem as DataRowView)["Phone"].ToString().Trim());
                comp.EmailID = ((ListCompany.SelectedItem as DataRowView)["EmailID"].ToString());
                comp.AutoBackUp = (Boolean.Parse((ListCompany.SelectedItem as DataRowView)["AutoBackUp"].ToString()));
                comp.Status = (Boolean.Parse((ListCompany.SelectedItem as DataRowView)["Status"].ToString()));


                txtCompanyID.Text = comp.CompanyID.ToString();
                txtCompanyName.Text = comp.CompanyName;
                txtMailingName.Text = comp.MailingName;
                txtCmpAddress.Text = comp.Address;
                ddlCountry.SelectedItem = comp.Country;
                ddlCurrency.SelectedValue = comp.Currency;
                dtpFinancialStart.Text = comp.FinancialYearStart.ToString();
                dtpBooksBeginning.Text = comp.BooksBeginning.ToString();
                txtCmpEmail.Text = comp.EmailID;
                txtCmpMobile.Text = comp.Mobile;
                txtCmpPhone.Text = comp.Phone;
                chkAutoBackup.Checked = comp.AutoBackUp;
                chkCmpStatus.Checked = comp.Status;

            }
        }
    }
}
