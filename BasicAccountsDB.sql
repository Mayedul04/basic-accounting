USE [master]
GO
/****** Object:  Database [BasicAccounts]    Script Date: 1/28/2017 11:58:30 PM ******/
CREATE DATABASE [BasicAccounts]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BasicAccounts', FILENAME = N'C:\Users\User\BasicAccounts.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BasicAccounts_log', FILENAME = N'C:\Users\User\BasicAccounts_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BasicAccounts] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BasicAccounts].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BasicAccounts] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BasicAccounts] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BasicAccounts] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BasicAccounts] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BasicAccounts] SET ARITHABORT OFF 
GO
ALTER DATABASE [BasicAccounts] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BasicAccounts] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [BasicAccounts] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BasicAccounts] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BasicAccounts] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BasicAccounts] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BasicAccounts] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BasicAccounts] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BasicAccounts] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BasicAccounts] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BasicAccounts] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BasicAccounts] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BasicAccounts] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BasicAccounts] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BasicAccounts] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BasicAccounts] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BasicAccounts] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BasicAccounts] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BasicAccounts] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BasicAccounts] SET  MULTI_USER 
GO
ALTER DATABASE [BasicAccounts] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BasicAccounts] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BasicAccounts] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BasicAccounts] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [BasicAccounts]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 1/28/2017 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[CompanyID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [nvarchar](100) NULL,
	[Address] [nvarchar](250) NULL,
	[Phone] [nchar](15) NULL,
	[EmailID] [nvarchar](25) NULL,
	[Mobile] [nchar](15) NULL,
	[MailingName] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[Currency] [int] NULL,
	[FinancialYearStart] [date] NULL,
	[BooksBeginning] [date] NULL,
	[AutoBackUp] [bit] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[CompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Currency]    Script Date: 1/28/2017 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[CurrencyID] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](25) NULL,
	[Symbol] [nchar](10) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[CurrencyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Groups]    Script Date: 1/28/2017 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Groups](
	[GroupID] [int] IDENTITY(1,1) NOT NULL,
	[ParentGroupID] [int] NOT NULL,
	[NatureID] [int] NOT NULL,
	[GroupName] [nvarchar](100) NULL,
	[CashFlowType] [nvarchar](150) NULL,
	[AffectonGrossProfit] [bit] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Groups] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ledgers]    Script Date: 1/28/2017 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ledgers](
	[LedgerID] [int] IDENTITY(1,1) NOT NULL,
	[GroupID] [int] NOT NULL,
	[LedgerName] [nvarchar](100) NULL,
	[OpeningCredit] [real] NULL,
	[OpeningDebit] [real] NULL,
	[CostCentreApplicable] [bit] NULL,
	[InventoryEnabled] [bit] NULL,
	[MaintainBalanceBill] [bit] NULL,
	[ServiceAffect] [bit] NULL,
	[FixedAssetAffect] [bit] NULL,
	[IsRecievable] [bit] NULL,
	[CreditLimit] [real] NULL,
	[ContactPerson] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[Email] [nvarchar](25) NULL,
	[Address] [nvarchar](150) NULL,
	[Phone] [nchar](15) NULL,
	[AddDate] [date] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Ledgers] PRIMARY KEY CLUSTERED 
(
	[LedgerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Natures]    Script Date: 1/28/2017 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Natures](
	[NatureID] [int] IDENTITY(1,1) NOT NULL,
	[Nature] [nvarchar](50) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Natures] PRIMARY KEY CLUSTERED 
(
	[NatureID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PresentPeriod]    Script Date: 1/28/2017 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PresentPeriod](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
 CONSTRAINT [PK_PresentPeriod] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Voucher_Contents]    Script Date: 1/28/2017 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Voucher_Contents](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[VoucherNo] [nvarchar](50) NULL,
	[LedgerID] [int] NULL,
	[VoucherType] [int] NULL,
	[DebitAmount] [decimal](18, 2) NULL,
	[CreditAmount] [decimal](18, 2) NULL,
	[OpenningLDebit] [decimal](18, 2) NULL,
	[OpenningLCredit] [decimal](18, 2) NULL,
	[PostingDate] [date] NULL,
 CONSTRAINT [PK_Voucher_Contents] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Vouchers]    Script Date: 1/28/2017 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vouchers](
	[VoucherID] [bigint] IDENTITY(1,1) NOT NULL,
	[VoucherType] [int] NOT NULL,
	[VoucherNo] [nvarchar](50) NULL,
	[PostingDate] [datetime] NULL,
	[Narration] [nvarchar](250) NULL,
 CONSTRAINT [PK_Vouchers] PRIMARY KEY CLUSTERED 
(
	[VoucherID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VoucherTypes]    Script Date: 1/28/2017 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VoucherTypes](
	[VTypeID] [int] IDENTITY(1,1) NOT NULL,
	[VoucherType] [nvarchar](15) NULL,
	[Prefix] [nvarchar](5) NULL,
	[Length] [int] NULL,
 CONSTRAINT [PK_VoucherTypes] PRIMARY KEY CLUSTERED 
(
	[VTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[View_TrialBalance_Initial]    Script Date: 1/28/2017 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_TrialBalance_Initial]
AS
SELECT        dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, dbo.Ledgers.LedgerID, dbo.Ledgers.LedgerName, SUM(dbo.Voucher_Contents.DebitAmount) AS TDebit, 
                         SUM(dbo.Voucher_Contents.CreditAmount) AS TCredit, dbo.Ledgers.GroupID, dbo.Ledgers.Status, 'T' AS TType
FROM            dbo.Ledgers INNER JOIN
                         dbo.Voucher_Contents ON dbo.Ledgers.LedgerID = dbo.Voucher_Contents.LedgerID
GROUP BY dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, dbo.Ledgers.LedgerName, dbo.Ledgers.GroupID, dbo.Ledgers.LedgerID, dbo.Ledgers.Status
HAVING        (dbo.Ledgers.Status = 1)

GO
/****** Object:  View [dbo].[View_BalanceSheet]    Script Date: 1/28/2017 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_BalanceSheet]
AS
SELECT        OpeningDebit + TDebit AS ClosingDebit, OpeningCredit + TCredit AS ClosingCredit, LedgerName, GroupID, TDebit, TCredit
FROM            dbo.View_TrialBalance_Initial

GO
/****** Object:  View [dbo].[View_Closing_Balance]    Script Date: 1/28/2017 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[View_Closing_Balance] as  SELECT dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, dbo.Ledgers.LedgerName, SUM(TempVoucher.DebitAmount) AS TDebit, SUM(TempVoucher.CreditAmount) AS TCredit, dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status FROM  (SELECT  dbo.Voucher_Contents.VoucherNo, dbo.Voucher_Contents.DebitAmount, dbo.Voucher_Contents.CreditAmount, dbo.Voucher_Contents.LedgerID   FROM   dbo.Voucher_Contents INNER JOIN dbo.Vouchers ON dbo.Voucher_Contents.VoucherNo = dbo.Vouchers.VoucherNo  WHERE ( dbo.Vouchers.PostingDate <='2016-07-12')) AS TempVoucher INNER JOIN dbo.Ledgers  ON dbo.Ledgers.LedgerID = TempVoucher.LedgerID    GROUP BY dbo.Ledgers.LedgerName, dbo.Ledgers.LedgerID, dbo.Ledgers.Status, dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, dbo.Ledgers.GroupID
GO
/****** Object:  View [dbo].[View_Ledger_Report]    Script Date: 1/28/2017 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Ledger_Report]
AS
SELECT        dbo.Voucher_Contents.VoucherNo, dbo.Voucher_Contents.DebitAmount, dbo.Voucher_Contents.CreditAmount, dbo.Voucher_Contents.PostingDate, 
                         dbo.Ledgers.LedgerName, Ledgers_1.LedgerName AS OpositeLedger, Voucher_Contents_1.DebitAmount AS OpositeDebit, 
                         Voucher_Contents_1.CreditAmount AS OpositeCredit, dbo.Voucher_Contents.LedgerID, Ledgers_1.LedgerID AS OpositeLedgerId, 
                         dbo.VoucherTypes.VoucherType
FROM            dbo.Voucher_Contents INNER JOIN
                         dbo.Ledgers ON dbo.Voucher_Contents.LedgerID = dbo.Ledgers.LedgerID INNER JOIN
                         dbo.Voucher_Contents AS Voucher_Contents_1 ON dbo.Voucher_Contents.VoucherNo = Voucher_Contents_1.VoucherNo INNER JOIN
                         dbo.Ledgers AS Ledgers_1 ON Voucher_Contents_1.LedgerID = Ledgers_1.LedgerID INNER JOIN
                         dbo.VoucherTypes ON dbo.Voucher_Contents.VoucherType = dbo.VoucherTypes.VTypeID

GO
/****** Object:  View [dbo].[View_Periodic_Trail_Balance_2]    Script Date: 1/28/2017 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[View_Periodic_Trail_Balance_2] as  Select dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, dbo.Ledgers.LedgerName, SUM(dbo.Voucher_Contents.DebitAmount) AS TDebit, SUM(dbo.Voucher_Contents.CreditAmount) AS TCredit, dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status, 'T' as TType  FROM            dbo.Voucher_Contents INNER JOIN  dbo.Ledgers ON dbo.Voucher_Contents.LedgerID = dbo.Ledgers.LedgerID   WHERE        (dbo.Voucher_Contents.PostingDate BETWEEN '2016-01-07' AND '2016-07-12')  GROUP BY dbo.Ledgers.LedgerName,dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status, dbo.Ledgers.OpeningDebit,dbo.Ledgers.OpeningCredit  
GO
/****** Object:  View [dbo].[View_Voucher_Report]    Script Date: 1/28/2017 11:58:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Voucher_Report]
AS
SELECT        dbo.Vouchers.PostingDate, dbo.Vouchers.VoucherNo, dbo.Voucher_Contents.DebitAmount, dbo.Voucher_Contents.CreditAmount, dbo.Voucher_Contents.VoucherType, 
                         dbo.Ledgers.LedgerName
FROM            dbo.Ledgers INNER JOIN
                         dbo.Voucher_Contents ON dbo.Ledgers.LedgerID = dbo.Voucher_Contents.LedgerID INNER JOIN
                         dbo.Vouchers ON dbo.Voucher_Contents.VoucherNo = dbo.Vouchers.VoucherNo

GO
ALTER TABLE [dbo].[Ledgers]  WITH CHECK ADD  CONSTRAINT [FK_Ledgers_Groups] FOREIGN KEY([GroupID])
REFERENCES [dbo].[Groups] ([GroupID])
GO
ALTER TABLE [dbo].[Ledgers] CHECK CONSTRAINT [FK_Ledgers_Groups]
GO
ALTER TABLE [dbo].[Vouchers]  WITH CHECK ADD  CONSTRAINT [FK_Vouchers_VoucherTypes] FOREIGN KEY([VoucherType])
REFERENCES [dbo].[VoucherTypes] ([VTypeID])
GO
ALTER TABLE [dbo].[Vouchers] CHECK CONSTRAINT [FK_Vouchers_VoucherTypes]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "View_TrialBalance_Initial"
            Begin Extent = 
               Top = 0
               Left = 97
               Bottom = 203
               Right = 267
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 2685
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_BalanceSheet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_BalanceSheet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TempVoucher"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 224
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Ledgers"
            Begin Extent = 
               Top = 6
               Left = 262
               Bottom = 135
               Right = 482
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Closing_Balance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Closing_Balance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Voucher_Contents"
            Begin Extent = 
               Top = 9
               Left = 301
               Bottom = 184
               Right = 481
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Ledgers"
            Begin Extent = 
               Top = 4
               Left = 19
               Bottom = 196
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Voucher_Contents_1"
            Begin Extent = 
               Top = 2
               Left = 593
               Bottom = 179
               Right = 773
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Ledgers_1"
            Begin Extent = 
               Top = 9
               Left = 835
               Bottom = 189
               Right = 1039
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "VoucherTypes"
            Begin Extent = 
               Top = 91
               Left = 506
               Bottom = 220
               Right = 676
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin Criteria' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Ledger_Report'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'Pane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Ledger_Report'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Ledger_Report'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Ledgers"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Voucher_Contents"
            Begin Extent = 
               Top = 6
               Left = 296
               Bottom = 135
               Right = 492
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_TrialBalance_Initial'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_TrialBalance_Initial'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[15] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Vouchers"
            Begin Extent = 
               Top = 8
               Left = 12
               Bottom = 178
               Right = 182
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Voucher_Contents"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 187
               Right = 426
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "Ledgers"
            Begin Extent = 
               Top = 1
               Left = 576
               Bottom = 130
               Right = 780
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Voucher_Report'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Voucher_Report'
GO
USE [master]
GO
ALTER DATABASE [BasicAccounts] SET  READ_WRITE 
GO
