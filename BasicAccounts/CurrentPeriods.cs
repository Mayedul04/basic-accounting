﻿using BasicAccounts.AppCodes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasicAccounts
{
    public partial class CurrentPeriods : Form
    {
        static string connetionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connetionString);
        public CurrentPeriods()
        {
            InitializeComponent();
        }

        
        private void Nature_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
                
            }

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                CurrentPeriod period = new CurrentPeriod();
                period.StartDate = dtpStartDate.Value;
                period.EndDate = dtpEndDate.Value;
                
                SqlDataAdapter adapter = new SqlDataAdapter();
                string sql = null;
                sql = "Update  PresentPeriod set StartDate=@StartDate,EndDate=@EndDate where ID=@ID";

                connection.Open();
                adapter.InsertCommand = new SqlCommand(sql, connection);
                adapter.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@ID"].Value = 1;
                adapter.InsertCommand.Parameters.Add("@StartDate", SqlDbType.Date);
                adapter.InsertCommand.Parameters["@StartDate"].Value = period.StartDate;
                adapter.InsertCommand.Parameters.Add("@EndDate", SqlDbType.Date);
                adapter.InsertCommand.Parameters["@EndDate"].Value = period.EndDate;
                adapter.InsertCommand.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Current Period has been  Updated !! ");
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                if (connection.State.ToString() == "open")
                {
                    connection.Close();
                }

            }
        }

        private void CurrentPeriods_Load(object sender, EventArgs e)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            sql = "Select StartDate,EndDate  from  PresentPeriod where  ID=@ID";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("ID", SqlDbType.NVarChar, 50).Value = 1;
            SqlDataReader reader = adapter.SelectCommand.ExecuteReader();
            CurrentPeriod period = new CurrentPeriod();
            
            while (reader.Read())
            {
                period.StartDate = DateTime.Parse(reader["StartDate"].ToString());
                period.EndDate = DateTime.Parse(reader["EndDate"].ToString());
            }
            connection.Close();
            dtpStartDate.Value = period.StartDate;
            dtpEndDate.Value = period.EndDate;
         }
    }
}
