﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.AppCodes;

namespace BasicAccounts.AppCodes
{
    class VoucherItem
    {
        private Int64 _entryID;
        private int _ledgerID;
        private int _voucherType;
        private decimal _debitAmount;
        private decimal _creditAmount;
        private string _voucherNo;
        
        private DateTime _postingDate;

        Validations validation = new Validations();

        public Int64 EntryID
        {
            get { return this._entryID; }
            set
            {
                validation.RequiredValidation(value, "ID ");
                this._entryID = value;
            }
        }

        public int LedgerID
        {
            get { return this._ledgerID; }
            set
            {
                validation.RequiredValidation(value, "Ledger ");
                this._ledgerID = value;
            }
        }
        public int VoucherType
        {
            get { return this._voucherType; }
            set
            {
                validation.RequiredValidation(value, "Voucher Type ");
                this._voucherType = value;
            }
        }
        public decimal DebitAmount
        {
            get { return this._debitAmount; }
            set
            {
                validation.ValidedDecimal(value.ToString());
                this._debitAmount = value;
            }
        }

        public decimal CreditAmount
        {
            get { return this._creditAmount; }
            set
            {
                validation.ValidedDecimal(value.ToString());
                this._creditAmount = value;
            }
        }

        

        public DateTime PostingDate
        {
            get { return this._postingDate; }
            set
            {
                validation.RequiredValidation(value, "Posting Date");
                this._postingDate = value;
            }
        }
        public string VoucherNo
        {
            get { return this._voucherNo; }
            set
            {
                validation.RequiredValidation(value, "Voucher No ");
                this._voucherNo = value;
            }
        }
    }
}
