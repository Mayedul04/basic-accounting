﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasicAccounts
{
    public partial class PeriodicLedger : Form
    {
        static string connetionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connetionString);
        private decimal debittotal = 0;
        private decimal credittotal = 0;
        private int LedgerID;
        private DateTime StartDate;
        private DateTime EndDate;
        public PeriodicLedger(int ledgerid,DateTime startdate,DateTime enddate)
        {
            InitializeComponent();
            StartDate = startdate;
            EndDate = enddate;
            LedgerID = ledgerid;
            
        }
        private void PeriodicLedger_Load(object sender, EventArgs e)
        {
            ListLedger.SelectedIndexChanged -= new EventHandler(ListLedger_SelectedIndexChanged);
            SqlDataAdapter cmd = new SqlDataAdapter("Select LedgerName,LedgerID from Ledgers where  Status=1", connection);
            DataSet ledgers = new System.Data.DataSet();
            cmd.Fill(ledgers, "Ledgers");
            DataTable dt2 = new DataTable();
            dt2 = ledgers.Tables[0];
            ListLedger.DataSource = dt2;
            ListLedger.SelectedIndexChanged += new EventHandler(ListLedger_SelectedIndexChanged);

            dtpOpenningDate.Value = StartDate;
            dtpClosingDate.Value = EndDate;
            LoadLedgerReport(LedgerID);
            LoadOpeningBalance(LedgerID);
        }
        private void LoadLedgerReport(int ledgerid)
        {
            string postingdate = "";
            string vouchernumber = "";
            decimal debitvalue = 0;
            decimal creditvalue = 0;
            decimal oppdebit = 0;
            decimal oppcredit = 0;
            int oppledgercount = 0;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            sql = "Select PostingDate, VoucherNo, VoucherType, OpositeLedger, OpositeDebit, DebitAmount, CreditAmount, OpositeCredit from View_Ledger_Report Where (LedgerID=@LedgerID and OpositeLedgerId<>@LedgerID and PostingDate BETWEEN '" + dtpOpenningDate.Value.ToString("yyyy-MM-dd") + "' AND '" + dtpClosingDate.Value.ToString("yyyy-MM-dd") + "')";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("LedgerID", SqlDbType.Int, 32).Value = ledgerid;
            SqlDataReader reader = adapter.SelectCommand.ExecuteReader();


            if (reader.HasRows)
            {

                while (reader.Read())
                {
                    debitvalue = decimal.Parse(reader["DebitAmount"].ToString());
                    creditvalue = decimal.Parse(reader["CreditAmount"].ToString());
                    oppdebit = decimal.Parse(reader["OpositeDebit"].ToString());
                    oppcredit = decimal.Parse(reader["OpositeCredit"].ToString());
                    if (reader["PostingDate"].ToString() != postingdate)
                    {

                        if (reader["VoucherNo"].ToString() != vouchernumber)
                        {
                            //if (grdLedgerReport.Rows.Count == 0)
                            //    this.grdLedgerReport.Rows.Add(reader["PostingDate"].ToString());
                            DataGridViewRow row = (DataGridViewRow)grdLedgerReport.Rows[0].Clone();
                            //if (grdLedgerReport.Rows.Count == 2)
                            //    grdLedgerReport.Rows.RemoveAt(0);
                            row.Cells[0].Value = reader["PostingDate"].ToString();
                            row.Cells[1].Value = reader["VoucherNo"].ToString();
                            row.Cells[2].Value = reader["VoucherType"].ToString();


                            if (oppledgercount == 1)
                            {
                                int rowcount = grdLedgerReport.Rows.Count;
                                grdLedgerReport.Rows[rowcount - 3].Cells[3].Value = grdLedgerReport.Rows[rowcount - 2].Cells[3].Value;
                                grdLedgerReport.Rows.RemoveAt(rowcount - 2);

                            }
                            row.Cells[3].Value = "(as per details)";
                            if (debitvalue > 0)
                            {
                                row.Cells[5].Value = debitvalue;
                                row.Cells[6].Value = "";
                            }
                            else
                            {
                                row.Cells[5].Value = "";
                                row.Cells[6].Value = creditvalue;
                            }
                            grdLedgerReport.Rows.Add(row);

                            oppledgercount = 0;
                            DataGridViewRow row2 = (DataGridViewRow)grdLedgerReport.Rows[0].Clone();
                            row2.Cells[3].Value = reader["OpositeLedger"].ToString();
                            row2.Cells[4].Value = (oppdebit > 0 ? oppdebit.ToString() + " Dr" : oppcredit.ToString() + " Cr");
                            grdLedgerReport.Rows.Add(row2);
                            oppledgercount++;

                            debittotal += debitvalue;
                            credittotal += creditvalue;


                        }
                        else
                        {
                            DataGridViewRow row1 = (DataGridViewRow)grdLedgerReport.Rows[0].Clone();
                            row1.Cells[3].Value = reader["OpositeLedger"].ToString();
                            row1.Cells[4].Value = (oppdebit > 0 ? oppdebit.ToString() + " Dr" : oppcredit.ToString() + " Cr");
                            grdLedgerReport.Rows.Add(row1);
                            oppledgercount++;
                        }

                    }
                    else
                    {
                        if (reader["VoucherNo"].ToString() != vouchernumber)
                        {
                            DataGridViewRow row = (DataGridViewRow)grdLedgerReport.Rows[0].Clone();
                            row.Cells[1].Value = reader["VoucherNo"].ToString();
                            row.Cells[2].Value = reader["VoucherType"].ToString();

                            if (oppledgercount == 1)
                            {
                                int rowcount = grdLedgerReport.Rows.Count;
                                grdLedgerReport.Rows[rowcount - 3].Cells[3].Value = grdLedgerReport.Rows[rowcount - 2].Cells[3].Value;
                                grdLedgerReport.Rows.RemoveAt(rowcount - 2);

                            }
                            row.Cells[3].Value = "(as per details)";
                            if (debitvalue > 0)
                            {
                                row.Cells[5].Value = debitvalue;
                                row.Cells[6].Value = "";
                            }
                            else
                            {
                                row.Cells[5].Value = "";
                                row.Cells[6].Value = creditvalue;
                            }
                            grdLedgerReport.Rows.Add(row);
                            oppledgercount = 0;
                            DataGridViewRow row2 = (DataGridViewRow)grdLedgerReport.Rows[0].Clone();
                            row2.Cells[3].Value = reader["OpositeLedger"].ToString();
                            row2.Cells[4].Value = (oppdebit > 0 ? oppdebit.ToString() + " Dr" : oppcredit.ToString() + " Cr");
                            grdLedgerReport.Rows.Add(row2);
                            oppledgercount++;

                            debittotal += debitvalue;
                            credittotal += creditvalue;


                        }
                        else
                        {
                            DataGridViewRow row1 = (DataGridViewRow)grdLedgerReport.Rows[0].Clone();
                            row1.Cells[3].Value = reader["OpositeLedger"].ToString();
                            row1.Cells[4].Value = (oppdebit > 0 ? oppdebit.ToString() + " Dr" : oppcredit.ToString() + " Cr");
                            grdLedgerReport.Rows.Add(row1);
                            oppledgercount++;
                        }

                    }
                    postingdate = reader["PostingDate"].ToString();
                    vouchernumber = reader["VoucherNo"].ToString();

                }
            }

            reader.Close();
            connection.Close();
            if (oppledgercount == 1)
            {
                int rowcount = grdLedgerReport.Rows.Count;
                grdLedgerReport.Rows[rowcount - 3].Cells[3].Value = grdLedgerReport.Rows[rowcount - 2].Cells[3].Value;
                grdLedgerReport.Rows.RemoveAt(rowcount - 2);
            }

            if (debittotal-credittotal > 0)
            {
                lblDebitTotal.Text = (debittotal - credittotal).ToString("0.00");
                lblCreditTotal.Text = "";
            }

            else
            {
                lblDebitTotal.Text = "";
                lblCreditTotal.Text = (credittotal - debittotal).ToString("0.00");
            }
            
            grdLedgerReport.AllowUserToAddRows = false;
            
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    grdLedgerReport.Rows.Clear();
        //    grdLedgerReport.AllowUserToAddRows = true;
        //    debittotal = 0;
        //    credittotal = 0;
        //    LoadLedgerReport(36);
        //    LoadOpeningBalance(36);

        //}
        private void LoadOpeningBalance(int ledgerid)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            Decimal openingdebit = 0;
            Decimal openingcredit = 0;
            Decimal trdebit = 0;
            Decimal trcredit = 0;
            Decimal closingdebit = 0;
            Decimal closingcredit = 0;
            decimal openingGdebit = 0;
            decimal openingGcredit = 0;
            decimal finalopeningdebit = 0;
            decimal finalopeningcredit = 0;
            Main mainform = new Main();
            DateTime startperiod = mainform.getStartPeriod;
            DateTime startdate = DateTime.Parse(dtpOpenningDate.Value.ToString());
            sql += " SELECT dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, dbo.Ledgers.LedgerName, SUM(TempVoucher.DebitAmount) AS TDebit, SUM(TempVoucher.CreditAmount) AS TCredit, dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status";
            sql += " FROM  (SELECT  dbo.Voucher_Contents.VoucherNo, dbo.Voucher_Contents.DebitAmount, dbo.Voucher_Contents.CreditAmount, dbo.Voucher_Contents.LedgerID ";
            sql += "  FROM   dbo.Voucher_Contents INNER JOIN dbo.Vouchers ON dbo.Voucher_Contents.VoucherNo = dbo.Vouchers.VoucherNo ";
            sql += " WHERE ( dbo.Vouchers.PostingDate <'" + dtpOpenningDate.Value.ToString("yyyy-MM-dd") + "')) AS TempVoucher INNER JOIN dbo.Ledgers  ON dbo.Ledgers.LedgerID = TempVoucher.LedgerID ";
            sql += " Where Ledgers.LedgerID=@LedgerID  GROUP BY dbo.Ledgers.LedgerName, dbo.Ledgers.LedgerID, dbo.Ledgers.Status, dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, dbo.Ledgers.GroupID";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("LedgerID", SqlDbType.Int, 32).Value = ledgerid;
            SqlDataReader reader = adapter.SelectCommand.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                if (reader["OpeningDebit"].ToString() != "")
                    openingdebit = decimal.Parse(reader["OpeningDebit"].ToString());
                if (reader["OpeningCredit"].ToString() != "")
                    openingcredit = decimal.Parse(reader["OpeningCredit"].ToString());
                trdebit = decimal.Parse(reader["TDebit"].ToString());
                trcredit = decimal.Parse(reader["TCredit"].ToString());
            }
            reader.Close();
            connection.Close();
            finalopeningdebit = openingdebit + trdebit;
            finalopeningcredit = openingcredit + trcredit;
            if (finalopeningdebit > finalopeningcredit)
            {
                openingGdebit = finalopeningdebit - finalopeningcredit;
                if (openingGdebit > 0)
                    lblOpeningDebit.Text = openingGdebit.ToString("0.00");
                else
                    lblOpeningDebit.Text = "";
                lblOpeningCredit.Text = "";
            }

            else
            {
                lblOpeningDebit.Text = "";
                openingGcredit = finalopeningcredit -finalopeningdebit ;
                if (openingGcredit > 0)
                    lblOpeningCredit.Text = openingGcredit.ToString("0.00");
                else
                    lblOpeningCredit.Text = "";
            }
            if ((openingGdebit + debittotal) > (openingGcredit + credittotal))
            {
                closingdebit = (openingGdebit + debittotal) - (openingGcredit + credittotal);
                if (closingdebit > 0)
                    lblClosingDebit.Text = closingdebit.ToString("0.00");
                else
                    lblClosingDebit.Text = "";
            }
            else
            {
                closingcredit = (openingGcredit + credittotal)-(openingGdebit + debittotal) ;
                if (closingcredit > 0)
                    lblClosingCredit.Text = closingcredit.ToString("0.00");
                else
                    lblClosingCredit.Text = "";
            }
                
        }

        private void txtLSearch_TextChanged(object sender, EventArgs e)
        {
            ListLedger.SelectedIndexChanged -= new EventHandler(ListLedger_SelectedIndexChanged);
            SqlDataAdapter cmd = new SqlDataAdapter("Select * from Ledgers where Status=1 and LedgerName like '%" + txtLSearch.Text + "%'", connection);
            DataSet ledgers = new System.Data.DataSet();
            cmd.Fill(ledgers, "Ledgers");
            DataTable dt = new DataTable();
            dt = ledgers.Tables[0];
            ListLedger.DataSource = dt;
            ListLedger.SelectedIndexChanged += new EventHandler(ListLedger_SelectedIndexChanged);
        }

        private void ListLedger_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListLedger.SelectedIndex != -1)
            {
                grdLedgerReport.Rows.Clear();
                grdLedgerReport.AllowUserToAddRows = true;
                debittotal = 0;
                credittotal = 0;
                lblClosingDebit.Text = "";
                lblClosingCredit.Text = "";
                grdLedgerReport.AllowUserToAddRows = true;
                lblLedgerName.Text = ListLedger.GetItemText(ListLedger.SelectedItem) + " Report";
                LoadLedgerReport(int.Parse(ListLedger.SelectedValue.ToString()));
                LoadOpeningBalance(int.Parse(ListLedger.SelectedValue.ToString()));
            }
        }
    }
}
