﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.AppCodes;

namespace BasicAccounts.AppCodes
{
    class Company
    {
        private int _companyID;
        private string _companyName;
        private string _emailID;
        private string _address;
        private string _phone;
        private string _mobile;
        private int _currency;
        private string _mailingName;
        private string _country;
        private DateTime _financilaYearStart;
        private DateTime _booksBegining;
        private Boolean _autoBackUp;
        private Boolean _status;

        Validations validation = new Validations();

        public int CompanyID
        {
            get { return this._companyID; }
            set
            {
                validation.RequiredValidation(value, "Company ID ");
                this._companyID = value;
            }
        }

        public int Currency
        {
            get { return this._currency; }
            set
            {
                validation.RequiredValidation(value, "Curency ");
                this._currency = value;
            }
        }

        public string CompanyName
        {
            get { return this._companyName; }
            set
            {
                validation.RequiredValidation(value, "Company Name");
                this._companyName = value;
            }
        }

        public string Address
        {
            get { return this._address; }
            set
            {
               // validation.RequiredValidation(value, "Company Address");
                this._address = value;
            }
        }

        public string Phone 
        {
            get { return this._phone; }
            set
            {
                if(value!=null)
                {
                    validation.PhoneValidation(value);
                    this._phone = value;
                }
               
            }
        }

        public string Mobile
        {
            get { return this._mobile; }
            set
            {
                if (value != null)
                {
                    validation.PhoneValidation(value);
                    this._mobile = value;
                }

            }
        }

        public string EmailID
        {
            get { return this._emailID; }
            set
            {
                    validation.RequiredValidation(value,"Email ");
                    validation.ValidEmail(value);
                    this._emailID = value;
            }
        }

        public string MailingName
        {
            get { return this._mailingName; }
            set
            {
                if (value != null)
                {
                    this._mailingName = value;
                }

            }
        }

        public string Country
        {
            get { return this._country; }
            set
            {
                    validation.RequiredValidation(value,"Country");
                    this._country = value;
                
            }
        }

        public DateTime FinancialYearStart
        {
            get { return this._financilaYearStart; }
            set
            {
                validation.RequiredValidation(value, "Financial Year Start Date ");
                this._financilaYearStart = value;
            }
        }

        public DateTime BooksBeginning
        {
            get { return this._booksBegining; }
            set
            {
                validation.RequiredValidation(value, "Books Beginning Start Date ");
                this._booksBegining = value;
            }
        }

        public bool Status
        {
            get { return this._status; }
            set
            {
                if (value)
                {
                    this._status = value;
                }
                else this._status = true;
            }
        }

        public bool AutoBackUp
        {
            get { return this._autoBackUp; }
            set
            {
                if (value)
                {
                    this._autoBackUp = value;
                }
                else this._autoBackUp = false;
            }
        }

    }
}
