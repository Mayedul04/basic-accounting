﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace WindowsFormsApplication1.AppCodes
{
    class Validations
    {
        public object ValidEmailRegex { get; private set; }

        public void RequiredValidation(string text, string fieldname)
        {
            if (text != null)
            {
                if (text == "")
                {
                    throw new Exception(fieldname + " Can not be Empty");
                }
                
            }
            else
            {
                throw new Exception(fieldname + " Can not be Null");
            }
        }
        public void RequiredValidation(DateTime date1, string fieldname)
        {
            if (date1 == null)
            {
                throw new Exception(fieldname + " Can not be Null");
            }
        }
        public void RequiredValidation(int value, string fieldname)
        {
            if (value<=0)
            {
                throw new Exception(fieldname + " Can not be Negetive");
            }
        }

        public void RequiredValidation(Int64 value, string fieldname)
        {
            if (value <= 0)
            {
                throw new Exception(fieldname + " Can not be Negetive");
            }
        }

        public  void ValidEmail(string email)
        {
            string expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, string.Empty).Length != 0)
                {
                    throw new Exception("Email is not Valid");
                
                }
            }
            else
            {
                throw new Exception("Email is not Valid");
            }
        }
        public void NumberOnly(string number)
        {
            string expresion;
            expresion = "[0-9]";
            if (Regex.IsMatch(number, expresion))
            {
                if (Regex.Replace(number, expresion, string.Empty).Length != 0)
                {
                    throw new Exception("Please provide correct Number");

                }

            }
            else
            {
                throw new Exception("Please provide correct Number");
            }

        }

        public void PhoneValidation(string number)
        {
            string expresion;
            expresion = "^(?:\\+88|01)?(?:\\d{11}|\\d{13})$";
            if (Regex.IsMatch(number, expresion))
            {
                if (Regex.Replace(number, expresion, string.Empty).Length != 0)
                {
                    throw new Exception("Please provide correct Phone/Mobile Number");

                }

            }
            else
            {
                throw new Exception("Please provide correct Phone/Mobile Number");
            }

        }

        public void ValidedDecimal(string str)
        {

            //string expresion;
            // expresion = "[0-9]{1,4}(\.[0-9]{1,2})?";
            // if (Regex.IsMatch(str, expresion))
            // {
            //     if (Regex.Replace(str, expresion, string.Empty).Length != 0)
            //     {
            //         throw new Exception("Please provide correct Decimal Value");

            //     }

            // }
            // else
            // {
            //     throw new Exception("Please provide correct Decimal Value");
            // }

            try
            {
                decimal.Parse(str);
            }
            catch
            {
                throw new Exception("Please provide correct Decimal Value");
            }
        }
        public void ValidedDouble(string str)
        {

           try
            {
                double.Parse(str);
            }
            catch
            {
                throw new Exception("Please provide correct  Value");
            }
        }
    }
}
