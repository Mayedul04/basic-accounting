﻿namespace BasicAccounts
{
    partial class PeriodicLedger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblLedgerName = new System.Windows.Forms.Label();
            this.pnlMainTrial = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.txtLSearch = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ListLedger = new System.Windows.Forms.ListBox();
            this.lblClosingCredit = new System.Windows.Forms.Label();
            this.lblClosingDebit = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblOpeningCredit = new System.Windows.Forms.Label();
            this.lblOpeningDebit = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpClosingDate = new System.Windows.Forms.DateTimePicker();
            this.dtpOpenningDate = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblDebitTotal = new System.Windows.Forms.Label();
            this.lblCreditTotal = new System.Windows.Forms.Label();
            this.grdLedgerReport = new System.Windows.Forms.DataGridView();
            this.clmODebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmOCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmVType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmAccHead = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTDebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlMainTrial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdLedgerReport)).BeginInit();
            this.SuspendLayout();
            // 
            // lblLedgerName
            // 
            this.lblLedgerName.AutoSize = true;
            this.lblLedgerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLedgerName.ForeColor = System.Drawing.SystemColors.Window;
            this.lblLedgerName.Location = new System.Drawing.Point(433, 10);
            this.lblLedgerName.Name = "lblLedgerName";
            this.lblLedgerName.Size = new System.Drawing.Size(131, 24);
            this.lblLedgerName.TabIndex = 5;
            this.lblLedgerName.Text = "Ledger Report";
            // 
            // pnlMainTrial
            // 
            this.pnlMainTrial.BackColor = System.Drawing.Color.GhostWhite;
            this.pnlMainTrial.Controls.Add(this.label8);
            this.pnlMainTrial.Controls.Add(this.txtLSearch);
            this.pnlMainTrial.Controls.Add(this.label10);
            this.pnlMainTrial.Controls.Add(this.ListLedger);
            this.pnlMainTrial.Controls.Add(this.lblClosingCredit);
            this.pnlMainTrial.Controls.Add(this.lblClosingDebit);
            this.pnlMainTrial.Controls.Add(this.label4);
            this.pnlMainTrial.Controls.Add(this.label3);
            this.pnlMainTrial.Controls.Add(this.lblOpeningCredit);
            this.pnlMainTrial.Controls.Add(this.lblOpeningDebit);
            this.pnlMainTrial.Controls.Add(this.label2);
            this.pnlMainTrial.Controls.Add(this.label1);
            this.pnlMainTrial.Controls.Add(this.dtpClosingDate);
            this.pnlMainTrial.Controls.Add(this.dtpOpenningDate);
            this.pnlMainTrial.Controls.Add(this.label12);
            this.pnlMainTrial.Controls.Add(this.label11);
            this.pnlMainTrial.Controls.Add(this.lblDebitTotal);
            this.pnlMainTrial.Controls.Add(this.lblCreditTotal);
            this.pnlMainTrial.Controls.Add(this.grdLedgerReport);
            this.pnlMainTrial.Location = new System.Drawing.Point(0, 37);
            this.pnlMainTrial.Name = "pnlMainTrial";
            this.pnlMainTrial.Size = new System.Drawing.Size(1141, 527);
            this.pnlMainTrial.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 106);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 13);
            this.label8.TabIndex = 109;
            this.label8.Text = "Search by Ledger Name";
            // 
            // txtLSearch
            // 
            this.txtLSearch.BackColor = System.Drawing.SystemColors.Window;
            this.txtLSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLSearch.Location = new System.Drawing.Point(13, 123);
            this.txtLSearch.Name = "txtLSearch";
            this.txtLSearch.Size = new System.Drawing.Size(137, 22);
            this.txtLSearch.TabIndex = 107;
            this.txtLSearch.TextChanged += new System.EventHandler(this.txtLSearch_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(13, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 20);
            this.label10.TabIndex = 106;
            this.label10.Text = "Ledger List";
            // 
            // ListLedger
            // 
            this.ListLedger.BackColor = System.Drawing.Color.GhostWhite;
            this.ListLedger.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListLedger.DisplayMember = "LedgerName";
            this.ListLedger.FormattingEnabled = true;
            this.ListLedger.Location = new System.Drawing.Point(12, 151);
            this.ListLedger.Name = "ListLedger";
            this.ListLedger.Size = new System.Drawing.Size(137, 366);
            this.ListLedger.TabIndex = 108;
            this.ListLedger.ValueMember = "LedgerID";
            this.ListLedger.SelectedIndexChanged += new System.EventHandler(this.ListLedger_SelectedIndexChanged);
            // 
            // lblClosingCredit
            // 
            this.lblClosingCredit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClosingCredit.Location = new System.Drawing.Point(959, 494);
            this.lblClosingCredit.Name = "lblClosingCredit";
            this.lblClosingCredit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblClosingCredit.Size = new System.Drawing.Size(150, 15);
            this.lblClosingCredit.TabIndex = 90;
            this.lblClosingCredit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblClosingDebit
            // 
            this.lblClosingDebit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClosingDebit.Location = new System.Drawing.Point(807, 495);
            this.lblClosingDebit.Name = "lblClosingDebit";
            this.lblClosingDebit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblClosingDebit.Size = new System.Drawing.Size(150, 15);
            this.lblClosingDebit.TabIndex = 89;
            this.lblClosingDebit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(794, 476);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(322, 13);
            this.label4.TabIndex = 88;
            this.label4.Text = "---------------------------------------------------------------------------------" +
    "------------------------";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(679, 494);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 15);
            this.label3.TabIndex = 87;
            this.label3.Text = "Closing Balance :";
            // 
            // lblOpeningCredit
            // 
            this.lblOpeningCredit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOpeningCredit.Location = new System.Drawing.Point(982, 439);
            this.lblOpeningCredit.Name = "lblOpeningCredit";
            this.lblOpeningCredit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblOpeningCredit.Size = new System.Drawing.Size(127, 15);
            this.lblOpeningCredit.TabIndex = 86;
            this.lblOpeningCredit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblOpeningDebit
            // 
            this.lblOpeningDebit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOpeningDebit.Location = new System.Drawing.Point(807, 439);
            this.lblOpeningDebit.Name = "lblOpeningDebit";
            this.lblOpeningDebit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblOpeningDebit.Size = new System.Drawing.Size(150, 15);
            this.lblOpeningDebit.TabIndex = 85;
            this.lblOpeningDebit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(700, 459);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 15);
            this.label2.TabIndex = 84;
            this.label2.Text = "Current Total :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(673, 435);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 15);
            this.label1.TabIndex = 83;
            this.label1.Text = "Opening Balance :";
            // 
            // dtpClosingDate
            // 
            this.dtpClosingDate.CustomFormat = "dd-MM-yyyy";
            this.dtpClosingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpClosingDate.Location = new System.Drawing.Point(13, 46);
            this.dtpClosingDate.Name = "dtpClosingDate";
            this.dtpClosingDate.Size = new System.Drawing.Size(128, 20);
            this.dtpClosingDate.TabIndex = 81;
            // 
            // dtpOpenningDate
            // 
            this.dtpOpenningDate.CustomFormat = "dd-MM-yyyy";
            this.dtpOpenningDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOpenningDate.Location = new System.Drawing.Point(13, 18);
            this.dtpOpenningDate.Name = "dtpOpenningDate";
            this.dtpOpenningDate.Size = new System.Drawing.Size(128, 20);
            this.dtpOpenningDate.TabIndex = 80;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(794, 504);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(322, 13);
            this.label12.TabIndex = 79;
            this.label12.Text = "---------------------------------------------------------------------------------" +
    "------------------------";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(794, 510);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(322, 13);
            this.label11.TabIndex = 78;
            this.label11.Text = "---------------------------------------------------------------------------------" +
    "------------------------";
            // 
            // lblDebitTotal
            // 
            this.lblDebitTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDebitTotal.Location = new System.Drawing.Point(807, 461);
            this.lblDebitTotal.Name = "lblDebitTotal";
            this.lblDebitTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDebitTotal.Size = new System.Drawing.Size(150, 15);
            this.lblDebitTotal.TabIndex = 77;
            this.lblDebitTotal.Text = "0.00";
            this.lblDebitTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCreditTotal
            // 
            this.lblCreditTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreditTotal.Location = new System.Drawing.Point(986, 461);
            this.lblCreditTotal.Name = "lblCreditTotal";
            this.lblCreditTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCreditTotal.Size = new System.Drawing.Size(123, 15);
            this.lblCreditTotal.TabIndex = 76;
            this.lblCreditTotal.Text = "0.00";
            this.lblCreditTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grdLedgerReport
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.GhostWhite;
            this.grdLedgerReport.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grdLedgerReport.BackgroundColor = System.Drawing.Color.GhostWhite;
            this.grdLedgerReport.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdLedgerReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.grdLedgerReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdLedgerReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmODebit,
            this.clmOCredit,
            this.clmVType,
            this.clmAccHead,
            this.clmAmount,
            this.clmTDebit,
            this.clmTCredit});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdLedgerReport.DefaultCellStyle = dataGridViewCellStyle8;
            this.grdLedgerReport.GridColor = System.Drawing.SystemColors.Control;
            this.grdLedgerReport.Location = new System.Drawing.Point(155, 15);
            this.grdLedgerReport.Name = "grdLedgerReport";
            this.grdLedgerReport.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grdLedgerReport.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdLedgerReport.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.grdLedgerReport.RowHeadersVisible = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.GhostWhite;
            this.grdLedgerReport.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.grdLedgerReport.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.GhostWhite;
            this.grdLedgerReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdLedgerReport.Size = new System.Drawing.Size(974, 417);
            this.grdLedgerReport.TabIndex = 0;
            // 
            // clmODebit
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            this.clmODebit.DefaultCellStyle = dataGridViewCellStyle3;
            this.clmODebit.HeaderText = "Date";
            this.clmODebit.Name = "clmODebit";
            this.clmODebit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmOCredit
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            this.clmOCredit.DefaultCellStyle = dataGridViewCellStyle4;
            this.clmOCredit.HeaderText = "Voucher No";
            this.clmOCredit.Name = "clmOCredit";
            this.clmOCredit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmOCredit.Width = 110;
            // 
            // clmVType
            // 
            this.clmVType.HeaderText = "VoucherType";
            this.clmVType.Name = "clmVType";
            this.clmVType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmAccHead
            // 
            this.clmAccHead.HeaderText = "Head of Accounts";
            this.clmAccHead.Name = "clmAccHead";
            this.clmAccHead.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmAccHead.Width = 262;
            // 
            // clmAmount
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.clmAmount.DefaultCellStyle = dataGridViewCellStyle5;
            this.clmAmount.HeaderText = "";
            this.clmAmount.Name = "clmAmount";
            this.clmAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmTDebit
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            this.clmTDebit.DefaultCellStyle = dataGridViewCellStyle6;
            this.clmTDebit.HeaderText = "Debit";
            this.clmTDebit.Name = "clmTDebit";
            this.clmTDebit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmTDebit.Width = 140;
            // 
            // clmTCredit
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            this.clmTCredit.DefaultCellStyle = dataGridViewCellStyle7;
            this.clmTCredit.HeaderText = "Credit";
            this.clmTCredit.Name = "clmTCredit";
            this.clmTCredit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmTCredit.Width = 140;
            // 
            // PeriodicLedger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(1141, 593);
            this.Controls.Add(this.lblLedgerName);
            this.Controls.Add(this.pnlMainTrial);
            this.Name = "PeriodicLedger";
            this.Text = "PeriodicLedger";
            this.Load += new System.EventHandler(this.PeriodicLedger_Load);
            this.pnlMainTrial.ResumeLayout(false);
            this.pnlMainTrial.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdLedgerReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblLedgerName;
        private System.Windows.Forms.Panel pnlMainTrial;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblDebitTotal;
        private System.Windows.Forms.Label lblCreditTotal;
        private System.Windows.Forms.DataGridView grdLedgerReport;
        private System.Windows.Forms.DateTimePicker dtpOpenningDate;
        private System.Windows.Forms.DateTimePicker dtpClosingDate;
        private System.Windows.Forms.Label lblOpeningCredit;
        private System.Windows.Forms.Label lblOpeningDebit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblClosingCredit;
        private System.Windows.Forms.Label lblClosingDebit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtLSearch;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ListBox ListLedger;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmODebit;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmOCredit;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmVType;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmAccHead;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTDebit;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTCredit;
    }
}