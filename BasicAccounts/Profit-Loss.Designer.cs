﻿namespace BasicAccounts
{
    partial class Profit_Loss
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlMainTrial = new System.Windows.Forms.Panel();
            this.grdProfitLoss = new System.Windows.Forms.DataGridView();
            this.clmAccHead = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmCDebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpClosingDate = new System.Windows.Forms.DateTimePicker();
            this.dtpOpenningDate = new System.Windows.Forms.DateTimePicker();
            this.btnPreview = new System.Windows.Forms.Button();
            this.pnlMainTrial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProfitLoss)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMainTrial
            // 
            this.pnlMainTrial.AutoScroll = true;
            this.pnlMainTrial.BackColor = System.Drawing.SystemColors.Window;
            this.pnlMainTrial.Controls.Add(this.grdProfitLoss);
            this.pnlMainTrial.Location = new System.Drawing.Point(0, 41);
            this.pnlMainTrial.Name = "pnlMainTrial";
            this.pnlMainTrial.Size = new System.Drawing.Size(1149, 552);
            this.pnlMainTrial.TabIndex = 2;
            // 
            // grdProfitLoss
            // 
            this.grdProfitLoss.BackgroundColor = System.Drawing.SystemColors.Window;
            this.grdProfitLoss.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdProfitLoss.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.grdProfitLoss.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdProfitLoss.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmAccHead,
            this.clmCDebit});
            this.grdProfitLoss.GridColor = System.Drawing.SystemColors.Control;
            this.grdProfitLoss.Location = new System.Drawing.Point(10, 16);
            this.grdProfitLoss.Name = "grdProfitLoss";
            this.grdProfitLoss.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grdProfitLoss.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdProfitLoss.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.grdProfitLoss.RowHeadersVisible = false;
            this.grdProfitLoss.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdProfitLoss.Size = new System.Drawing.Size(1129, 524);
            this.grdProfitLoss.TabIndex = 0;
            // 
            // clmAccHead
            // 
            this.clmAccHead.HeaderText = "Income";
            this.clmAccHead.Name = "clmAccHead";
            this.clmAccHead.Width = 908;
            // 
            // clmCDebit
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            this.clmCDebit.DefaultCellStyle = dataGridViewCellStyle8;
            this.clmCDebit.HeaderText = "Amount";
            this.clmCDebit.Name = "clmCDebit";
            this.clmCDebit.Width = 200;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Window;
            this.label1.Location = new System.Drawing.Point(464, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Profit & Loss Account";
            // 
            // dtpClosingDate
            // 
            this.dtpClosingDate.CustomFormat = "dd-MM-yyyy";
            this.dtpClosingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpClosingDate.Location = new System.Drawing.Point(868, 12);
            this.dtpClosingDate.Name = "dtpClosingDate";
            this.dtpClosingDate.Size = new System.Drawing.Size(150, 20);
            this.dtpClosingDate.TabIndex = 12;
            // 
            // dtpOpenningDate
            // 
            this.dtpOpenningDate.CustomFormat = "dd-MM-yyyy";
            this.dtpOpenningDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOpenningDate.Location = new System.Drawing.Point(693, 12);
            this.dtpOpenningDate.Name = "dtpOpenningDate";
            this.dtpOpenningDate.Size = new System.Drawing.Size(150, 20);
            this.dtpOpenningDate.TabIndex = 11;
            this.dtpOpenningDate.ValueChanged += new System.EventHandler(this.dtpOpenningDate_ValueChanged);
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(1042, 12);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPreview.TabIndex = 10;
            this.btnPreview.Text = "Create View";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // Profit_Loss
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(1149, 593);
            this.Controls.Add(this.dtpClosingDate);
            this.Controls.Add(this.dtpOpenningDate);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlMainTrial);
            this.Name = "Profit_Loss";
            this.Text = "Profit_Loss";
            this.Load += new System.EventHandler(this.Profit_Loss_Load);
            this.pnlMainTrial.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdProfitLoss)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlMainTrial;
        private System.Windows.Forms.DataGridView grdProfitLoss;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmAccHead;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmCDebit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpClosingDate;
        private System.Windows.Forms.DateTimePicker dtpOpenningDate;
        private System.Windows.Forms.Button btnPreview;
    }
}