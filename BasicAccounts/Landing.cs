﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasicAccounts
{
    public partial class Landing : Form
    {
        static string connetionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connetionString);
        public Landing()
        {
            InitializeComponent();
        }

        private void Landing_Load(object sender, EventArgs e)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select * from Company where Status=1", connection);
            DataSet natures = new System.Data.DataSet();
            cmd.Fill(natures, "Company");
            DataTable dt = new DataTable();
            dt = natures.Tables[0];
            ListCompany.DataSource = dt;

            lblCompanyName.Text = dt.Rows[0].ItemArray[1].ToString();
        }
    }
}
