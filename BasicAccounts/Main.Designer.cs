﻿namespace BasicAccounts
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mastersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountsSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ledgersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventorySetupToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.payrollSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviceItemSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviceItemSetupToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.basicTransactionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cashPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cashReceiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankReceiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.journalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseTransactionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesTransactionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryTransactionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.payrollTransactionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trailBalaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.balanceSheetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profitLossAcoountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incomeStatementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.receiptAndPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cashFlowStatementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fundFlowStatementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.booksAndRegisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.payrollReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.voucherReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cashPaymentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bankPaymentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cashReceiptToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bankReceiptToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.journalToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.contraToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ledgerReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pnlTopMenu = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblCompany = new System.Windows.Forms.Label();
            this.pnlPlayground = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.pnlTopMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(173)))), ((int)(((byte)(167)))));
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mastersToolStripMenuItem,
            this.accountsSetupToolStripMenuItem,
            this.inventorySetupToolStripMenuItem1,
            this.payrollSetupToolStripMenuItem,
            this.serviceItemSetupToolStripMenuItem,
            this.serviceItemSetupToolStripMenuItem1,
            this.transactionsToolStripMenuItem,
            this.basicTransactionsToolStripMenuItem,
            this.purchaseTransactionsToolStripMenuItem,
            this.salesTransactionsToolStripMenuItem,
            this.inventoryTransactionsToolStripMenuItem,
            this.payrollTransactionsToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.trailBalaceToolStripMenuItem,
            this.balanceSheetToolStripMenuItem,
            this.profitLossAcoountToolStripMenuItem,
            this.incomeStatementToolStripMenuItem,
            this.receiptAndPaymentToolStripMenuItem,
            this.cashFlowStatementToolStripMenuItem,
            this.fundFlowStatementToolStripMenuItem,
            this.booksAndRegisterToolStripMenuItem,
            this.payrollReportToolStripMenuItem,
            this.inventoryReportsToolStripMenuItem,
            this.voucherReportsToolStripMenuItem,
            this.ledgerReportsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(167, 689);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mastersToolStripMenuItem
            // 
            this.mastersToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mastersToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Window;
            this.mastersToolStripMenuItem.Margin = new System.Windows.Forms.Padding(5);
            this.mastersToolStripMenuItem.Name = "mastersToolStripMenuItem";
            this.mastersToolStripMenuItem.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.mastersToolStripMenuItem.Size = new System.Drawing.Size(150, 21);
            this.mastersToolStripMenuItem.Text = "Masters";
            // 
            // accountsSetupToolStripMenuItem
            // 
            this.accountsSetupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.groupsToolStripMenuItem,
            this.ledgersToolStripMenuItem});
            this.accountsSetupToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accountsSetupToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.accountsSetupToolStripMenuItem.Name = "accountsSetupToolStripMenuItem";
            this.accountsSetupToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.accountsSetupToolStripMenuItem.Text = "Accounts Setup";
            // 
            // groupsToolStripMenuItem
            // 
            this.groupsToolStripMenuItem.Name = "groupsToolStripMenuItem";
            this.groupsToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.groupsToolStripMenuItem.Text = "Groups";
            this.groupsToolStripMenuItem.Click += new System.EventHandler(this.groupsToolStripMenuItem_Click_1);
            // 
            // ledgersToolStripMenuItem
            // 
            this.ledgersToolStripMenuItem.Name = "ledgersToolStripMenuItem";
            this.ledgersToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.ledgersToolStripMenuItem.Text = "Ledgers";
            this.ledgersToolStripMenuItem.Click += new System.EventHandler(this.ledgersToolStripMenuItem_Click_1);
            // 
            // inventorySetupToolStripMenuItem1
            // 
            this.inventorySetupToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inventorySetupToolStripMenuItem1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.inventorySetupToolStripMenuItem1.Name = "inventorySetupToolStripMenuItem1";
            this.inventorySetupToolStripMenuItem1.Size = new System.Drawing.Size(160, 21);
            this.inventorySetupToolStripMenuItem1.Text = "Inventory Setup";
            this.inventorySetupToolStripMenuItem1.Click += new System.EventHandler(this.inventorySetupToolStripMenuItem1_Click);
            // 
            // payrollSetupToolStripMenuItem
            // 
            this.payrollSetupToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.payrollSetupToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.payrollSetupToolStripMenuItem.Name = "payrollSetupToolStripMenuItem";
            this.payrollSetupToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.payrollSetupToolStripMenuItem.Text = "Payroll Setup";
            // 
            // serviceItemSetupToolStripMenuItem
            // 
            this.serviceItemSetupToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serviceItemSetupToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.serviceItemSetupToolStripMenuItem.Name = "serviceItemSetupToolStripMenuItem";
            this.serviceItemSetupToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.serviceItemSetupToolStripMenuItem.Text = "Service Item Setup";
            // 
            // serviceItemSetupToolStripMenuItem1
            // 
            this.serviceItemSetupToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serviceItemSetupToolStripMenuItem1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.serviceItemSetupToolStripMenuItem1.Name = "serviceItemSetupToolStripMenuItem1";
            this.serviceItemSetupToolStripMenuItem1.Size = new System.Drawing.Size(160, 21);
            this.serviceItemSetupToolStripMenuItem1.Text = "Asset Item Setup";
            // 
            // transactionsToolStripMenuItem
            // 
            this.transactionsToolStripMenuItem.AutoSize = false;
            this.transactionsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transactionsToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Window;
            this.transactionsToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.transactionsToolStripMenuItem.Name = "transactionsToolStripMenuItem";
            this.transactionsToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.transactionsToolStripMenuItem.Text = "Transactions";
            // 
            // basicTransactionsToolStripMenuItem
            // 
            this.basicTransactionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cashPaymentToolStripMenuItem,
            this.cashReceiptToolStripMenuItem,
            this.bankPaymentToolStripMenuItem,
            this.bankReceiptToolStripMenuItem,
            this.journalToolStripMenuItem,
            this.contraToolStripMenuItem});
            this.basicTransactionsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.basicTransactionsToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.basicTransactionsToolStripMenuItem.Name = "basicTransactionsToolStripMenuItem";
            this.basicTransactionsToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.basicTransactionsToolStripMenuItem.Text = "Basic Transactions";
            // 
            // cashPaymentToolStripMenuItem
            // 
            this.cashPaymentToolStripMenuItem.Name = "cashPaymentToolStripMenuItem";
            this.cashPaymentToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.cashPaymentToolStripMenuItem.Text = "Cash Payment";
            this.cashPaymentToolStripMenuItem.Click += new System.EventHandler(this.cashPaymentToolStripMenuItem_Click);
            // 
            // cashReceiptToolStripMenuItem
            // 
            this.cashReceiptToolStripMenuItem.Name = "cashReceiptToolStripMenuItem";
            this.cashReceiptToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.cashReceiptToolStripMenuItem.Text = "Cash Receipt";
            this.cashReceiptToolStripMenuItem.Click += new System.EventHandler(this.cashReceiptToolStripMenuItem_Click);
            // 
            // bankPaymentToolStripMenuItem
            // 
            this.bankPaymentToolStripMenuItem.Name = "bankPaymentToolStripMenuItem";
            this.bankPaymentToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.bankPaymentToolStripMenuItem.Text = "Bank Payment";
            this.bankPaymentToolStripMenuItem.Click += new System.EventHandler(this.bankPaymentToolStripMenuItem_Click);
            // 
            // bankReceiptToolStripMenuItem
            // 
            this.bankReceiptToolStripMenuItem.Name = "bankReceiptToolStripMenuItem";
            this.bankReceiptToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.bankReceiptToolStripMenuItem.Text = "Bank Receipt";
            this.bankReceiptToolStripMenuItem.Click += new System.EventHandler(this.bankReceiptToolStripMenuItem_Click);
            // 
            // journalToolStripMenuItem
            // 
            this.journalToolStripMenuItem.Name = "journalToolStripMenuItem";
            this.journalToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.journalToolStripMenuItem.Text = "Journal";
            this.journalToolStripMenuItem.Click += new System.EventHandler(this.journalToolStripMenuItem_Click);
            // 
            // contraToolStripMenuItem
            // 
            this.contraToolStripMenuItem.Name = "contraToolStripMenuItem";
            this.contraToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.contraToolStripMenuItem.Text = "Contra";
            this.contraToolStripMenuItem.Click += new System.EventHandler(this.contraToolStripMenuItem_Click);
            // 
            // purchaseTransactionsToolStripMenuItem
            // 
            this.purchaseTransactionsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.purchaseTransactionsToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.purchaseTransactionsToolStripMenuItem.Name = "purchaseTransactionsToolStripMenuItem";
            this.purchaseTransactionsToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.purchaseTransactionsToolStripMenuItem.Text = "Purchase Transactions";
            // 
            // salesTransactionsToolStripMenuItem
            // 
            this.salesTransactionsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salesTransactionsToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.salesTransactionsToolStripMenuItem.Name = "salesTransactionsToolStripMenuItem";
            this.salesTransactionsToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.salesTransactionsToolStripMenuItem.Text = "Sales Transactions";
            // 
            // inventoryTransactionsToolStripMenuItem
            // 
            this.inventoryTransactionsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inventoryTransactionsToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.inventoryTransactionsToolStripMenuItem.Name = "inventoryTransactionsToolStripMenuItem";
            this.inventoryTransactionsToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.inventoryTransactionsToolStripMenuItem.Text = "Inventory Transactions";
            // 
            // payrollTransactionsToolStripMenuItem
            // 
            this.payrollTransactionsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.payrollTransactionsToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.payrollTransactionsToolStripMenuItem.Name = "payrollTransactionsToolStripMenuItem";
            this.payrollTransactionsToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.payrollTransactionsToolStripMenuItem.Text = "Payroll Transactions";
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportsToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Window;
            this.reportsToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // trailBalaceToolStripMenuItem
            // 
            this.trailBalaceToolStripMenuItem.Name = "trailBalaceToolStripMenuItem";
            this.trailBalaceToolStripMenuItem.Size = new System.Drawing.Size(160, 19);
            this.trailBalaceToolStripMenuItem.Text = "Trial Balace";
            this.trailBalaceToolStripMenuItem.Click += new System.EventHandler(this.trailBalaceToolStripMenuItem_Click);
            // 
            // balanceSheetToolStripMenuItem
            // 
            this.balanceSheetToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.balanceSheetToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.balanceSheetToolStripMenuItem.Name = "balanceSheetToolStripMenuItem";
            this.balanceSheetToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.balanceSheetToolStripMenuItem.Text = "Balance Sheet";
            this.balanceSheetToolStripMenuItem.Click += new System.EventHandler(this.balanceSheetToolStripMenuItem_Click);
            // 
            // profitLossAcoountToolStripMenuItem
            // 
            this.profitLossAcoountToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profitLossAcoountToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.profitLossAcoountToolStripMenuItem.Name = "profitLossAcoountToolStripMenuItem";
            this.profitLossAcoountToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.profitLossAcoountToolStripMenuItem.Text = "Profit and Loss A/C";
            this.profitLossAcoountToolStripMenuItem.Click += new System.EventHandler(this.profitLossAcoountToolStripMenuItem_Click);
            // 
            // incomeStatementToolStripMenuItem
            // 
            this.incomeStatementToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.incomeStatementToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.incomeStatementToolStripMenuItem.Name = "incomeStatementToolStripMenuItem";
            this.incomeStatementToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.incomeStatementToolStripMenuItem.Text = "Income Statement";
            // 
            // receiptAndPaymentToolStripMenuItem
            // 
            this.receiptAndPaymentToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.receiptAndPaymentToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.receiptAndPaymentToolStripMenuItem.Name = "receiptAndPaymentToolStripMenuItem";
            this.receiptAndPaymentToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.receiptAndPaymentToolStripMenuItem.Text = "Receipt and Payment";
            // 
            // cashFlowStatementToolStripMenuItem
            // 
            this.cashFlowStatementToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cashFlowStatementToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.cashFlowStatementToolStripMenuItem.Name = "cashFlowStatementToolStripMenuItem";
            this.cashFlowStatementToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.cashFlowStatementToolStripMenuItem.Text = "Cash Flow Statement";
            // 
            // fundFlowStatementToolStripMenuItem
            // 
            this.fundFlowStatementToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fundFlowStatementToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.fundFlowStatementToolStripMenuItem.Name = "fundFlowStatementToolStripMenuItem";
            this.fundFlowStatementToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.fundFlowStatementToolStripMenuItem.Text = "Fund Flow Statement";
            // 
            // booksAndRegisterToolStripMenuItem
            // 
            this.booksAndRegisterToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.booksAndRegisterToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.booksAndRegisterToolStripMenuItem.Name = "booksAndRegisterToolStripMenuItem";
            this.booksAndRegisterToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.booksAndRegisterToolStripMenuItem.Text = "Books and Register";
            // 
            // payrollReportToolStripMenuItem
            // 
            this.payrollReportToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.payrollReportToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.payrollReportToolStripMenuItem.Name = "payrollReportToolStripMenuItem";
            this.payrollReportToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.payrollReportToolStripMenuItem.Text = "Payroll Reports";
            this.payrollReportToolStripMenuItem.Click += new System.EventHandler(this.payrollReportToolStripMenuItem_Click);
            // 
            // inventoryReportsToolStripMenuItem
            // 
            this.inventoryReportsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inventoryReportsToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.inventoryReportsToolStripMenuItem.Name = "inventoryReportsToolStripMenuItem";
            this.inventoryReportsToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.inventoryReportsToolStripMenuItem.Text = "Inventory Reports";
            // 
            // voucherReportsToolStripMenuItem
            // 
            this.voucherReportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cashPaymentToolStripMenuItem1,
            this.bankPaymentToolStripMenuItem1,
            this.cashReceiptToolStripMenuItem1,
            this.bankReceiptToolStripMenuItem1,
            this.journalToolStripMenuItem1,
            this.contraToolStripMenuItem1});
            this.voucherReportsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voucherReportsToolStripMenuItem.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.voucherReportsToolStripMenuItem.Name = "voucherReportsToolStripMenuItem";
            this.voucherReportsToolStripMenuItem.Size = new System.Drawing.Size(160, 21);
            this.voucherReportsToolStripMenuItem.Text = "Voucher Reports";
            // 
            // cashPaymentToolStripMenuItem1
            // 
            this.cashPaymentToolStripMenuItem1.Name = "cashPaymentToolStripMenuItem1";
            this.cashPaymentToolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
            this.cashPaymentToolStripMenuItem1.Text = "Cash Payment";
            this.cashPaymentToolStripMenuItem1.Click += new System.EventHandler(this.cashPaymentToolStripMenuItem1_Click);
            // 
            // bankPaymentToolStripMenuItem1
            // 
            this.bankPaymentToolStripMenuItem1.Name = "bankPaymentToolStripMenuItem1";
            this.bankPaymentToolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
            this.bankPaymentToolStripMenuItem1.Text = "Bank Payment";
            this.bankPaymentToolStripMenuItem1.Click += new System.EventHandler(this.bankPaymentToolStripMenuItem1_Click);
            // 
            // cashReceiptToolStripMenuItem1
            // 
            this.cashReceiptToolStripMenuItem1.Name = "cashReceiptToolStripMenuItem1";
            this.cashReceiptToolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
            this.cashReceiptToolStripMenuItem1.Text = "Cash Receipt";
            this.cashReceiptToolStripMenuItem1.Click += new System.EventHandler(this.cashReceiptToolStripMenuItem1_Click);
            // 
            // bankReceiptToolStripMenuItem1
            // 
            this.bankReceiptToolStripMenuItem1.Name = "bankReceiptToolStripMenuItem1";
            this.bankReceiptToolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
            this.bankReceiptToolStripMenuItem1.Text = "Bank Receipt";
            this.bankReceiptToolStripMenuItem1.Click += new System.EventHandler(this.bankReceiptToolStripMenuItem1_Click);
            // 
            // journalToolStripMenuItem1
            // 
            this.journalToolStripMenuItem1.Name = "journalToolStripMenuItem1";
            this.journalToolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
            this.journalToolStripMenuItem1.Text = "Journal";
            this.journalToolStripMenuItem1.Click += new System.EventHandler(this.journalToolStripMenuItem1_Click);
            // 
            // contraToolStripMenuItem1
            // 
            this.contraToolStripMenuItem1.Name = "contraToolStripMenuItem1";
            this.contraToolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
            this.contraToolStripMenuItem1.Text = "Contra";
            this.contraToolStripMenuItem1.Click += new System.EventHandler(this.contraToolStripMenuItem1_Click);
            // 
            // ledgerReportsToolStripMenuItem
            // 
            this.ledgerReportsToolStripMenuItem.Name = "ledgerReportsToolStripMenuItem";
            this.ledgerReportsToolStripMenuItem.Size = new System.Drawing.Size(160, 19);
            this.ledgerReportsToolStripMenuItem.Text = "Ledger Reports";
            this.ledgerReportsToolStripMenuItem.Click += new System.EventHandler(this.ledgerReportsToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Window;
            this.label1.Location = new System.Drawing.Point(186, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Year : ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartDate.ForeColor = System.Drawing.SystemColors.Window;
            this.lblStartDate.Location = new System.Drawing.Point(231, 18);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(74, 15);
            this.lblStartDate.TabIndex = 3;
            this.lblStartDate.Text = "01-07-2016 ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Coral;
            this.label3.Location = new System.Drawing.Point(302, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "To";
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndDate.ForeColor = System.Drawing.SystemColors.Window;
            this.lblEndDate.Location = new System.Drawing.Point(329, 18);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(71, 15);
            this.lblEndDate.TabIndex = 5;
            this.lblEndDate.Text = "30-06-2017";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Gold;
            this.label5.Location = new System.Drawing.Point(630, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(252, 18);
            this.label5.TabIndex = 6;
            this.label5.Text = "ADVANCE SOFTWARE LIMITED";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.Window;
            this.label6.Location = new System.Drawing.Point(1128, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "Voucher Up Date :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.Window;
            this.label7.Location = new System.Drawing.Point(1249, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 15);
            this.label7.TabIndex = 8;
            this.label7.Text = "30-06-2017";
            // 
            // pnlTopMenu
            // 
            this.pnlTopMenu.Controls.Add(this.label12);
            this.pnlTopMenu.Controls.Add(this.label11);
            this.pnlTopMenu.Controls.Add(this.label10);
            this.pnlTopMenu.Controls.Add(this.label9);
            this.pnlTopMenu.Controls.Add(this.label8);
            this.pnlTopMenu.Controls.Add(this.lblCompany);
            this.pnlTopMenu.Location = new System.Drawing.Point(167, 37);
            this.pnlTopMenu.Name = "pnlTopMenu";
            this.pnlTopMenu.Size = new System.Drawing.Size(1199, 30);
            this.pnlTopMenu.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(998, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(200, 30);
            this.label12.TabIndex = 5;
            this.label12.Text = "Log out";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(80)))));
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(796, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(201, 30);
            this.label11.TabIndex = 4;
            this.label11.Text = "Period";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(141)))), ((int)(((byte)(212)))));
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(597, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(197, 30);
            this.label10.TabIndex = 3;
            this.label10.Text = "Print";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Yellow;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(392, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(204, 30);
            this.label9.TabIndex = 2;
            this.label9.Text = "Export";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(108)))), ((int)(((byte)(10)))));
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(196, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(196, 30);
            this.label8.TabIndex = 1;
            this.label8.Text = "Configuration";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCompany
            // 
            this.lblCompany.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(161)))), ((int)(((byte)(199)))));
            this.lblCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompany.Location = new System.Drawing.Point(0, 0);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(196, 30);
            this.lblCompany.TabIndex = 0;
            this.lblCompany.Text = "Company Information";
            this.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlPlayground
            // 
            this.pnlPlayground.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlPlayground.BackColor = System.Drawing.Color.Transparent;
            this.pnlPlayground.Location = new System.Drawing.Point(175, 70);
            this.pnlPlayground.Name = "pnlPlayground";
            this.pnlPlayground.Size = new System.Drawing.Size(1161, 619);
            this.pnlPlayground.TabIndex = 10;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(126)))));
            this.ClientSize = new System.Drawing.Size(1344, 689);
            this.Controls.Add(this.pnlPlayground);
            this.Controls.Add(this.pnlTopMenu);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblEndDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblStartDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(866, 485);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Accounts";
            this.Load += new System.EventHandler(this.Main_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Main_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnlTopMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mastersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountsSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventorySetupToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem payrollSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serviceItemSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serviceItemSetupToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem transactionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem basicTransactionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashReceiptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankReceiptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem journalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseTransactionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesTransactionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryTransactionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem payrollTransactionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem balanceSheetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profitLossAcoountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incomeStatementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receiptAndPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashFlowStatementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fundFlowStatementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem booksAndRegisterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem payrollReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem voucherReportsToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel pnlTopMenu;
        private System.Windows.Forms.Label lblCompany;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel pnlPlayground;
        private System.Windows.Forms.ToolStripMenuItem groupsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ledgersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trailBalaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashPaymentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem bankPaymentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cashReceiptToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem bankReceiptToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem journalToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem contraToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ledgerReportsToolStripMenuItem;
    }
}