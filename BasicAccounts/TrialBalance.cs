﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasicAccounts
{
    public partial class TrialBalance : Form
    {
        static string connetionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connetionString);
        private decimal debittotal = 0;
        private string reporttype = "normal";
        private decimal credittotal = 0;
        private int subgroupcount = 0;
        private List<object> GropupLevel1 = new List<Object>();
        private List<object> GropupLevel2 = new List<Object>();

        private  Decimal oDebitGTotal = 0;
        private Decimal oCreditGTotal = 0;
        private Decimal TCreditGTotal = 0;
        private Decimal TDebitGTotal = 0;
        private Decimal cDebitGTotal = 0;
        private Decimal cCreditGTotal = 0;
        public TrialBalance()
        {
            InitializeComponent();
        }

        private void TrialBalance_Load(object sender, EventArgs e)
        {

            int parentid = 0;
            string parentname = "";
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = 1;
            SqlDataReader reader1 = adapter.SelectCommand.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    parentname = reader1["GroupName"].ToString();
                    parentid = int.Parse(reader1["GroupID"].ToString());
                    Parent parent = new Parent();
                    parent.Id = parentid;
                    parent.Name = parentname;
                    GropupLevel1.Add(parent);
                }
            }

            connection.Close();
            foreach (Parent items in GropupLevel1)
            {
                LoadSubGroups(items, 2);
            }


            lblDebitTotal.Text = debittotal.ToString("0.00");
            lblCreditTotal.Text = credittotal.ToString("0.00");

            grdTrialBalance.AllowUserToAddRows = false;
        }

        private void LoadSubGroups(Parent parent, int level)
        {
            
            int parentid = 0;
            string parentname = "";
            int i = 0;
            SqlDataAdapter adapter1 = new SqlDataAdapter();
            string sql = null;
            sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID";
            connection.Open();
            adapter1.SelectCommand = new SqlCommand(sql, connection);
            adapter1.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = parent.Id;
            SqlDataReader reader1 = adapter1.SelectCommand.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    parentname = reader1["GroupName"].ToString();
                    parentid = int.Parse(reader1["GroupID"].ToString());
                    Parent parent1 = new Parent();
                    parent1.Id = parentid;
                    parent1.Name = parentname;
                    GropupLevel2.Add(parent1);
                }
            }
            else
            {
                if (level == 3)
                {
                    int rowcount = grdTrialBalance.Rows.Count;
                    if (rowcount > 1)
                    {
                        grdTrialBalance.Rows.RemoveAt(rowcount - 2);
                    }
                }
                else
                {
                    reader1.Close();
                    connection.Close();
                    LoadLedgers(parent, 1);
               }
            }

            connection.Close();
            if (GropupLevel2.Count > 0)
            {
                if(grdTrialBalance.Rows.Count==0 && reporttype!="normal")
                    this.grdTrialBalance.Rows.Add(parent.Name);
                else
                {

                    DataGridViewRow row1 = (DataGridViewRow)grdTrialBalance.Rows[0].Clone();
                    row1.Cells[0].Value = parent.Name;
                    grdTrialBalance.Rows.Add(row1);
                }
                   
                subgroupcount = 0;
                foreach (Parent items in GropupLevel2)
                {
                    LoadLedgers(items, 2);
                    
                }
                GropupLevel2.Clear();
            }
            
        }

        private void LoadLedgers(Parent parent,int level)
        {
           
            #region Load Data
            Decimal rowDebitTotal = 0;
            Decimal rowCreditTotal = 0;
            Decimal openingDebit = 0;
            Decimal openingCredit = 0;
            Decimal tDebit = 0;
            Decimal tCredit = 0;
            Decimal oDebitTotal = 0;
            Decimal oCreditTotal = 0;
            Decimal TCreditTotal = 0;
            Decimal TDebitTotal = 0;
            Decimal cDebitTotal = 0;
            Decimal cCreditTotal = 0;
            string blank = "";
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            if(reporttype=="normal")
                sql = "Select OpeningDebit,OpeningCredit,LedgerName,TDebit,TCredit, LedgerID,TType  from  View_TrialBalance_Initial Where (GroupID = @GroupID)";
            else
                sql = "Select OpeningDebit,OpeningCredit,LedgerName,TDebit,TCredit, LedgerID, TType  from  View_Periodic_Trail_Balance_2 Where (GroupID = @GroupID)  order by LedgerID, TType Desc";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("GroupID", SqlDbType.Int, 32).Value = parent.Id;
            SqlDataReader reader = adapter.SelectCommand.ExecuteReader();
            subgroupcount++;
            int duplicateledger = 0;
            if (reader.HasRows)
            {
                DataGridViewRow row1 = (DataGridViewRow)grdTrialBalance.Rows[0].Clone();
                
                if (level == 2)
                    row1.Cells[0].Value = "     " + parent.Name;
                else
                    row1.Cells[0].Value = parent.Name;
                grdTrialBalance.Rows.Add(row1);
                var loop = true;
                while (loop)
                {
                    loop = reader.Read();
                    if (loop) //When more ledgers are coming under same group
                    {
                        if (int.Parse(reader["LedgerID"].ToString()) == duplicateledger)
                        #region When Same Ledger came with only Closing balance
                        {
                            
                            decimal calculatedopeningDebit = decimal.Parse(reader["OpeningDebit"].ToString());
                            decimal calculatedopeningCredit = decimal.Parse(reader["OpeningCredit"].ToString());

                            openingDebit = calculatedopeningDebit;

                            if (openingDebit != 0)
                            {
                                if (grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[1].Value.ToString() != "")
                                {
                                    oDebitTotal -= decimal.Parse(grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[1].Value.ToString());
                                    //cDebitTotal -= decimal.Parse(grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[1].Value.ToString());
                                }

                                oDebitTotal += openingDebit;
                                rowDebitTotal += openingDebit;
                                grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[1].Value = openingDebit;
                            }
                            else grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[1].Value = "";

                            openingCredit = calculatedopeningCredit;
                            if (openingCredit != 0)
                            {

                                if (grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[2].Value.ToString() != "")
                                {
                                    oCreditTotal -= decimal.Parse(grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[2].Value.ToString());
                                    //cCreditTotal -= decimal.Parse(grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[2].Value.ToString());
                                }

                                oCreditTotal += openingCredit;
                                rowCreditTotal += openingCredit;
                                grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[2].Value = openingCredit;
                            }
                            else grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[2].Value = "";

                            if (grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[3].Value.ToString() != "")
                                tDebit = decimal.Parse(grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[3].Value.ToString());
                            else
                                tDebit = 0;
                            if (tDebit != 0)
                            {
                                //TDebitTotal += tDebit;
                                rowDebitTotal += tDebit;
                            }

                            if (grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[4].Value.ToString() != "")
                                tCredit = decimal.Parse(grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[4].Value.ToString());
                            else
                                tCredit = 0;
                            if (tCredit != 0)
                            {
                                // TCreditTotal += tCredit;
                                rowCreditTotal += tCredit;
                            }

                            //MessageBox.Show("RowDebit: " + rowDebitTotal + "RowCredit: " + rowCreditTotal);

                            if (rowDebitTotal > rowCreditTotal)
                            {
                                if (grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[5].Value.ToString() != "")
                                {
                                    cDebitTotal -= decimal.Parse(grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[5].Value.ToString());
                                }
                                grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[5].Value = rowDebitTotal - rowCreditTotal;
                                grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[6].Value = "";
                                cDebitTotal += rowDebitTotal - rowCreditTotal;
                               // MessageBox.Show("GroupDebit: " + cDebitTotal + "GroupCredit: " + cCreditTotal);
                            }

                            else
                            {
                                if (rowCreditTotal - rowDebitTotal > 0)
                                {
                                    if (grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[6].Value.ToString() != "")
                                    {
                                        cCreditTotal -= decimal.Parse(grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[6].Value.ToString());
                                    }
                                    grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[6].Value = rowCreditTotal - rowDebitTotal;
                                    grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[5].Value = "";
                                    cCreditTotal += rowCreditTotal - rowDebitTotal;
                                    //MessageBox.Show("GroupDebit: " + cDebitTotal + "GroupCredit: " + cCreditTotal);
                                }
                                    
                                else grdTrialBalance.Rows[grdTrialBalance.RowCount - 1].Cells[6].Value = "";
                                
                                
                            }
                        } 
                        #endregion
                        else
                        #region Ledger are coming Normally with Transactions
                        {
                            DataGridViewRow row = (DataGridViewRow)grdTrialBalance.Rows[0].Clone();
                            row.Cells[0].Value = "          " + reader["LedgerName"].ToString();
                            openingDebit = decimal.Parse(reader["OpeningDebit"].ToString());

                            if (openingDebit != 0)
                            {
                                row.Cells[1].Value = openingDebit;
                                oDebitTotal += openingDebit;
                                rowDebitTotal += openingDebit;
                            }
                            else row.Cells[1].Value = "";
                            openingCredit = decimal.Parse(reader["OpeningCredit"].ToString());

                            if (openingCredit != 0)
                            {
                                row.Cells[2].Value = openingCredit;
                                oCreditTotal += openingCredit;
                                rowCreditTotal += openingCredit;
                            }
                            else row.Cells[2].Value = "";
                            if (reader["TType"].ToString() == "T")
                            {
                                tDebit = decimal.Parse(reader["TDebit"].ToString());
                                if (tDebit != 0)
                                {
                                    row.Cells[3].Value = tDebit;
                                    TDebitTotal += tDebit;
                                    rowDebitTotal += tDebit;
                                }
                                else row.Cells[3].Value = "";
                                tCredit = decimal.Parse(reader["TCredit"].ToString());
                                if (tCredit != 0)
                                {
                                    row.Cells[4].Value = tCredit;
                                    TCreditTotal += tCredit;
                                    rowCreditTotal += tCredit;
                                }
                                else row.Cells[4].Value = "";
                            }

                            if (rowDebitTotal > rowCreditTotal)
                            {
                                row.Cells[5].Value = rowDebitTotal - rowCreditTotal;
                                cDebitTotal += rowDebitTotal - rowCreditTotal;
                                row.Cells[6].Value = "";
                            }

                            else
                            {
                                if (rowCreditTotal - rowDebitTotal > 0)
                                {
                                    row.Cells[6].Value = rowCreditTotal - rowDebitTotal;
                                    row.Cells[5].Value = "";
                                    cCreditTotal += rowCreditTotal - rowDebitTotal;
                                }
                                    
                                else row.Cells[6].Value = "";
                                
                            }

                            grdTrialBalance.Rows.Add(row);

                        } 
                        #endregion

                        duplicateledger = int.Parse(reader["LedgerID"].ToString());
                        rowDebitTotal = 0;
                        rowCreditTotal = 0;
                    }
                    else
                    #region Group and Subgroup Total Calculation
                    {
                        DataGridViewRow row = (DataGridViewRow)grdTrialBalance.Rows[0].Clone();
                        if (level == 2)
                            row.Cells[0].Value = "     Sub Total :";
                        else
                            row.Cells[0].Value = "Group Total :";

                        row.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        if (oDebitTotal > 0)
                            row.Cells[1].Value = oDebitTotal;
                        else row.Cells[1].Value = "";
                        row.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        if (oCreditTotal > 0)
                            row.Cells[2].Value = oCreditTotal;
                        else row.Cells[2].Value = "";
                        row.Cells[2].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        if (TDebitTotal > 0)
                            row.Cells[3].Value = TDebitTotal;
                        else row.Cells[3].Value = "";
                        row.Cells[3].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        if (TCreditTotal > 0)
                            row.Cells[4].Value = TCreditTotal;
                        else row.Cells[4].Value = "";
                        row.Cells[4].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        row.Cells[5].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        if (cDebitTotal > 0)
                        {
                            row.Cells[5].Value = cDebitTotal;
                            cCreditTotal = 0;
                            row.Cells[6].Value = "";
                        }
                        else
                        {
                            row.Cells[5].Value = "";
                            if (cCreditTotal > 0)
                                row.Cells[6].Value = cCreditTotal;
                            else
                                row.Cells[6].Value = "";
                            cDebitTotal = 0;
                        }
                        row.Cells[6].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        grdTrialBalance.Rows.Add(row);
                        if (level == 2)
                        {
                            oDebitGTotal += oDebitTotal;
                            oCreditGTotal += oCreditTotal;
                            TDebitGTotal += TDebitTotal;
                            TCreditGTotal += TCreditTotal;
                            cDebitGTotal += cDebitTotal;
                            cCreditGTotal += cCreditTotal;

                        }

                        debittotal += cDebitTotal;
                        credittotal += cCreditTotal;

                    } 
                    #endregion
                }
                
            }
            else
            #region When No Ledger found under Group
            {
                reader.Close();
                connection.Close();
                int rowcount = grdTrialBalance.Rows.Count;
                if (level == 2)  //This Dipest Group Don't have any ledger, so Remove
                {

                    if (GropupLevel2.Count == 1)
                    {
                        if (rowcount > 1)
                        {
                            grdTrialBalance.Rows.RemoveAt(rowcount - 2);
                        }
                    }

                }

            } 
            #endregion
            if (GropupLevel2.Count > 1) //Have atleast One Ledger
            #region Main Group Total Calculation
            {
                if (subgroupcount == GropupLevel2.Count)
                {
                    DataGridViewRow row2 = (DataGridViewRow)grdTrialBalance.Rows[0].Clone();
                    row2.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    row2.Cells[0].Value = "Grand Group Total";
                    row2.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    if (oDebitGTotal > 0)
                        row2.Cells[1].Value = oDebitGTotal;
                    else row2.Cells[1].Value = "";
                    row2.Cells[2].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    if (oCreditGTotal > 0)
                        row2.Cells[2].Value = oCreditGTotal;
                    else row2.Cells[2].Value = "";
                    row2.Cells[3].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    if (TDebitGTotal > 0)
                        row2.Cells[3].Value = TDebitGTotal;
                    else row2.Cells[3].Value = "";
                    row2.Cells[4].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    if (TCreditGTotal > 0)
                        row2.Cells[4].Value = TCreditGTotal;
                    else row2.Cells[4].Value = "";
                    row2.Cells[5].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    if (cDebitGTotal > cCreditGTotal)
                    {
                        row2.Cells[5].Value = cDebitGTotal - cCreditGTotal;
                        row2.Cells[6].Value = "";
                    }
                    else
                    {
                        if (cCreditGTotal - cDebitGTotal > 0)
                        {
                            row2.Cells[6].Value = cCreditGTotal - cDebitGTotal;
                            row2.Cells[5].Value = "";
                        }
                        else row2.Cells[6].Value = "";
                    }
                    row2.Cells[6].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    
                    grdTrialBalance.Rows.Add(row2);

                    oDebitGTotal = 0;
                    oCreditGTotal = 0;
                    TDebitGTotal = 0;
                    TCreditGTotal = 0;
                    cDebitGTotal = 0;
                    cCreditGTotal = 0;
                }
            } 
            #endregion


            connection.Close();
            #endregion
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            string sql2 = null;
            Main mainform = new Main();
            DateTime startperiod = mainform.getStartPeriod;
            DateTime startdate =DateTime.Parse(dtpOpenningDate.Value.ToString());
            sql += "ALTER View [View_Closing_Balance] as ";
            sql += " SELECT dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, dbo.Ledgers.LedgerName, SUM(TempVoucher.DebitAmount) AS TDebit, SUM(TempVoucher.CreditAmount) AS TCredit, dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status";
            sql += " FROM  (SELECT  dbo.Voucher_Contents.VoucherNo, dbo.Voucher_Contents.DebitAmount, dbo.Voucher_Contents.CreditAmount, dbo.Voucher_Contents.LedgerID ";
            sql += "  FROM   dbo.Voucher_Contents INNER JOIN dbo.Vouchers ON dbo.Voucher_Contents.VoucherNo = dbo.Vouchers.VoucherNo ";
            sql += " WHERE ( dbo.Vouchers.PostingDate <='" + dtpOpenningDate.Value.ToString("yyyy-MM-dd") + "')) AS TempVoucher INNER JOIN dbo.Ledgers  ON dbo.Ledgers.LedgerID = TempVoucher.LedgerID ";
            sql += "   GROUP BY dbo.Ledgers.LedgerName, dbo.Ledgers.LedgerID, dbo.Ledgers.Status, dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, dbo.Ledgers.GroupID";

            
            sql2 += "ALTER View [View_Periodic_Trail_Balance_2] as ";
            sql2 += " Select dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, dbo.Ledgers.LedgerName, SUM(dbo.Voucher_Contents.DebitAmount) AS TDebit, SUM(dbo.Voucher_Contents.CreditAmount) AS TCredit, dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status, 'T' as TType ";
            sql2 += " FROM            dbo.Voucher_Contents INNER JOIN  dbo.Ledgers ON dbo.Voucher_Contents.LedgerID = dbo.Ledgers.LedgerID  ";
            sql2 += " WHERE        (dbo.Voucher_Contents.PostingDate BETWEEN '" + dtpOpenningDate.Value.ToString("yyyy-MM-dd") + "' AND '" + dtpClosingDate.Value.ToString("yyyy-MM-dd") + "') ";
            sql2 += " GROUP BY dbo.Ledgers.LedgerName,dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status, dbo.Ledgers.OpeningDebit,dbo.Ledgers.OpeningCredit  ";
            sql2 += "HAVING        (dbo.Ledgers.Status = 1)";
            sql2 += " UNION ALL ";
            if (startdate <= startperiod)
            {
                sql2 += "Select dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, ";
                sql2 += " dbo.Ledgers.LedgerName, 0 as TDebit, 0 as TCredit, dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status, 'C' as TType ";
                sql2 += " from dbo.Ledgers ";
                sql2 += " Where  ((dbo.Ledgers.OpeningDebit>0 or dbo.Ledgers.OpeningCredit>0) and Status=1 and LedgerID!=57)";
            }
            else
            {
                sql2 += "Select CASE WHEN dbo.View_Closing_Balance.OpeningDebit + dbo.View_Closing_Balance.TDebit > dbo.View_Closing_Balance.OpeningCredit + dbo.View_Closing_Balance.TCredit THEN (dbo.View_Closing_Balance.OpeningDebit + dbo.View_Closing_Balance.TDebit) - (dbo.View_Closing_Balance.OpeningCredit + dbo.View_Closing_Balance.TCredit) ELSE 0 END AS OpeningDebit, ";
                sql2 += "       CASE WHEN dbo.View_Closing_Balance.OpeningDebit + dbo.View_Closing_Balance.TDebit < dbo.View_Closing_Balance.OpeningCredit + dbo.View_Closing_Balance.TCredit THEN (dbo.View_Closing_Balance.OpeningCredit+ dbo.View_Closing_Balance.TCredit) - (dbo.View_Closing_Balance.OpeningDebit + dbo.View_Closing_Balance.TDebit) ELSE 0 END AS OpeningCredit, ";
                sql2 += " dbo.View_Closing_Balance.LedgerName,  dbo.View_Closing_Balance.TDebit, dbo.View_Closing_Balance.TCredit, dbo.View_Closing_Balance.LedgerID, dbo.View_Closing_Balance.GroupID, dbo.View_Closing_Balance.Status, 'C' as TType ";
                sql2 += " from dbo.View_Closing_Balance ";
                sql2 += " Group by dbo.View_Closing_Balance.OpeningDebit, dbo.View_Closing_Balance.TDebit, dbo.View_Closing_Balance.OpeningCredit, dbo.View_Closing_Balance.TCredit, dbo.View_Closing_Balance.LedgerName, dbo.View_Closing_Balance.Status, dbo.View_Closing_Balance.GroupID, dbo.View_Closing_Balance.LedgerID";
                sql2 += " Having  ((dbo.View_Closing_Balance.OpeningDebit + dbo.View_Closing_Balance.TDebit) - (dbo.View_Closing_Balance.OpeningCredit + dbo.View_Closing_Balance.TCredit)>0 or (dbo.View_Closing_Balance.OpeningCredit + dbo.View_Closing_Balance.TCredit) - (dbo.View_Closing_Balance.OpeningDebit + dbo.View_Closing_Balance.TDebit)>0)";
            }
           

            adapter.InsertCommand = new SqlCommand(sql, connection);
            connection.Open();
            
            adapter.InsertCommand.ExecuteNonQuery();
            adapter.InsertCommand = new SqlCommand(sql2, connection);
            adapter.InsertCommand.ExecuteNonQuery();
            connection.Close();

            reporttype = "periodic";
            grdTrialBalance.Rows.Clear();
            GropupLevel1.Clear();
            GropupLevel2.Clear();
            debittotal = 0;
            credittotal = 0;
            int parentid = 0;
            string parentname = "";
            
            sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = 1;
            SqlDataReader reader1 = adapter.SelectCommand.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    parentname = reader1["GroupName"].ToString();
                    parentid = int.Parse(reader1["GroupID"].ToString());
                    Parent parent = new Parent();
                    parent.Id = parentid;
                    parent.Name = parentname;
                    GropupLevel1.Add(parent);
                }
            }

            connection.Close();
            foreach (Parent items in GropupLevel1)
            {
                LoadSubGroups(items, 2);
            }


            lblDebitTotal.Text = debittotal.ToString("0.00");
            lblCreditTotal.Text = credittotal.ToString("0.00");

            grdTrialBalance.AllowUserToAddRows = false;
        }

        private void grdTrialBalance_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DateTime startdate = DateTime.Parse(dtpOpenningDate.Value.ToString());
            DateTime enddate = DateTime.Parse(dtpClosingDate.Value.ToString());
            //Main mainform = new Main();
            //this.Close();

            //mainform.LoadLedgerReport(35,startdate,enddate);
            //this.IsMdiContainer = true;
            //PeriodicLedger objForm = new PeriodicLedger(36, startdate, enddate);
            //this.Close();
            //objForm.Parent= this.Parent;
            //objForm.Show();

            //PeriodicLedger objForm = new PeriodicLedger(36, startdate, enddate);
            //objForm.TopLevel = false;
            //Panel pnlPlayground;
            //if (pnlPlayground.Controls.Count > 0)
            //    pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.Controls.Add(objForm);

            //objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //objForm.Dock = DockStyle.None;
            //objForm.Location = new Point(
            // this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
            // this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            //objForm.Anchor = AnchorStyles.None;
            //objForm.Show();
        }

        private void dtpOpenningDate_ValueChanged(object sender, EventArgs e)
        {
            dtpClosingDate.Value = dtpOpenningDate.Value.AddDays(+30);
        }
    }
       public class Parent
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
    }
    
}
