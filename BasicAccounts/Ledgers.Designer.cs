﻿namespace BasicAccounts
{
    partial class Ledgers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ledgers));
            CustomControl.Office2010White office2010White2 = new CustomControl.Office2010White();
            this.LedgerTree = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.sdsLedgers = new System.Windows.Forms.BindingSource(this.components);
            this.dataSetAccounts = new BasicAccounts.DataSetAccounts();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.chkLedgerStatus = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ddlGroups = new System.Windows.Forms.ComboBox();
            this.groupsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.chkFixedAsset = new System.Windows.Forms.CheckBox();
            this.ddlDrCr = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.chkRecievable = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.chkServiceAffect = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.chkInventory = new System.Windows.Forms.CheckBox();
            this.chkMainTainBill = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.LedgerTab = new System.Windows.Forms.TabControl();
            this.tabBasic = new System.Windows.Forms.TabPage();
            this.dtpAddDate = new System.Windows.Forms.DateTimePicker();
            this.txtEmail = new CustomControl.XTextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtPhone = new CustomControl.XTextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtCPerson = new CustomControl.XTextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtOpeningBalance = new CustomControl.XTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtLedgerID = new CustomControl.XTextBox();
            this.txtCity = new CustomControl.XTextBox();
            this.txtLedgerName = new CustomControl.XTextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtAddress = new CustomControl.XTextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.tabAdv = new System.Windows.Forms.TabPage();
            this.txtCreditLimit = new CustomControl.XTextBox();
            this.xButton3 = new CustomControl.XButton();
            this.xButton2 = new CustomControl.XButton();
            this.xButton1 = new CustomControl.XButton();
            this.btnAddnew = new CustomControl.XButton();
            this.label26 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.xComboBox1 = new CustomControl.XComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.sdsLedgers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupsBindingSource)).BeginInit();
            this.panel3.SuspendLayout();
            this.LedgerTab.SuspendLayout();
            this.tabBasic.SuspendLayout();
            this.tabAdv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // LedgerTree
            // 
            this.LedgerTree.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LedgerTree.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LedgerTree.ImageIndex = 0;
            this.LedgerTree.ImageList = this.imageList1;
            this.LedgerTree.Location = new System.Drawing.Point(378, 14);
            this.LedgerTree.Name = "LedgerTree";
            this.LedgerTree.SelectedImageIndex = 1;
            this.LedgerTree.Size = new System.Drawing.Size(294, 340);
            this.LedgerTree.TabIndex = 25;
            this.LedgerTree.Click += new System.EventHandler(this.LedgerTree_Click_1);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "groups.png");
            this.imageList1.Images.SetKeyName(1, "Ledger1.jpg");
            // 
            // sdsLedgers
            // 
            this.sdsLedgers.DataMember = "Ledgers";
            this.sdsLedgers.DataSource = this.dataSetAccounts;
            // 
            // dataSetAccounts
            // 
            this.dataSetAccounts.DataSetName = "DataSetAccounts";
            this.dataSetAccounts.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Ledger ID";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "Ledger Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 280);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "Active";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(135, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = ":";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(135, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(11, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = ":";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(134, 281);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 16);
            this.label8.TabIndex = 7;
            this.label8.Text = "?";
            // 
            // chkLedgerStatus
            // 
            this.chkLedgerStatus.Checked = true;
            this.chkLedgerStatus.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLedgerStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLedgerStatus.Location = new System.Drawing.Point(155, 280);
            this.chkLedgerStatus.Name = "chkLedgerStatus";
            this.chkLedgerStatus.Size = new System.Drawing.Size(80, 20);
            this.chkLedgerStatus.TabIndex = 12;
            this.chkLedgerStatus.Text = "Yes / No";
            this.chkLedgerStatus.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 16);
            this.label1.TabIndex = 10;
            this.label1.Text = "Group Name";
            // 
            // ddlGroups
            // 
            this.ddlGroups.BackColor = System.Drawing.SystemColors.Window;
            this.ddlGroups.DataSource = this.groupsBindingSource;
            this.ddlGroups.DisplayMember = "GroupName";
            this.ddlGroups.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ddlGroups.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlGroups.FormattingEnabled = true;
            this.ddlGroups.ItemHeight = 15;
            this.ddlGroups.Location = new System.Drawing.Point(378, 62);
            this.ddlGroups.Name = "ddlGroups";
            this.ddlGroups.Size = new System.Drawing.Size(180, 23);
            this.ddlGroups.TabIndex = 3;
            this.ddlGroups.ValueMember = "GroupID";
            // 
            // groupsBindingSource
            // 
            this.groupsBindingSource.DataMember = "Groups";
            this.groupsBindingSource.DataSource = this.dataSetAccounts;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(135, 59);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(11, 16);
            this.label9.TabIndex = 12;
            this.label9.Text = ":";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 12);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(132, 16);
            this.label11.TabIndex = 13;
            this.label11.Text = "Maintain Balance Bill";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(7, 35);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(116, 16);
            this.label13.TabIndex = 17;
            this.label13.Text = "Inventory Enabled";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(7, 59);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(114, 16);
            this.label15.TabIndex = 20;
            this.label15.Text = "Fixed Asset Affect";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(141, 60);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 16);
            this.label14.TabIndex = 22;
            this.label14.Text = "?";
            // 
            // chkFixedAsset
            // 
            this.chkFixedAsset.Checked = true;
            this.chkFixedAsset.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFixedAsset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFixedAsset.Location = new System.Drawing.Point(159, 60);
            this.chkFixedAsset.Name = "chkFixedAsset";
            this.chkFixedAsset.Size = new System.Drawing.Size(80, 20);
            this.chkFixedAsset.TabIndex = 15;
            this.chkFixedAsset.Text = "Yes / No";
            this.chkFixedAsset.UseVisualStyleBackColor = true;
            // 
            // ddlDrCr
            // 
            this.ddlDrCr.BackColor = System.Drawing.SystemColors.Window;
            this.ddlDrCr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ddlDrCr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlDrCr.FormattingEnabled = true;
            this.ddlDrCr.ItemHeight = 15;
            this.ddlDrCr.Location = new System.Drawing.Point(290, 254);
            this.ddlDrCr.Name = "ddlDrCr";
            this.ddlDrCr.Size = new System.Drawing.Size(42, 23);
            this.ddlDrCr.TabIndex = 11;
            this.ddlDrCr.Tag = "";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(135, 256);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(11, 16);
            this.label22.TabIndex = 39;
            this.label22.Text = ":";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(8, 256);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(112, 16);
            this.label23.TabIndex = 38;
            this.label23.Text = "Opening Balance";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(144, 131);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(11, 16);
            this.label20.TabIndex = 36;
            this.label20.Text = ":";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(7, 131);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 16);
            this.label21.TabIndex = 35;
            this.label21.Text = "Credit Limit";
            // 
            // chkRecievable
            // 
            this.chkRecievable.Checked = true;
            this.chkRecievable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRecievable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRecievable.Location = new System.Drawing.Point(159, 105);
            this.chkRecievable.Name = "chkRecievable";
            this.chkRecievable.Size = new System.Drawing.Size(80, 20);
            this.chkRecievable.TabIndex = 17;
            this.chkRecievable.Text = "Yes / No";
            this.chkRecievable.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(140, 107);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(15, 16);
            this.label18.TabIndex = 32;
            this.label18.Text = "?";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(7, 107);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(91, 16);
            this.label19.TabIndex = 31;
            this.label19.Text = "Is Recievable";
            // 
            // chkServiceAffect
            // 
            this.chkServiceAffect.Checked = true;
            this.chkServiceAffect.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkServiceAffect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkServiceAffect.Location = new System.Drawing.Point(160, 82);
            this.chkServiceAffect.Name = "chkServiceAffect";
            this.chkServiceAffect.Size = new System.Drawing.Size(80, 20);
            this.chkServiceAffect.TabIndex = 16;
            this.chkServiceAffect.Text = "Yes / No";
            this.chkServiceAffect.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(141, 84);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(15, 16);
            this.label17.TabIndex = 29;
            this.label17.Text = "?";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(141, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 16);
            this.label10.TabIndex = 28;
            this.label10.Text = "?";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(141, 40);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 16);
            this.label12.TabIndex = 27;
            this.label12.Text = "?";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(8, 84);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(90, 16);
            this.label16.TabIndex = 26;
            this.label16.Text = "Service Affect";
            // 
            // chkInventory
            // 
            this.chkInventory.Checked = true;
            this.chkInventory.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkInventory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInventory.Location = new System.Drawing.Point(160, 36);
            this.chkInventory.Name = "chkInventory";
            this.chkInventory.Size = new System.Drawing.Size(80, 20);
            this.chkInventory.TabIndex = 14;
            this.chkInventory.Text = "Yes / No";
            this.chkInventory.UseVisualStyleBackColor = true;
            // 
            // chkMainTainBill
            // 
            this.chkMainTainBill.Checked = true;
            this.chkMainTainBill.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMainTainBill.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.chkMainTainBill.FlatAppearance.CheckedBackColor = System.Drawing.Color.Black;
            this.chkMainTainBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMainTainBill.Location = new System.Drawing.Point(160, 13);
            this.chkMainTainBill.Name = "chkMainTainBill";
            this.chkMainTainBill.Size = new System.Drawing.Size(77, 20);
            this.chkMainTainBill.TabIndex = 13;
            this.chkMainTainBill.Text = "Yes / No";
            this.chkMainTainBill.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.GhostWhite;
            this.panel3.Controls.Add(this.LedgerTab);
            this.panel3.Controls.Add(this.xButton3);
            this.panel3.Controls.Add(this.xButton2);
            this.panel3.Controls.Add(this.xButton1);
            this.panel3.Controls.Add(this.btnAddnew);
            this.panel3.Controls.Add(this.LedgerTree);
            this.panel3.Controls.Add(this.ddlGroups);
            this.panel3.Location = new System.Drawing.Point(0, 20);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(688, 373);
            this.panel3.TabIndex = 8;
            // 
            // LedgerTab
            // 
            this.LedgerTab.Controls.Add(this.tabBasic);
            this.LedgerTab.Controls.Add(this.tabAdv);
            this.LedgerTab.Location = new System.Drawing.Point(8, -1);
            this.LedgerTab.Name = "LedgerTab";
            this.LedgerTab.SelectedIndex = 0;
            this.LedgerTab.Size = new System.Drawing.Size(356, 334);
            this.LedgerTab.TabIndex = 0;
            // 
            // tabBasic
            // 
            this.tabBasic.BackColor = System.Drawing.Color.GhostWhite;
            this.tabBasic.Controls.Add(this.xComboBox1);
            this.tabBasic.Controls.Add(this.dtpAddDate);
            this.tabBasic.Controls.Add(this.txtEmail);
            this.tabBasic.Controls.Add(this.label34);
            this.tabBasic.Controls.Add(this.label35);
            this.tabBasic.Controls.Add(this.txtPhone);
            this.tabBasic.Controls.Add(this.label32);
            this.tabBasic.Controls.Add(this.txtCPerson);
            this.tabBasic.Controls.Add(this.label33);
            this.tabBasic.Controls.Add(this.txtOpeningBalance);
            this.tabBasic.Controls.Add(this.label30);
            this.tabBasic.Controls.Add(this.label31);
            this.tabBasic.Controls.Add(this.txtLedgerID);
            this.tabBasic.Controls.Add(this.txtCity);
            this.tabBasic.Controls.Add(this.ddlDrCr);
            this.tabBasic.Controls.Add(this.txtLedgerName);
            this.tabBasic.Controls.Add(this.label28);
            this.tabBasic.Controls.Add(this.label29);
            this.tabBasic.Controls.Add(this.label22);
            this.tabBasic.Controls.Add(this.txtAddress);
            this.tabBasic.Controls.Add(this.label25);
            this.tabBasic.Controls.Add(this.label23);
            this.tabBasic.Controls.Add(this.label27);
            this.tabBasic.Controls.Add(this.label5);
            this.tabBasic.Controls.Add(this.label2);
            this.tabBasic.Controls.Add(this.label8);
            this.tabBasic.Controls.Add(this.label24);
            this.tabBasic.Controls.Add(this.chkLedgerStatus);
            this.tabBasic.Controls.Add(this.label4);
            this.tabBasic.Controls.Add(this.label9);
            this.tabBasic.Controls.Add(this.label3);
            this.tabBasic.Controls.Add(this.label1);
            this.tabBasic.Controls.Add(this.label7);
            this.tabBasic.Controls.Add(this.label6);
            this.tabBasic.Location = new System.Drawing.Point(4, 22);
            this.tabBasic.Name = "tabBasic";
            this.tabBasic.Padding = new System.Windows.Forms.Padding(3);
            this.tabBasic.Size = new System.Drawing.Size(348, 308);
            this.tabBasic.TabIndex = 0;
            this.tabBasic.Text = "Basic";
            // 
            // dtpAddDate
            // 
            this.dtpAddDate.CustomFormat = "dd/MM/yyyy";
            this.dtpAddDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAddDate.Location = new System.Drawing.Point(154, 86);
            this.dtpAddDate.Name = "dtpAddDate";
            this.dtpAddDate.Size = new System.Drawing.Size(180, 20);
            this.dtpAddDate.TabIndex = 4;
            // 
            // txtEmail
            // 
            this.txtEmail.ColorTable = office2010White2;
            this.txtEmail.ForeColor = System.Drawing.SystemColors.Window;
            this.txtEmail.Location = new System.Drawing.Point(155, 229);
            this.txtEmail.Multiline = false;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Padding = new System.Windows.Forms.Padding(2);
            this.txtEmail.ReadOnly = false;
            this.txtEmail.Size = new System.Drawing.Size(180, 21);
            this.txtEmail.TabIndex = 9;
            this.txtEmail.Theme = CustomControl.Theme.MSOffice2010_WHITE;
            this.txtEmail.WaterMark = "";
            this.txtEmail.WaterMarkActiveForeColor = System.Drawing.Color.Gray;
            this.txtEmail.WaterMarkFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.WaterMarkForeColor = System.Drawing.Color.LightGray;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(9, 229);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(42, 16);
            this.label34.TabIndex = 122;
            this.label34.Text = "Email";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(136, 229);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(11, 16);
            this.label35.TabIndex = 123;
            this.label35.Text = ":";
            // 
            // txtPhone
            // 
            this.txtPhone.ColorTable = office2010White2;
            this.txtPhone.ForeColor = System.Drawing.SystemColors.Window;
            this.txtPhone.Location = new System.Drawing.Point(155, 204);
            this.txtPhone.Multiline = false;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Padding = new System.Windows.Forms.Padding(2);
            this.txtPhone.ReadOnly = false;
            this.txtPhone.Size = new System.Drawing.Size(180, 21);
            this.txtPhone.TabIndex = 8;
            this.txtPhone.Theme = CustomControl.Theme.MSOffice2010_WHITE;
            this.txtPhone.WaterMark = "";
            this.txtPhone.WaterMarkActiveForeColor = System.Drawing.Color.Gray;
            this.txtPhone.WaterMarkFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhone.WaterMarkForeColor = System.Drawing.Color.LightGray;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(9, 204);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(104, 16);
            this.label32.TabIndex = 119;
            this.label32.Text = "Contact Number";
            // 
            // txtCPerson
            // 
            this.txtCPerson.ColorTable = office2010White2;
            this.txtCPerson.ForeColor = System.Drawing.SystemColors.Window;
            this.txtCPerson.Location = new System.Drawing.Point(155, 180);
            this.txtCPerson.Multiline = false;
            this.txtCPerson.Name = "txtCPerson";
            this.txtCPerson.Padding = new System.Windows.Forms.Padding(2);
            this.txtCPerson.ReadOnly = false;
            this.txtCPerson.Size = new System.Drawing.Size(180, 21);
            this.txtCPerson.TabIndex = 7;
            this.txtCPerson.Theme = CustomControl.Theme.MSOffice2010_WHITE;
            this.txtCPerson.WaterMark = "";
            this.txtCPerson.WaterMarkActiveForeColor = System.Drawing.Color.Gray;
            this.txtCPerson.WaterMarkFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCPerson.WaterMarkForeColor = System.Drawing.Color.LightGray;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(136, 204);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(11, 16);
            this.label33.TabIndex = 120;
            this.label33.Text = ":";
            // 
            // txtOpeningBalance
            // 
            this.txtOpeningBalance.ColorTable = office2010White2;
            this.txtOpeningBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOpeningBalance.ForeColor = System.Drawing.SystemColors.Window;
            this.txtOpeningBalance.Location = new System.Drawing.Point(155, 254);
            this.txtOpeningBalance.Multiline = false;
            this.txtOpeningBalance.Name = "txtOpeningBalance";
            this.txtOpeningBalance.Padding = new System.Windows.Forms.Padding(2);
            this.txtOpeningBalance.ReadOnly = false;
            this.txtOpeningBalance.Size = new System.Drawing.Size(131, 21);
            this.txtOpeningBalance.TabIndex = 10;
            this.txtOpeningBalance.Theme = CustomControl.Theme.MSOffice2010_WHITE;
            this.txtOpeningBalance.WaterMark = "";
            this.txtOpeningBalance.WaterMarkActiveForeColor = System.Drawing.Color.Gray;
            this.txtOpeningBalance.WaterMarkFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOpeningBalance.WaterMarkForeColor = System.Drawing.Color.LightGray;
            this.txtOpeningBalance.Leave += new System.EventHandler(this.txtOpeningBalance_Leave);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(8, 180);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(99, 16);
            this.label30.TabIndex = 116;
            this.label30.Text = "Contact Person";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(135, 180);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(11, 16);
            this.label31.TabIndex = 117;
            this.label31.Text = ":";
            // 
            // txtLedgerID
            // 
            this.txtLedgerID.ColorTable = office2010White2;
            this.txtLedgerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLedgerID.ForeColor = System.Drawing.SystemColors.Window;
            this.txtLedgerID.Location = new System.Drawing.Point(155, 8);
            this.txtLedgerID.Multiline = false;
            this.txtLedgerID.Name = "txtLedgerID";
            this.txtLedgerID.Padding = new System.Windows.Forms.Padding(2);
            this.txtLedgerID.ReadOnly = true;
            this.txtLedgerID.Size = new System.Drawing.Size(180, 21);
            this.txtLedgerID.TabIndex = 1;
            this.txtLedgerID.Theme = CustomControl.Theme.MSOffice2010_WHITE;
            this.txtLedgerID.WaterMark = "";
            this.txtLedgerID.WaterMarkActiveForeColor = System.Drawing.Color.Gray;
            this.txtLedgerID.WaterMarkFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLedgerID.WaterMarkForeColor = System.Drawing.Color.LightGray;
            // 
            // txtCity
            // 
            this.txtCity.ColorTable = office2010White2;
            this.txtCity.ForeColor = System.Drawing.SystemColors.Window;
            this.txtCity.Location = new System.Drawing.Point(155, 157);
            this.txtCity.Multiline = false;
            this.txtCity.Name = "txtCity";
            this.txtCity.Padding = new System.Windows.Forms.Padding(2);
            this.txtCity.ReadOnly = false;
            this.txtCity.Size = new System.Drawing.Size(180, 21);
            this.txtCity.TabIndex = 6;
            this.txtCity.Text = "City";
            this.txtCity.Theme = CustomControl.Theme.MSOffice2010_WHITE;
            this.txtCity.WaterMark = "";
            this.txtCity.WaterMarkActiveForeColor = System.Drawing.Color.Gray;
            this.txtCity.WaterMarkFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCity.WaterMarkForeColor = System.Drawing.Color.LightGray;
            // 
            // txtLedgerName
            // 
            this.txtLedgerName.ColorTable = office2010White2;
            this.txtLedgerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLedgerName.ForeColor = System.Drawing.SystemColors.Window;
            this.txtLedgerName.Location = new System.Drawing.Point(155, 32);
            this.txtLedgerName.Multiline = false;
            this.txtLedgerName.Name = "txtLedgerName";
            this.txtLedgerName.Padding = new System.Windows.Forms.Padding(2);
            this.txtLedgerName.ReadOnly = false;
            this.txtLedgerName.Size = new System.Drawing.Size(180, 21);
            this.txtLedgerName.TabIndex = 2;
            this.txtLedgerName.Theme = CustomControl.Theme.MSOffice2010_WHITE;
            this.txtLedgerName.WaterMark = "";
            this.txtLedgerName.WaterMarkActiveForeColor = System.Drawing.Color.Gray;
            this.txtLedgerName.WaterMarkFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLedgerName.WaterMarkForeColor = System.Drawing.Color.LightGray;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(8, 156);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(30, 16);
            this.label28.TabIndex = 113;
            this.label28.Text = "City";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(135, 157);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(11, 16);
            this.label29.TabIndex = 114;
            this.label29.Text = ":";
            // 
            // txtAddress
            // 
            this.txtAddress.ColorTable = office2010White2;
            this.txtAddress.ForeColor = System.Drawing.SystemColors.Window;
            this.txtAddress.Location = new System.Drawing.Point(155, 111);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Padding = new System.Windows.Forms.Padding(2);
            this.txtAddress.ReadOnly = false;
            this.txtAddress.Size = new System.Drawing.Size(180, 42);
            this.txtAddress.TabIndex = 5;
            this.txtAddress.Theme = CustomControl.Theme.MSOffice2010_WHITE;
            this.txtAddress.WaterMark = "";
            this.txtAddress.WaterMarkActiveForeColor = System.Drawing.Color.Gray;
            this.txtAddress.WaterMarkFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddress.WaterMarkForeColor = System.Drawing.Color.LightGray;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(8, 111);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(59, 16);
            this.label25.TabIndex = 110;
            this.label25.Text = "Address";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(135, 111);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(11, 16);
            this.label27.TabIndex = 111;
            this.label27.Text = ":";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 16);
            this.label2.TabIndex = 107;
            this.label2.Text = "Customar Add Date";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(135, 87);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(11, 16);
            this.label24.TabIndex = 108;
            this.label24.Text = ":";
            // 
            // tabAdv
            // 
            this.tabAdv.BackColor = System.Drawing.Color.GhostWhite;
            this.tabAdv.Controls.Add(this.label11);
            this.tabAdv.Controls.Add(this.txtCreditLimit);
            this.tabAdv.Controls.Add(this.label13);
            this.tabAdv.Controls.Add(this.chkRecievable);
            this.tabAdv.Controls.Add(this.label15);
            this.tabAdv.Controls.Add(this.label18);
            this.tabAdv.Controls.Add(this.label14);
            this.tabAdv.Controls.Add(this.label20);
            this.tabAdv.Controls.Add(this.label19);
            this.tabAdv.Controls.Add(this.label21);
            this.tabAdv.Controls.Add(this.chkFixedAsset);
            this.tabAdv.Controls.Add(this.chkServiceAffect);
            this.tabAdv.Controls.Add(this.chkMainTainBill);
            this.tabAdv.Controls.Add(this.label17);
            this.tabAdv.Controls.Add(this.chkInventory);
            this.tabAdv.Controls.Add(this.label10);
            this.tabAdv.Controls.Add(this.label16);
            this.tabAdv.Controls.Add(this.label12);
            this.tabAdv.Location = new System.Drawing.Point(4, 22);
            this.tabAdv.Name = "tabAdv";
            this.tabAdv.Padding = new System.Windows.Forms.Padding(3);
            this.tabAdv.Size = new System.Drawing.Size(348, 308);
            this.tabAdv.TabIndex = 1;
            this.tabAdv.Text = "Advance";
            // 
            // txtCreditLimit
            // 
            this.txtCreditLimit.ColorTable = office2010White2;
            this.txtCreditLimit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditLimit.Location = new System.Drawing.Point(160, 132);
            this.txtCreditLimit.Multiline = false;
            this.txtCreditLimit.Name = "txtCreditLimit";
            this.txtCreditLimit.Padding = new System.Windows.Forms.Padding(2);
            this.txtCreditLimit.ReadOnly = false;
            this.txtCreditLimit.Size = new System.Drawing.Size(180, 21);
            this.txtCreditLimit.TabIndex = 18;
            this.txtCreditLimit.Theme = CustomControl.Theme.MSOffice2010_WHITE;
            this.txtCreditLimit.WaterMark = "Default Watermark...";
            this.txtCreditLimit.WaterMarkActiveForeColor = System.Drawing.Color.Gray;
            this.txtCreditLimit.WaterMarkFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditLimit.WaterMarkForeColor = System.Drawing.Color.LightGray;
            this.txtCreditLimit.Leave += new System.EventHandler(this.txtCreditLimit_Leave);
            // 
            // xButton3
            // 
            this.xButton3.ColorTable = office2010White2;
            this.xButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xButton3.Image = ((System.Drawing.Image)(resources.GetObject("xButton3.Image")));
            this.xButton3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.xButton3.Location = new System.Drawing.Point(285, 339);
            this.xButton3.Name = "xButton3";
            this.xButton3.Size = new System.Drawing.Size(75, 23);
            this.xButton3.TabIndex = 22;
            this.xButton3.Text = "CLEAR";
            this.xButton3.Theme = CustomControl.Theme.MSOffice2010_WHITE;
            this.xButton3.UseVisualStyleBackColor = true;
            this.xButton3.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // xButton2
            // 
            this.xButton2.ColorTable = office2010White2;
            this.xButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xButton2.Image = ((System.Drawing.Image)(resources.GetObject("xButton2.Image")));
            this.xButton2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.xButton2.Location = new System.Drawing.Point(193, 339);
            this.xButton2.Name = "xButton2";
            this.xButton2.Size = new System.Drawing.Size(75, 23);
            this.xButton2.TabIndex = 21;
            this.xButton2.Text = "DELETE";
            this.xButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.xButton2.Theme = CustomControl.Theme.MSOffice2010_WHITE;
            this.xButton2.UseVisualStyleBackColor = true;
            this.xButton2.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // xButton1
            // 
            this.xButton1.ColorTable = office2010White2;
            this.xButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xButton1.Image = ((System.Drawing.Image)(resources.GetObject("xButton1.Image")));
            this.xButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.xButton1.Location = new System.Drawing.Point(102, 339);
            this.xButton1.Name = "xButton1";
            this.xButton1.Size = new System.Drawing.Size(75, 23);
            this.xButton1.TabIndex = 20;
            this.xButton1.Text = "EDIT";
            this.xButton1.Theme = CustomControl.Theme.MSOffice2010_WHITE;
            this.xButton1.UseVisualStyleBackColor = true;
            this.xButton1.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnAddnew
            // 
            this.btnAddnew.ColorTable = office2010White2;
            this.btnAddnew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddnew.Image = ((System.Drawing.Image)(resources.GetObject("btnAddnew.Image")));
            this.btnAddnew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddnew.Location = new System.Drawing.Point(10, 339);
            this.btnAddnew.Name = "btnAddnew";
            this.btnAddnew.Size = new System.Drawing.Size(75, 23);
            this.btnAddnew.TabIndex = 19;
            this.btnAddnew.Text = "SAVE";
            this.btnAddnew.Theme = CustomControl.Theme.MSOffice2010_WHITE;
            this.btnAddnew.UseVisualStyleBackColor = true;
            this.btnAddnew.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.SystemColors.Control;
            this.label26.Location = new System.Drawing.Point(1, 2);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(58, 16);
            this.label26.TabIndex = 9;
            this.label26.Text = "Ledgers";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(666, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 12);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // xComboBox1
            // 
            office2010White2.BorderColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            office2010White2.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            office2010White2.ButtonMouseOverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            office2010White2.ButtonMouseOverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            office2010White2.ButtonMouseOverColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(225)))), ((int)(((byte)(137)))));
            office2010White2.ButtonMouseOverColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(249)))), ((int)(((byte)(224)))));
            office2010White2.ButtonNormalColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(154)))), ((int)(((byte)(154)))));
            office2010White2.ButtonNormalColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            office2010White2.ButtonNormalColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            office2010White2.ButtonNormalColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            office2010White2.ButtonSelectedColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            office2010White2.ButtonSelectedColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            office2010White2.ButtonSelectedColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(229)))), ((int)(((byte)(117)))));
            office2010White2.ButtonSelectedColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(216)))), ((int)(((byte)(107)))));
            office2010White2.HoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            office2010White2.SelectedTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            office2010White2.TextColor = System.Drawing.Color.Black;
            this.xComboBox1.ColorTable = office2010White2;
            this.xComboBox1.CustomWidth = 176;
            this.xComboBox1.DropDownWidth = 180;
            this.xComboBox1.FormattingEnabled = true;
            this.xComboBox1.Location = new System.Drawing.Point(155, 58);
            this.xComboBox1.Name = "xComboBox1";
            this.xComboBox1.Size = new System.Drawing.Size(180, 24);
            this.xComboBox1.TabIndex = 26;
            this.xComboBox1.Theme = CustomControl.Theme.MSOffice2010_WHITE;
            // 
            // Ledgers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(173)))), ((int)(((byte)(167)))));
            this.ClientSize = new System.Drawing.Size(687, 392);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.panel3);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Ledgers";
            this.Text = "Ledgers";
            this.Load += new System.EventHandler(this.Ledgers_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sdsLedgers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupsBindingSource)).EndInit();
            this.panel3.ResumeLayout(false);
            this.LedgerTab.ResumeLayout(false);
            this.tabBasic.ResumeLayout(false);
            this.tabBasic.PerformLayout();
            this.tabAdv.ResumeLayout(false);
            this.tabAdv.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView LedgerTree;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkLedgerStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ddlGroups;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox chkFixedAsset;
        private DataSetAccounts dataSetAccounts;
        private System.Windows.Forms.CheckBox chkServiceAffect;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox chkInventory;
        private System.Windows.Forms.CheckBox chkMainTainBill;
        private System.Windows.Forms.CheckBox chkRecievable;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.BindingSource groupsBindingSource;
        private System.Windows.Forms.ComboBox ddlDrCr;
        private System.Windows.Forms.BindingSource sdsLedgers;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ImageList imageList1;
        private CustomControl.XButton btnAddnew;
        private CustomControl.XButton xButton3;
        private CustomControl.XButton xButton2;
        private CustomControl.XButton xButton1;
        private CustomControl.XTextBox txtLedgerName;
        private CustomControl.XTextBox txtLedgerID;
        private CustomControl.XTextBox txtCreditLimit;
        private CustomControl.XTextBox txtOpeningBalance;
        private System.Windows.Forms.TabControl LedgerTab;
        private System.Windows.Forms.TabPage tabBasic;
        private CustomControl.XTextBox txtCPerson;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private CustomControl.XTextBox txtCity;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private CustomControl.XTextBox txtAddress;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TabPage tabAdv;
        private CustomControl.XTextBox txtEmail;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private CustomControl.XTextBox txtPhone;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.DateTimePicker dtpAddDate;
        private CustomControl.XComboBox xComboBox1;
    }
}