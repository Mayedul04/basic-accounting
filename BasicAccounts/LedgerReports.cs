﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace BasicAccounts
{
    public partial class LedgerReports : Form
    {
        static string connetionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connetionString);
        private decimal debittotal = 0;
        private decimal credittotal = 0;
        
        public LedgerReports()
        {
            InitializeComponent();
        }

        private void LedgerReports_Load(object sender, EventArgs e)
        {
            ListLedger.SelectedIndexChanged -= new EventHandler(ListLedger_SelectedIndexChanged);
            SqlDataAdapter cmd = new SqlDataAdapter("Select LedgerName,LedgerID from Ledgers where  Status=1", connection);
            DataSet ledgers = new System.Data.DataSet();
            cmd.Fill(ledgers, "Ledgers");
            DataTable dt2 = new DataTable();
            dt2 = ledgers.Tables[0];
            ListLedger.DataSource = dt2;
            ListLedger.SelectedIndexChanged += new EventHandler(ListLedger_SelectedIndexChanged);
        }

        private void LoadLedgerReport(int ledgerid)
        {
            string postingdate = "";
            string vouchernumber = "";
            decimal debitvalue = 0;
            decimal creditvalue = 0;
            decimal oppdebit = 0;
            decimal oppcredit = 0;
            int oppledgercount = 0;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            sql = "Select PostingDate, VoucherNo, VoucherType, OpositeLedger, OpositeDebit, DebitAmount, CreditAmount, OpositeCredit from View_Ledger_Report Where(LedgerID=@LedgerID and OpositeLedgerId<>@LedgerID)";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("LedgerID", SqlDbType.Int, 32).Value = ledgerid;
            SqlDataReader reader = adapter.SelectCommand.ExecuteReader();


            if (reader.HasRows)
            {

                while (reader.Read())
                {
                    debitvalue = decimal.Parse(reader["DebitAmount"].ToString());
                    creditvalue = decimal.Parse(reader["CreditAmount"].ToString());
                    oppdebit = decimal.Parse(reader["OpositeDebit"].ToString());
                    oppcredit = decimal.Parse(reader["OpositeCredit"].ToString());
                    if (reader["PostingDate"].ToString() != postingdate)
                    {
                       
                        if (reader["VoucherNo"].ToString() != vouchernumber)
                        {
                            DataGridViewRow row = (DataGridViewRow)grdLedgerReport.Rows[0].Clone();
                            row.Cells[0].Value = reader["PostingDate"].ToString();
                            row.Cells[1].Value = reader["VoucherNo"].ToString();
                            row.Cells[2].Value = reader["VoucherType"].ToString();
                            
                            if (oppledgercount == 1)
                            {
                                int rowcount = grdLedgerReport.Rows.Count;
                                grdLedgerReport.Rows[rowcount - 3].Cells[3].Value = grdLedgerReport.Rows[rowcount - 2].Cells[3].Value;
                                grdLedgerReport.Rows.RemoveAt(rowcount - 2);
                        
                            }
                                row.Cells[3].Value = "(as per details)";
                                if (debitvalue > 0)
                                {
                                    row.Cells[5].Value = debitvalue;
                                    row.Cells[6].Value = "";
                                }
                                else
                                {
                                    row.Cells[5].Value = "";
                                    row.Cells[6].Value = creditvalue;
                                }
                                grdLedgerReport.Rows.Add(row);
                                oppledgercount = 0;
                                DataGridViewRow row2 = (DataGridViewRow)grdLedgerReport.Rows[0].Clone();
                                row2.Cells[3].Value = reader["OpositeLedger"].ToString();
                                row2.Cells[4].Value = (oppdebit > 0 ? oppdebit.ToString() + " Dr" : oppcredit.ToString() + " Cr");
                                grdLedgerReport.Rows.Add(row2);
                                oppledgercount++;
                                     
                            debittotal += debitvalue;
                            credittotal += creditvalue;

                            
                        }
                        else
                        {
                            DataGridViewRow row1 = (DataGridViewRow)grdLedgerReport.Rows[0].Clone();
                            row1.Cells[3].Value = reader["OpositeLedger"].ToString();
                            row1.Cells[4].Value = (oppdebit > 0 ? oppdebit.ToString() + " Dr" : oppcredit.ToString() + " Cr");
                            grdLedgerReport.Rows.Add(row1);
                            oppledgercount++;
                        }
                        
                    }
                    else
                    {
                        if (reader["VoucherNo"].ToString() != vouchernumber)
                        {
                            DataGridViewRow row = (DataGridViewRow)grdLedgerReport.Rows[0].Clone();
                            row.Cells[1].Value = reader["VoucherNo"].ToString();
                            row.Cells[2].Value = reader["VoucherType"].ToString();

                            if (oppledgercount == 1)
                            {
                                int rowcount = grdLedgerReport.Rows.Count;
                                grdLedgerReport.Rows[rowcount - 3].Cells[3].Value = grdLedgerReport.Rows[rowcount - 2].Cells[3].Value;
                                grdLedgerReport.Rows.RemoveAt(rowcount - 2);
                                
                            }
                                row.Cells[3].Value = "(as per details)";
                                if (debitvalue > 0)
                                {
                                    row.Cells[5].Value = debitvalue;
                                    row.Cells[6].Value = "";
                                }
                                else
                                {
                                    row.Cells[5].Value = "";
                                    row.Cells[6].Value = creditvalue;
                                }
                                grdLedgerReport.Rows.Add(row);
                                oppledgercount = 0;
                                DataGridViewRow row2 = (DataGridViewRow)grdLedgerReport.Rows[0].Clone();
                                row2.Cells[3].Value = reader["OpositeLedger"].ToString();
                                row2.Cells[4].Value = (oppdebit > 0 ? oppdebit.ToString() + " Dr" : oppcredit.ToString() + " Cr");
                                grdLedgerReport.Rows.Add(row2);
                                oppledgercount++;
                         
                            debittotal += debitvalue;
                            credittotal += creditvalue;


                        }
                        else
                        {
                            DataGridViewRow row1 = (DataGridViewRow)grdLedgerReport.Rows[0].Clone();
                            row1.Cells[3].Value = reader["OpositeLedger"].ToString();
                            row1.Cells[4].Value = (oppdebit > 0 ? oppdebit.ToString() + " Dr" : oppcredit.ToString() + " Cr");
                            grdLedgerReport.Rows.Add(row1);
                            oppledgercount++;
                        }

                    }
                    postingdate = reader["PostingDate"].ToString();
                    vouchernumber = reader["VoucherNo"].ToString();
                    
                }
            }

            reader.Close();
            connection.Close();
            if (oppledgercount == 1)
            {
                int rowcount = grdLedgerReport.Rows.Count;
                grdLedgerReport.Rows[rowcount - 3].Cells[3].Value = grdLedgerReport.Rows[rowcount - 2].Cells[3].Value;
                grdLedgerReport.Rows.RemoveAt(rowcount - 2);
            }

            lblDebitTotal.Text = debittotal.ToString("0.00");
            lblCreditTotal.Text = credittotal.ToString("0.00");
           grdLedgerReport.AllowUserToAddRows = false;
        }
        private void txtLSearch_TextChanged(object sender, EventArgs e)
        {
            ListLedger.SelectedIndexChanged -= new EventHandler(ListLedger_SelectedIndexChanged);
            SqlDataAdapter cmd = new SqlDataAdapter("Select * from Ledgers where Status=1 and LedgerName like '%" + txtLSearch.Text + "%'", connection);
            DataSet ledgers = new System.Data.DataSet();
            cmd.Fill(ledgers, "Ledgers");
            DataTable dt = new DataTable();
            dt = ledgers.Tables[0];
            ListLedger.DataSource = dt;
            ListLedger.SelectedIndexChanged += new EventHandler(ListLedger_SelectedIndexChanged);
        }

        private void ListLedger_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListLedger.SelectedIndex != -1)
            {
                //if (grdLedgerReport.RowCount > 2)
                //{
                    grdLedgerReport.Rows.Clear();
                    debittotal = 0;
                    credittotal = 0;
               // }
                grdLedgerReport.AllowUserToAddRows = true;
                lblLedgerName.Text = ListLedger.GetItemText(ListLedger.SelectedItem) + " Report";
                LoadLedgerReport(int.Parse(ListLedger.SelectedValue.ToString()));
            }
        }

        
    }
}
