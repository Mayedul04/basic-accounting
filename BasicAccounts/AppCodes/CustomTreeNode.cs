﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicAccounts.AppCodes
{
    class CustomTreeNode
    {
        private int nodeid;
        private string nodename;
        private int parentid;

        public CustomTreeNode(int nodeid, string nodename, int parentid)
        { }
        
        public int NodeID
        {
            get { return nodeid; }
        }
        public string NodeName
        {
            get { return nodename; }
        }
        public int ParentID
        {
            get { return parentid; }
        }
    }
}
