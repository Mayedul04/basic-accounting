﻿namespace BasicAccounts
{
    partial class BalanceSheet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlMainTrial = new System.Windows.Forms.Panel();
            this.grdBalanceSheet = new System.Windows.Forms.DataGridView();
            this.clmAccHead = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmCDebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpOpenningDate = new System.Windows.Forms.DateTimePicker();
            this.btnPreview = new System.Windows.Forms.Button();
            this.pnlMainTrial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdBalanceSheet)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMainTrial
            // 
            this.pnlMainTrial.AutoScroll = true;
            this.pnlMainTrial.BackColor = System.Drawing.SystemColors.Window;
            this.pnlMainTrial.Controls.Add(this.grdBalanceSheet);
            this.pnlMainTrial.Location = new System.Drawing.Point(0, 42);
            this.pnlMainTrial.Name = "pnlMainTrial";
            this.pnlMainTrial.Size = new System.Drawing.Size(1149, 550);
            this.pnlMainTrial.TabIndex = 1;
            // 
            // grdBalanceSheet
            // 
            this.grdBalanceSheet.BackgroundColor = System.Drawing.SystemColors.Window;
            this.grdBalanceSheet.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdBalanceSheet.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdBalanceSheet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdBalanceSheet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmAccHead,
            this.clmCDebit});
            this.grdBalanceSheet.GridColor = System.Drawing.SystemColors.Control;
            this.grdBalanceSheet.Location = new System.Drawing.Point(10, 12);
            this.grdBalanceSheet.Name = "grdBalanceSheet";
            this.grdBalanceSheet.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grdBalanceSheet.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdBalanceSheet.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.grdBalanceSheet.RowHeadersVisible = false;
            this.grdBalanceSheet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdBalanceSheet.Size = new System.Drawing.Size(1129, 528);
            this.grdBalanceSheet.TabIndex = 0;
            // 
            // clmAccHead
            // 
            this.clmAccHead.HeaderText = "Properties & Assets";
            this.clmAccHead.Name = "clmAccHead";
            this.clmAccHead.Width = 908;
            // 
            // clmCDebit
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            this.clmCDebit.DefaultCellStyle = dataGridViewCellStyle2;
            this.clmCDebit.HeaderText = "Amount";
            this.clmCDebit.Name = "clmCDebit";
            this.clmCDebit.Width = 200;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Window;
            this.label1.Location = new System.Drawing.Point(498, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Balance Sheet";
            // 
            // dtpOpenningDate
            // 
            this.dtpOpenningDate.CustomFormat = "dd-MM-yyyy";
            this.dtpOpenningDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOpenningDate.Location = new System.Drawing.Point(855, 14);
            this.dtpOpenningDate.Name = "dtpOpenningDate";
            this.dtpOpenningDate.Size = new System.Drawing.Size(150, 20);
            this.dtpOpenningDate.TabIndex = 8;
            
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(1027, 13);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPreview.TabIndex = 7;
            this.btnPreview.Text = "Create View";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // BalanceSheet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(1149, 593);
            this.Controls.Add(this.dtpOpenningDate);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlMainTrial);
            this.Name = "BalanceSheet";
            this.Text = "BalanceSheet";
            this.Load += new System.EventHandler(this.BalanceSheet_Load);
            this.pnlMainTrial.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdBalanceSheet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlMainTrial;
        private System.Windows.Forms.DataGridView grdBalanceSheet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmAccHead;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmCDebit;
        private System.Windows.Forms.DateTimePicker dtpOpenningDate;
        private System.Windows.Forms.Button btnPreview;
    }
}