﻿using BasicAccounts.AppCodes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasicAccounts
{
    public partial class BalanceSheet : Form
    {
        static string connetionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connetionString);
        private decimal debittotal = 0;
        private decimal credittotal = 0;
        private decimal totalincome = 0;
        private decimal totalexpese = 0;
        private string reporttype = "normal";
        private Boolean haveledger = false;
        private Boolean havesubclass = false;
        private int subgroupcount = 0;
        private List<object> GropupLevel1 = new List<Object>();
        private List<object> GropupLevel2 = new List<Object>();
        private Boolean isLiabilities=false;
        private Boolean plmode = false;
        private string nature = "";
        private Decimal cDebitGTotal = 0;
        private Decimal cCreditGTotal = 0;
        public BalanceSheet()
        {
            InitializeComponent();
        }

        private void BalanceSheet_Load(object sender, EventArgs e)
        {
            int parentid = 0;
            string parentname = "";
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            #region Assets
            sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID and NatureID=@NatureID";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = 1;
            adapter.SelectCommand.Parameters.Add("NatureID", SqlDbType.Int, 32).Value = 1;
            SqlDataReader reader1 = adapter.SelectCommand.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    parentname = reader1["GroupName"].ToString();
                    parentid = int.Parse(reader1["GroupID"].ToString());
                    Parent parent = new Parent();
                    parent.Id = parentid;
                    parent.Name = parentname;
                    GropupLevel1.Add(parent);
                }
                
            }
            nature = "Assets";
            isLiabilities = false;
            reader1.Close();
            connection.Close();
            foreach (Parent items in GropupLevel1)
            {
                LoadSubGroups(items, 2);
            }
            GropupLevel1.Clear();
            subgroupcount = 0;
            #endregion
            DataGridViewRow row2 = (DataGridViewRow)grdBalanceSheet.Rows[0].Clone();
            row2.Cells[0].Value = "Grand Total";
            row2.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            row2.Cells[1].Value = debittotal;
            row2.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            grdBalanceSheet.Rows.Add(row2);

            
            DataGridViewRow row1 = (DataGridViewRow)grdBalanceSheet.Rows[0].Clone();
            row1.Cells[0].Value = "Fund & Laibilities";
            row1.Cells[1].Value = "Amount";
            row1.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            row1.Cells[0].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            row1.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            row1.Cells[1].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdBalanceSheet.Rows.Add(row1);

            #region Liabilities
            sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID and NatureID=@NatureID";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = 1;
            adapter.SelectCommand.Parameters.Add("NatureID", SqlDbType.Int, 32).Value = 2;
            SqlDataReader reader2 = adapter.SelectCommand.ExecuteReader();
            if (reader2.HasRows)
            {
                while (reader2.Read())
                {
                    parentname = reader2["GroupName"].ToString();
                    parentid = int.Parse(reader2["GroupID"].ToString());
                    Parent parent = new Parent();
                    parent.Id = parentid;
                    parent.Name = parentname;
                    GropupLevel1.Add(parent);
                }
                
            }
            nature = "Liabilities";
            isLiabilities = true;
            reader2.Close();
            connection.Close();
            foreach (Parent items in GropupLevel1)
            {
                LoadSubGroups(items, 2);
            }
            GropupLevel1.Clear();
            subgroupcount = 0;
            #endregion

            if (debittotal!=credittotal)
            {
                plmode = true;  //Only calculation for PL Account
                #region Income
                sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID and NatureID=@NatureID";
                connection.Open();
                adapter.SelectCommand = new SqlCommand(sql, connection);
                adapter.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = 1;
                adapter.SelectCommand.Parameters.Add("NatureID", SqlDbType.Int, 32).Value = 4;
                reader1 = adapter.SelectCommand.ExecuteReader();
                if (reader1.HasRows)
                {
                    while (reader1.Read())
                    {
                        parentname = reader1["GroupName"].ToString();
                        parentid = int.Parse(reader1["GroupID"].ToString());
                        Parent parent = new Parent();
                        parent.Id = parentid;
                        parent.Name = parentname;
                        GropupLevel1.Add(parent);
                    }
                    
                }
                reader1.Close();
                connection.Close();
                nature = "Income";
                isLiabilities = true;
                foreach (Parent items in GropupLevel1)
                {
                    LoadSubGroups(items, 2);
                }
                GropupLevel1.Clear();
                subgroupcount = 0;
                #endregion
                #region Expenses
                sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID and NatureID=@NatureID";
                connection.Open();
                adapter.SelectCommand = new SqlCommand(sql, connection);
                adapter.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = 1;
                adapter.SelectCommand.Parameters.Add("NatureID", SqlDbType.Int, 32).Value = 3;
                reader2 = adapter.SelectCommand.ExecuteReader();
                if (reader2.HasRows)
                {
                    while (reader2.Read())
                    {
                        parentname = reader2["GroupName"].ToString();
                        parentid = int.Parse(reader2["GroupID"].ToString());
                        Parent parent = new Parent();
                        parent.Id = parentid;
                        parent.Name = parentname;
                        GropupLevel1.Add(parent);
                    }
                    
                }
                nature = "Expenditure";
                isLiabilities = false;
                reader2.Close();
                connection.Close();
                foreach (Parent items in GropupLevel1)
                {
                    LoadSubGroups(items, 2);
                }
                #endregion
                if (totalincome != totalexpese)
                    AddProfitLossBalance();
                
                DataGridViewRow row = (DataGridViewRow)grdBalanceSheet.Rows[0].Clone();
                row.Cells[0].Value = "Profit & Loss Acc";
                row.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                grdBalanceSheet.Rows.Add(row);

                LoadProfitLoss();

                DataGridViewRow row3 = (DataGridViewRow)grdBalanceSheet.Rows[0].Clone();
                row3.Cells[0].Value = "Grand Total";
                row3.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                row3.Cells[1].Value = credittotal;
                row3.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                grdBalanceSheet.Rows.Add(row3);


            }
            
            grdBalanceSheet.AllowUserToAddRows = false;
        }

        private void LoadSubGroups(Parent parent, int level)
        {
            if (GropupLevel2.Count > 0)
                GropupLevel2.Clear();
            int parentid = 0;
            string parentname = "";
            int i = 0;
            SqlDataAdapter adapter1 = new SqlDataAdapter();
            string sql = null;
            sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID";
            connection.Open();
            adapter1.SelectCommand = new SqlCommand(sql, connection);
            adapter1.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = parent.Id;
            SqlDataReader reader1 = adapter1.SelectCommand.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    parentname = reader1["GroupName"].ToString();
                    parentid = int.Parse(reader1["GroupID"].ToString());
                    Parent parent1 = new Parent();
                    parent1.Id = parentid;
                    parent1.Name = parentname;
                    GropupLevel2.Add(parent1);
                }
            }
            else
            {
                    reader1.Close();
                    connection.Close();
                    LoadLedgers(parent, 1);
            }

            connection.Close();
            if (GropupLevel2.Count > 0)
            {
                if (plmode == false)
                {
                    if (grdBalanceSheet.Rows.Count == 0 && reporttype != "normal")
                        this.grdBalanceSheet.Rows.Add(parent.Name);
                    else
                    {
                        DataGridViewRow row1 = (DataGridViewRow)grdBalanceSheet.Rows[0].Clone();
                        row1.Cells[0].Value = parent.Name;
                        grdBalanceSheet.Rows.Add(row1);
                    }
                }
                
                subgroupcount = 0;
                foreach (Parent items in GropupLevel2)
                {
                    LoadLedgers(items, 2);

                }
                GropupLevel2.Clear();
            }
        }

        private void LoadLedgers(Parent parent, int level)
        {
            #region Load Data
            Decimal rowDebitTotal = 0;
            Decimal rowCreditTotal = 0;
            Decimal cDebitTotal = 0;
            Decimal cCreditTotal = 0;
            Decimal difference = 0;
            int ledgercount = 0;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            if (reporttype == "normal")
                sql = "Select ClosingDebit,ClosingCredit,LedgerName from  View_BalanceSheet Where (GroupID =@GroupID) and ClosingCredit-ClosingDebit!=0";
            else
                if (nature == "Income" || nature == "Expenditure")
            {
                sql = " SELECT  dbo.View_Periodic_Trail_Balance_2.LedgerName, dbo.View_Periodic_Trail_Balance_2.GroupID, dbo.View_Periodic_Trail_Balance_2.TDebit ,  dbo.View_Periodic_Trail_Balance_2.TCredit , dbo.Groups.NatureID ";
                sql += " FROM   dbo.Groups INNER JOIN dbo.View_Periodic_Trail_Balance_2 ON dbo.Groups.GroupID = dbo.View_Periodic_Trail_Balance_2.GroupID  Where (View_Periodic_Trail_Balance_2.GroupID =@GroupID) and TCredit- TDebit!=0";
            }
            else
            {
                sql = " SELECT  dbo.View_Closing_Balance.LedgerName, dbo.View_Closing_Balance.GroupID, dbo.View_Closing_Balance.OpeningDebit + dbo.View_Closing_Balance.TDebit AS ClosingDebit, dbo.View_Closing_Balance.OpeningCredit + dbo.View_Closing_Balance.TCredit AS ClosingCredit, dbo.Groups.NatureID ";
                sql += " FROM   dbo.Groups INNER JOIN dbo.View_Closing_Balance ON dbo.Groups.GroupID = dbo.View_Closing_Balance.GroupID  Where (View_Closing_Balance.GroupID =@GroupID) ";
            }
                    
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("GroupID", SqlDbType.Int, 32).Value = parent.Id;
            SqlDataReader reader = adapter.SelectCommand.ExecuteReader();
            subgroupcount++;

            if (reader.HasRows)
            {
                if (plmode == false) // On plmode only calculation will be done without adding row
                {
                    #region Sub Group's Name Entry
                    if (grdBalanceSheet.Rows.Count == 0 && reporttype != "normal")
                        this.grdBalanceSheet.Rows.Add(parent.Name);
                    else
                    {
                        DataGridViewRow row1 = (DataGridViewRow)grdBalanceSheet.Rows[0].Clone();
                        if (level == 2)
                            row1.Cells[0].Value = "     " + parent.Name;
                        else
                            row1.Cells[0].Value = parent.Name;
                        grdBalanceSheet.Rows.Add(row1);
                    } 
                    #endregion
                }
                var loop = true;
                while (loop)
                {
                    loop = reader.Read();
                    if (loop)  // Have Data Row
                    {
                        

                        #region Ledger Level Row entry on Normal mode
                        DataGridViewRow row = (DataGridViewRow)grdBalanceSheet.Rows[0].Clone();
                        row.Cells[0].Value = "          " + reader["LedgerName"].ToString();
                        if (reporttype != "normal" && (nature == "Income" || nature == "Expenditure"))
                        {
                            rowDebitTotal = decimal.Parse(reader["TDebit"].ToString());
                            rowCreditTotal = decimal.Parse(reader["TCredit"].ToString());
                        }
                        else
                        {
                            rowDebitTotal = decimal.Parse(reader["ClosingDebit"].ToString());
                            rowCreditTotal = decimal.Parse(reader["ClosingCredit"].ToString());
                        }
                        if (rowDebitTotal != rowCreditTotal)
                        {
                            if (isLiabilities == true)
                            {
                                if (rowCreditTotal > rowDebitTotal)
                                {
                                    difference = rowCreditTotal - rowDebitTotal;
                                    row.Cells[1].Value = difference;
                                }

                                else
                                {
                                    difference = rowDebitTotal - rowCreditTotal;
                                    if (difference != 0)
                                        row.Cells[1].Value = "(-)" + difference;
                                    else
                                        row.Cells[1].Value = "";

                                }

                            }
                            else
                            {
                                if (rowDebitTotal > rowCreditTotal)
                                {
                                    difference = rowDebitTotal - rowCreditTotal;
                                    row.Cells[1].Value = difference;
                                }

                                else
                                {
                                    difference = rowCreditTotal - rowDebitTotal;
                                    if (difference != 0)
                                        row.Cells[1].Value = "(-)" + difference;
                                    else
                                        row.Cells[1].Value = "";

                                }

                            }
                            if (plmode == false)  // On plmode only calculation will be done without adding row
                            {
                                grdBalanceSheet.Rows.Add(row);
                                ledgercount++;
                            }
                               
                        }
                        
                            #endregion
                        
                         
                        difference = 0;
                        cDebitTotal += rowDebitTotal;
                        cCreditTotal += rowCreditTotal;
                    }
                    else
                    {
                        #region  Group Level Total Calculation and adding Row
                        DataGridViewRow row = (DataGridViewRow)grdBalanceSheet.Rows[0].Clone();
                        if (level == 2)
                            row.Cells[0].Value = "    Sub Total :";
                        else
                            row.Cells[0].Value = "Group Total :";
                        row.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        row.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        if (cCreditTotal != cDebitTotal)
                        {
                            if (isLiabilities == true)  //Only Income and Liabilities can come here
                            {
                                if (cCreditTotal > cDebitTotal)
                                {
                                    difference = cCreditTotal - cDebitTotal;
                                    row.Cells[1].Value = difference;
                                    if (nature == "Income")
                                        totalincome += difference;
                                    else
                                        credittotal += difference;
                                }
                                else
                                {
                                    difference = cDebitTotal - cCreditTotal;
                                    if (difference != 0)
                                        row.Cells[1].Value = "(-)" + difference;
                                    else
                                        row.Cells[1].Value = "";
                                    if (nature == "Income" && nature != "Liabilities")
                                        totalincome -= difference;
                                    else
                                        credittotal -= difference;
                                }
                            }
                            else //Only Assets and Expenditure can come here
                            {
                                if (cDebitTotal > cCreditTotal)
                                {
                                    difference = cDebitTotal - cCreditTotal;
                                    row.Cells[1].Value = difference;
                                    if (nature == "Expenditure")
                                        totalexpese += difference;
                                    else
                                        debittotal += difference;
                                }

                                else
                                {
                                    difference = cCreditTotal - cDebitTotal;
                                    if (difference != 0)
                                        row.Cells[1].Value = "(-)" + difference;
                                    else
                                        row.Cells[1].Value = "";
                                    if (nature == "Expenditure")
                                        totalexpese -= difference;
                                    else
                                        debittotal -= difference;
                                }
                            }
                            if (plmode == false) // On plmode only calculation will be done without adding row
                                grdBalanceSheet.Rows.Add(row);
                        }
                        else
                        {
                            if (ledgercount == 0)
                            {
                                int rowcount = grdBalanceSheet.Rows.Count;
                                grdBalanceSheet.Rows.RemoveAt(rowcount - 1);
                            }
                        }
                       
                       #endregion
                        
                        difference = 0;
                        if (level == 2)
                        {
                            cDebitGTotal += cDebitTotal;
                            cCreditGTotal += cCreditTotal;
                        }

                    }
                }

            }
            else
            {
                reader.Close();
                connection.Close();
                if (plmode == false)
                {
                    int rowcount = grdBalanceSheet.Rows.Count;
                    if (level == 2)  //This Dipest Group Don't have any ledger, so Remove
                    {
                        if (GropupLevel2.Count == 1)
                        {
                            if (rowcount > 1)
                            {
                                grdBalanceSheet.Rows.RemoveAt(rowcount - 2);
                            }
                        }

                    }
                }
                   
                               
            }
            if (GropupLevel2.Count > 1)
            {
                if (subgroupcount == GropupLevel2.Count)
                {
                    if (plmode == false)
                    {
                        #region Main Group Level Grand Total Entry
                        DataGridViewRow row2 = (DataGridViewRow)grdBalanceSheet.Rows[1].Clone();
                        row2.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        row2.Cells[0].Value = "Group Total";
                        row2.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        if (cCreditGTotal != cDebitGTotal)
                        {
                            if (isLiabilities == true)
                            {
                                if (cCreditGTotal > cDebitGTotal)
                                {
                                    difference = cCreditGTotal - cDebitGTotal;
                                    row2.Cells[1].Value = difference;
                                }

                                else
                                {
                                    difference = cDebitGTotal - cCreditGTotal;
                                    if (difference != 0)
                                        row2.Cells[1].Value = "(-)" + difference;
                                    else
                                        row2.Cells[1].Value = "";
                                }
                                //credittotal += difference;
                            }
                            else
                            {
                                if (cDebitGTotal > cCreditGTotal)
                                {
                                    difference = cDebitGTotal - cCreditGTotal;
                                    row2.Cells[1].Value = difference;
                                }

                                else
                                {
                                    difference = cCreditGTotal - cDebitGTotal;
                                    if (difference != 0)
                                        row2.Cells[1].Value = "(-)" + difference;
                                    else
                                        row2.Cells[1].Value = "";
                                }
                                // debittotal += difference;
                            }
                        }
                        
                        

                        grdBalanceSheet.Rows.Add(row2); 
                        #endregion
                    }
                    
                    difference = 0;
                    cDebitGTotal = 0;
                    cCreditGTotal = 0;
                }
            }


            connection.Close();
            #endregion
        }
        private void AddProfitLossBalance()
        {
            try
            {
                Ledger ledger = new Ledger();
                ledger.LedgerID = 4023;
                Decimal openingvalue =totalexpese  - totalincome;
                if (openingvalue > 0)
                    ledger.OpeningDebit = openingvalue;
                else
                    ledger.OpeningCredit =totalincome - totalexpese;


                SqlDataAdapter adapter = new SqlDataAdapter();
                string sql = null;
                sql = "Update  Ledgers set  OpeningDebit=@OpeningDebit, OpeningCredit=@OpeningCredit where LedgerID=@LedgerID";

                connection.Open();
                adapter.InsertCommand = new SqlCommand(sql, connection);
                adapter.InsertCommand.Parameters.Add("@LedgerID", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@LedgerID"].Value = ledger.LedgerID;
                adapter.InsertCommand.Parameters.Add("@OpeningDebit", SqlDbType.Real);
                adapter.InsertCommand.Parameters["@OpeningDebit"].Value = ledger.OpeningDebit;
                adapter.InsertCommand.Parameters.Add("@OpeningCredit", SqlDbType.Real);
                adapter.InsertCommand.Parameters["@OpeningCredit"].Value = ledger.OpeningCredit;

                adapter.InsertCommand.ExecuteNonQuery();
                connection.Close();
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                if (connection.State.ToString() == "open")
                {
                    connection.Close();
                }

            }
        }
        private void LoadProfitLoss()
        {
            SqlDataAdapter adapter1 = new SqlDataAdapter();
            string sql = null;
            decimal openingvalue = 0;
            string drcr = "";
            sql = "Select [OpeningDebit], [OpeningCredit]  from  Ledgers where LedgerID=@LedgerID";
            connection.Open();
            adapter1.SelectCommand = new SqlCommand(sql, connection);
            adapter1.SelectCommand.Parameters.Add("LedgerID", SqlDbType.Int, 32).Value = 4023;
            SqlDataReader reader1 = adapter1.SelectCommand.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    if (decimal.Parse(reader1["OpeningDebit"].ToString()) > 0)
                    {
                        openingvalue = decimal.Parse(reader1["OpeningDebit"].ToString());
                        drcr = "Dr";
                    }
                    else
                    {
                        openingvalue = decimal.Parse(reader1["OpeningCredit"].ToString());
                        drcr = "Cr";
                    }
                    DataGridViewRow row = (DataGridViewRow)grdBalanceSheet.Rows[0].Clone();
                    row.Cells[0].Value = "    Profit  & Loss acc";
                    row.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    if (drcr == "Dr")
                    {
                        row.Cells[1].Value = "(-)" + openingvalue.ToString();
                        credittotal -= openingvalue;
                    }
                    else
                    {
                        row.Cells[1].Value = openingvalue.ToString();
                        credittotal += openingvalue;
                    } 
                    row.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    grdBalanceSheet.Rows.Add(row);
                }
                DataGridViewRow row1= (DataGridViewRow)grdBalanceSheet.Rows[0].Clone();
                row1.Cells[0].Value = "Group Total :";
                row1.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                if (drcr == "Dr")
                {
                    row1.Cells[1].Value = "(-)" + openingvalue.ToString();
                    
                }
                else
                {
                    row1.Cells[1].Value = openingvalue.ToString();
                   
                }
                row1.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                grdBalanceSheet.Rows.Add(row1);
            }
            else
            {
                reader1.Close();
                connection.Close();
                
            }
            connection.Close();
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            string sql2 = null;
            string sql3 = null;
            Main mainform = new Main();
            DateTime startperiod = mainform.getStartPeriod;
            DateTime startdate = DateTime.Parse(dtpOpenningDate.Value.ToString());
            sql += "ALTER View [View_Closing_Balance] as ";
            sql += " SELECT dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, dbo.Ledgers.LedgerName, SUM(TempVoucher.DebitAmount) AS TDebit, SUM(TempVoucher.CreditAmount) AS TCredit, dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status";
            sql += " FROM  (SELECT  dbo.Voucher_Contents.VoucherNo, dbo.Voucher_Contents.DebitAmount, dbo.Voucher_Contents.CreditAmount, dbo.Voucher_Contents.LedgerID ";
            sql += "  FROM   dbo.Voucher_Contents INNER JOIN dbo.Vouchers ON dbo.Voucher_Contents.VoucherNo = dbo.Vouchers.VoucherNo ";
            sql += " WHERE ( dbo.Vouchers.PostingDate <='" + dtpOpenningDate.Value.ToString("yyyy-MM-dd") + "')) AS TempVoucher INNER JOIN dbo.Ledgers  ON dbo.Ledgers.LedgerID = TempVoucher.LedgerID ";
            sql += "   GROUP BY dbo.Ledgers.LedgerName, dbo.Ledgers.LedgerID, dbo.Ledgers.Status, dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, dbo.Ledgers.GroupID";

            sql2 += "ALTER View [View_Periodic_Trail_Balance_2] as ";
            sql2 += " Select dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, dbo.Ledgers.LedgerName, SUM(dbo.Voucher_Contents.DebitAmount) AS TDebit, SUM(dbo.Voucher_Contents.CreditAmount) AS TCredit, dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status, 'T' as TType ";
            sql2 += " FROM            dbo.Voucher_Contents INNER JOIN  dbo.Ledgers ON dbo.Voucher_Contents.LedgerID = dbo.Ledgers.LedgerID  ";
            sql2 += " WHERE        (dbo.Voucher_Contents.PostingDate BETWEEN '" + startperiod.ToString("yyyy-MM-dd") + "' AND '" + dtpOpenningDate.Value.ToString("yyyy-MM-dd") + "') ";
            sql2 += " GROUP BY dbo.Ledgers.LedgerName,dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status, dbo.Ledgers.OpeningDebit,dbo.Ledgers.OpeningCredit  ";


            //sql2 += "ALTER View [View_Periodic_Trail_Balance_2] as ";
            //sql2 += " Select dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, dbo.Ledgers.LedgerName, SUM(dbo.Voucher_Contents.DebitAmount) AS TDebit, SUM(dbo.Voucher_Contents.CreditAmount) AS TCredit, dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status, 'T' as TType ";
            //sql2 += " FROM            dbo.Voucher_Contents INNER JOIN  dbo.Ledgers ON dbo.Voucher_Contents.LedgerID = dbo.Ledgers.LedgerID  ";
            //sql2 += " WHERE        (dbo.Voucher_Contents.PostingDate BETWEEN '" + dtpOpenningDate.Value.ToString("yyyy-MM-dd") + "' AND '" + dtpClosingDate.Value.ToString("yyyy-MM-dd") + "') ";
            //sql2 += " GROUP BY dbo.Ledgers.LedgerName,dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status, dbo.Ledgers.OpeningDebit,dbo.Ledgers.OpeningCredit  ";
            //sql2 += "HAVING        (dbo.Ledgers.Status = 1)";
            //sql2 += " UNION ALL ";
            //if (startdate <= startperiod)
            //{
            //    sql2 += "Select dbo.Ledgers.OpeningDebit, dbo.Ledgers.OpeningCredit, ";
            //    sql2 += " dbo.Ledgers.LedgerName, 0 as TDebit, 0 as TCredit, dbo.Ledgers.LedgerID, dbo.Ledgers.GroupID, dbo.Ledgers.Status, 'C' as TType ";
            //    sql2 += " from dbo.Ledgers ";
            //    sql2 += " Where  ((dbo.Ledgers.OpeningDebit>0 or dbo.Ledgers.OpeningCredit>0) and Status=1 and LedgerID!=57)";
            //}
            //else
            //{
            //    sql2 += "Select CASE WHEN dbo.View_Closing_Balance.OpeningDebit + dbo.View_Closing_Balance.TDebit > dbo.View_Closing_Balance.OpeningCredit + dbo.View_Closing_Balance.TCredit THEN (dbo.View_Closing_Balance.OpeningDebit + dbo.View_Closing_Balance.TDebit) - (dbo.View_Closing_Balance.OpeningCredit + dbo.View_Closing_Balance.TCredit) ELSE 0 END AS OpeningDebit, ";
            //    sql2 += "       CASE WHEN dbo.View_Closing_Balance.OpeningDebit + dbo.View_Closing_Balance.TDebit < dbo.View_Closing_Balance.OpeningCredit + dbo.View_Closing_Balance.TCredit THEN (dbo.View_Closing_Balance.OpeningCredit+ dbo.View_Closing_Balance.TCredit) - (dbo.View_Closing_Balance.OpeningDebit + dbo.View_Closing_Balance.TDebit) ELSE 0 END AS OpeningCredit, ";
            //    sql2 += " dbo.View_Closing_Balance.LedgerName,  dbo.View_Closing_Balance.TDebit, dbo.View_Closing_Balance.TCredit, dbo.View_Closing_Balance.LedgerID, dbo.View_Closing_Balance.GroupID, dbo.View_Closing_Balance.Status, 'C' as TType ";
            //    sql2 += " from dbo.View_Closing_Balance ";
            //    sql2 += " Group by dbo.View_Closing_Balance.OpeningDebit, dbo.View_Closing_Balance.TDebit, dbo.View_Closing_Balance.OpeningCredit, dbo.View_Closing_Balance.TCredit, dbo.View_Closing_Balance.LedgerName, dbo.View_Closing_Balance.Status, dbo.View_Closing_Balance.GroupID, dbo.View_Closing_Balance.LedgerID";
            //    sql2 += " Having  ((dbo.View_Closing_Balance.OpeningDebit + dbo.View_Closing_Balance.TDebit) - (dbo.View_Closing_Balance.OpeningCredit + dbo.View_Closing_Balance.TCredit)>0 or (dbo.View_Closing_Balance.OpeningCredit + dbo.View_Closing_Balance.TCredit) - (dbo.View_Closing_Balance.OpeningDebit + dbo.View_Closing_Balance.TDebit)>0)";
            //}

            //sql3 += "ALTER View [View_BalanceSheet_Periodic] as ";
            //sql3 += " SELECT  dbo.View_Closing_Balance.LedgerName, dbo.View_Closing_Balance.GroupID, dbo.View_Closing_Balance.OpeningDebit + dbo.View_Closing_Balance.TDebit AS ClosingDebit, dbo.View_Closing_Balance.OpeningCredit + dbo.View_Closing_Balance.TCredit AS ClosingCredit, dbo.Groups.NatureID ";
            //sql3 += " FROM   dbo.Groups INNER JOIN dbo.View_Closing_Balance ON dbo.Groups.GroupID = dbo.View_Closing_Balance.GroupID";

            connection.Open();
            adapter.InsertCommand = new SqlCommand(sql, connection);
            adapter.InsertCommand.ExecuteNonQuery();
            adapter.InsertCommand = new SqlCommand(sql2, connection);
            adapter.InsertCommand.ExecuteNonQuery();
            connection.Close();

            reporttype = "periodic";
            grdBalanceSheet.Rows.Clear();
            GropupLevel1.Clear();
            GropupLevel2.Clear();
            debittotal = 0;
            credittotal = 0;
            totalincome = 0;
            totalexpese = 0;
            int parentid = 0;
            string parentname = "";
            plmode = false;
            #region Assets
            sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID and NatureID=@NatureID";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = 1;
            adapter.SelectCommand.Parameters.Add("NatureID", SqlDbType.Int, 32).Value = 1;
            SqlDataReader reader1 = adapter.SelectCommand.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    parentname = reader1["GroupName"].ToString();
                    parentid = int.Parse(reader1["GroupID"].ToString());
                    Parent parent = new Parent();
                    parent.Id = parentid;
                    parent.Name = parentname;
                    GropupLevel1.Add(parent);
                }
            }
            isLiabilities = false;
            nature = "Assets";
            connection.Close();
            foreach (Parent items in GropupLevel1)
            {
                LoadSubGroups(items, 2);
            }
            GropupLevel1.Clear();
            subgroupcount = 0;
            #endregion
            DataGridViewRow row2 = (DataGridViewRow)grdBalanceSheet.Rows[0].Clone();
            row2.Cells[0].Value = "Grand Total";
            row2.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            row2.Cells[1].Value = debittotal;
            row2.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            grdBalanceSheet.Rows.Add(row2);


            DataGridViewRow row1 = (DataGridViewRow)grdBalanceSheet.Rows[0].Clone();
            row1.Cells[0].Value = "Fund & Laibilities";
            row1.Cells[1].Value = "Amount";
            row1.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            row1.Cells[0].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            row1.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            row1.Cells[1].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            grdBalanceSheet.Rows.Add(row1);

            #region Liabilities
            sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID and NatureID=@NatureID";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = 1;
            adapter.SelectCommand.Parameters.Add("NatureID", SqlDbType.Int, 32).Value = 2;
            SqlDataReader reader2 = adapter.SelectCommand.ExecuteReader();
            if (reader2.HasRows)
            {
                while (reader2.Read())
                {
                    parentname = reader2["GroupName"].ToString();
                    parentid = int.Parse(reader2["GroupID"].ToString());
                    Parent parent = new Parent();
                    parent.Id = parentid;
                    parent.Name = parentname;
                    GropupLevel1.Add(parent);
                }
            }
            isLiabilities = true;
            nature = "Liabilities";
            connection.Close();
            foreach (Parent items in GropupLevel1)
            {
                LoadSubGroups(items, 2);
            }
            GropupLevel1.Clear();
            subgroupcount = 0;
            #endregion

            if (debittotal != credittotal)
            {
                plmode = true;
                #region Income
                sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID and NatureID=@NatureID";
                connection.Open();
                adapter.SelectCommand = new SqlCommand(sql, connection);
                adapter.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = 1;
                adapter.SelectCommand.Parameters.Add("NatureID", SqlDbType.Int, 32).Value = 4;
                reader1 = adapter.SelectCommand.ExecuteReader();
                if (reader1.HasRows)
                {
                    while (reader1.Read())
                    {
                        parentname = reader1["GroupName"].ToString();
                        parentid = int.Parse(reader1["GroupID"].ToString());
                        Parent parent = new Parent();
                        parent.Id = parentid;
                        parent.Name = parentname;
                        GropupLevel1.Add(parent);
                    }
                    
                }
                nature = "Income";
                isLiabilities = true;
                reader1.Close();
                connection.Close();
                foreach (Parent items in GropupLevel1)
                {
                    LoadSubGroups(items, 2);
                }
                GropupLevel1.Clear();
                subgroupcount = 0;
                #endregion
                #region Expenses
                sql = "Select GroupName, GroupID  from  Groups where ParentGroupID=@ParentGroupID and NatureID=@NatureID";
                connection.Open();
                adapter.SelectCommand = new SqlCommand(sql, connection);
                adapter.SelectCommand.Parameters.Add("ParentGroupID", SqlDbType.Int, 32).Value = 1;
                adapter.SelectCommand.Parameters.Add("NatureID", SqlDbType.Int, 32).Value = 3;
                reader2 = adapter.SelectCommand.ExecuteReader();
                if (reader2.HasRows)
                {
                    while (reader2.Read())
                    {
                        parentname = reader2["GroupName"].ToString();
                        parentid = int.Parse(reader2["GroupID"].ToString());
                        Parent parent = new Parent();
                        parent.Id = parentid;
                        parent.Name = parentname;
                        GropupLevel1.Add(parent);
                    }
                    
                }
                nature = "Expenditure";
                isLiabilities = false;
                reader2.Close();
                connection.Close();
                foreach (Parent items in GropupLevel1)
                {
                    LoadSubGroups(items, 2);
                }
                #endregion
                if (totalincome != totalexpese)
                    AddProfitLossBalance();
                nature = "";
                DataGridViewRow row = (DataGridViewRow)grdBalanceSheet.Rows[0].Clone();
                row.Cells[0].Value = "Profit & Loss Acc";
                row.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                grdBalanceSheet.Rows.Add(row);

                LoadProfitLoss();

                DataGridViewRow row3 = (DataGridViewRow)grdBalanceSheet.Rows[0].Clone();
                row3.Cells[0].Value = "Grand Total";
                row3.Cells[0].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                row3.Cells[1].Value = credittotal;
                row3.Cells[1].Style.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                grdBalanceSheet.Rows.Add(row3);


            }
            grdBalanceSheet.AllowUserToAddRows = false;
        }

    }
}
