﻿using BasicAccounts.AppCodes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasicAccounts
{
    public partial class Vouchers : Form
    {
        static string connetionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connetionString);
        private int Typeid;
        private decimal debittotal = 0;
        private decimal credittotal = 0;
        private decimal creditbalace = 0;
        private decimal debitbalace = 0;
        private decimal beforeupdatevaluedr = 0;
        private decimal beforeupdatevaluecr = 0;
        private Boolean loadvoucherdetails = false;
        public Vouchers(int typeid)
        {
            
            InitializeComponent();
            this.Typeid = typeid;
            
        }

        private void Vouchers_Load(object sender, EventArgs e)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select LedgerName,LedgerID from Ledgers where  Status=1", connection);
            DataSet ledgers = new System.Data.DataSet();
            cmd.Fill(ledgers, "Ledgers");
            DataTable dt2 = new DataTable();
            dt2 = ledgers.Tables[0];
            clmnLedgers.DataSource = dt2;

            
                     
            LoadVoucherNumber();
            grdviewParticulars.Rows.Clear();
        
            ListVoucher.SelectedIndexChanged -= new EventHandler(ListVoucher_SelectedIndexChanged);
            SqlDataAdapter cmd1 = new SqlDataAdapter("Select * from Vouchers where VoucherType=@VoucherType order by PostingDate Desc", connection);
            cmd1.SelectCommand.Parameters.Add("VoucherType", SqlDbType.Int, 32).Value = Typeid;
            DataSet vouchers = new System.Data.DataSet();
            cmd1.Fill(vouchers, "Vouchers");
            DataTable dt = new DataTable();
            dt = vouchers.Tables[0];
            ListVoucher.DataSource = dt;
            ListVoucher.SelectedIndexChanged += new EventHandler(ListVoucher_SelectedIndexChanged);

            //Load Unsaved Entry on Last Failer
            //DialogResult result = MessageBox.Show("Last Time You Have some Unsaved Entry. Do You Want to Work on those?", "Show", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            //if (result.Equals(DialogResult.OK))
            //{
                
            //}
            //else
            //{

            //}
            #region Load Unsaved Entry
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            sql = "Select EntryID,LedgerID,VoucherNo,DebitAmount,CreditAmount  from  dbo.Voucher_Contents where VoucherType=@VoucherType and VoucherNo not like '%CP%' and VoucherNo not like '%BP%' and VoucherNo not like '%CR%' and VoucherNo not like '%BR%' and VoucherNo not like '%JV%' and VoucherNo not like '%CT%'";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("VoucherType", SqlDbType.Int, 32).Value = Typeid;
            SqlDataReader reader = adapter.SelectCommand.ExecuteReader();
            int i = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (i == 0)
                    {
                        hdnguid.Text = reader["VoucherNo"].ToString();
                    }
                    DataGridViewRow row = (DataGridViewRow)grdviewParticulars.Rows[i].Clone();
                    row.Cells[0].Value = Int32.Parse(reader["LedgerID"].ToString());
                    if (decimal.Parse(reader["DebitAmount"].ToString()) != 0)
                    {
                        row.Cells[1].Value = decimal.Parse(reader["DebitAmount"].ToString());
                    }

                    row.Cells[1].Tag = "2"; //2 for Edit Mode to understand the mode of action(inser/update)
                    row.Cells[0].Tag = Int64.Parse(reader["EntryID"].ToString());
                    if (decimal.Parse(reader["CreditAmount"].ToString()) != 0)
                    {
                        row.Cells[2].Value = decimal.Parse(reader["CreditAmount"].ToString());
                    }

                    grdviewParticulars.Rows.Add(row);

                    debittotal += decimal.Parse(reader["DebitAmount"].ToString());
                    credittotal += decimal.Parse(reader["CreditAmount"].ToString());
                    i++;
                }
            }
            else
            {
                hdnguid.Text = Guid.NewGuid().ToString();
                debittotal = 0;
                credittotal = 0;
            }


            connection.Close();

            lblDebitTotal.Text = debittotal.ToString("0.00");
            lblCreditTotal.Text = credittotal.ToString("0.00");
            #endregion
            //Load Unsaved Entry on Last Failer
        }
        private void LoadVoucherDetails(string vouchernumber)
        {
            if (grdviewParticulars.RowCount > 0)
            {
                grdviewParticulars.Rows.Clear();
                debittotal = 0;
                credittotal = 0;
            }
            if (loadvoucherdetails != false)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                string sql = null;
                sql = "Select EntryID,LedgerID, DebitAmount ,CreditAmount  from  dbo.Voucher_Contents where  VoucherNo=@VoucherNo";
                connection.Open();
                adapter.SelectCommand = new SqlCommand(sql, connection);
                adapter.SelectCommand.Parameters.Add("VoucherNo", SqlDbType.NVarChar, 50).Value = vouchernumber;
                SqlDataReader reader = adapter.SelectCommand.ExecuteReader();
                int i = 0;

                while (reader.Read())
                {
                    DataGridViewRow row = (DataGridViewRow)grdviewParticulars.Rows[i].Clone();
                    row.Cells[0].Value = Int32.Parse(reader["LedgerID"].ToString());
                    if (decimal.Parse(reader["DebitAmount"].ToString()) != 0)
                    {
                        row.Cells[1].Value = decimal.Parse(reader["DebitAmount"].ToString());
                    }

                    row.Cells[1].Tag = "2"; //2 for Edit Mode to understand the mode of action(inser/update)
                    row.Cells[0].Tag = Int64.Parse(reader["EntryID"].ToString());
                    if (decimal.Parse(reader["CreditAmount"].ToString()) != 0)
                    {
                        row.Cells[2].Value = decimal.Parse(reader["CreditAmount"].ToString());
                    }

                    grdviewParticulars.Rows.Add(row);
                    debittotal += decimal.Parse(reader["DebitAmount"].ToString());
                    credittotal += decimal.Parse(reader["CreditAmount"].ToString());

                }

                connection.Close();
               // ListVoucher.SelectedIndexChanged += new EventHandler(ListVoucher_SelectedIndexChanged);
            }  
            

            lblDebitTotal.Text = debittotal.ToString("0.00");
            lblCreditTotal.Text = credittotal.ToString("0.00");
        }
        private void LoadVoucherNumber()
        {
            SqlCommand cmd = new SqlCommand("Select * from VoucherTypes where VTypeID=@VTypeID", connection);
            connection.Open();
            cmd.Parameters.Add("VTypeID", SqlDbType.Int, 32).Value = Typeid;
            SqlDataReader reader = cmd.ExecuteReader();
            string prefix = "";
            int length = 0;
            string formatedvalue = "";
            int needed0 = 0;
           // string generatedvnumber = "";
            while (reader.Read())
            {
                txtVType.Text = reader["VoucherType"].ToString();
                prefix = reader["Prefix"].ToString();
                length = int.Parse(reader["Length"].ToString());
            }
            connection.Close();
            int lastvoucherid = getLastVID()+1;
            needed0 = length - lastvoucherid.ToString().Length;
            //if (lastvoucherid == 1)
            //    needed0 = length - 1;
            //else
            //needed0=length - int.Parse(Math.Ceiling(Math.Log10(lastvoucherid)).ToString());
            
            for (int i = 1; i <= needed0; i++)
            {
                formatedvalue += "0";
            }
            formatedvalue += lastvoucherid.ToString();
            txtVNumber.Text =prefix +formatedvalue;
            txtVNumber.Tag = null;
        }

        private Int32 getLastVID()
        {
            Int32 retid=0;
            string retstr = "";
            SqlCommand cmd = new SqlCommand("Select top 1 VoucherNo from Vouchers where VoucherType=@VoucherType order by VoucherID Desc", connection);
            connection.Open();
            cmd.Parameters.Add("VoucherType", SqlDbType.Int, 32).Value = Typeid;
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                retstr = reader["VoucherNo"].ToString();
                
            }
            connection.Close();
            if (retstr != "")
            {
                string digits = retstr.Substring(2);
                retid = Int32.Parse(digits);
            }
            else retid = 0;
            return retid;
        }
        
        private void grdviewParticulars_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                ((ComboBox)e.Control).DropDownStyle = ComboBoxStyle.DropDown;
                ((ComboBox)e.Control).AutoCompleteSource = AutoCompleteSource.ListItems;
                ((ComboBox)e.Control).AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
                
            }
           else if (e.Control is DataGridViewTextBoxEditingControl)
                {
                    //((TextBox)e.Control).AutoCompleteSource = AutoCompleteSource.CustomSource;
                    //((TextBox)e.Control).AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
                    //((TextBox)e.Control).AutoCompleteCustomSource.AddRange(new string[] { (debittotal - credittotal).ToString()});

                //((TextBox)e.Control).SelectAll();
                //((TextBox)e.Control).SelectionStart = 0;
               // ((TextBox)e.Control).SelectionLength = ((TextBox)e.Control).Text.Length;
            }
                           
        }
        private void InsertMainVoucher()
        {
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                string sql = null;
                Voucher voucher = new Voucher();
                
                voucher.VoucherNo = txtVNumber.Text;
                voucher.VoucherType = Typeid;
                voucher.PostingDate = dtpPostingDate.Value;
                voucher.Narration = txtNarration.Text;
                if (txtVNumber.Tag == null)
                {
                   
                    sql = "Select VoucherNo from Vouchers where VoucherNo=@VoucherNo";
                    connection.Open();
                    adapter.SelectCommand = new SqlCommand(sql, connection);
                    adapter.SelectCommand.Parameters.Add("VoucherNo", SqlDbType.NVarChar, 50).Value = txtVNumber.Text;
                    SqlDataReader reader = adapter.SelectCommand.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Close();
                        connection.Close();
                        MessageBox.Show("This voucher number already has taken. Next Voucher number will be used.");
                        LoadVoucherNumber();
                    }
                    else
                    {
                        reader.Close();
                        connection.Close();
                    }
                   
                    sql = "Insert into Vouchers (VoucherNo,Narration,PostingDate,VoucherType) values (@VoucherNo,@Narration,@PostingDate,@VoucherType)";
                    adapter.InsertCommand = new SqlCommand(sql, connection);
                    connection.Open();
                    adapter.InsertCommand.Parameters.Add("@VoucherNo", SqlDbType.NVarChar, 50);
                    adapter.InsertCommand.Parameters["@VoucherNo"].Value = voucher.VoucherNo;
                    adapter.InsertCommand.Parameters.Add("@VoucherType", SqlDbType.Int, 32);
                    adapter.InsertCommand.Parameters["@VoucherType"].Value = voucher.VoucherType;
                    adapter.InsertCommand.Parameters.Add("@PostingDate", SqlDbType.DateTime);
                    adapter.InsertCommand.Parameters["@PostingDate"].Value = voucher.PostingDate;
                    adapter.InsertCommand.Parameters.Add("@Narration", SqlDbType.NVarChar, 150);
                    adapter.InsertCommand.Parameters["@Narration"].Value = voucher.Narration;
                    adapter.InsertCommand.ExecuteNonQuery();

                    connection.Close();
                    MessageBox.Show("Voucher has been Inserted!!");
                    updateVoucherItems();
                }
                else
                {
                    voucher.VoucherID =int.Parse(txtVNumber.Tag.ToString());
                    sql = "Update  Vouchers set Narration=@Narration,PostingDate=@PostingDate where VoucherID=@VoucherID";
                    adapter.InsertCommand = new SqlCommand(sql, connection);
                    connection.Open();

                    adapter.InsertCommand.Parameters.Add("@VoucherID", SqlDbType.Int, 32);
                    adapter.InsertCommand.Parameters["@VoucherID"].Value = voucher.VoucherID;
                    adapter.InsertCommand.Parameters.Add("@PostingDate", SqlDbType.DateTime);
                    adapter.InsertCommand.Parameters["@PostingDate"].Value = voucher.PostingDate;
                    adapter.InsertCommand.Parameters.Add("@Narration", SqlDbType.NVarChar, 150);
                    adapter.InsertCommand.Parameters["@Narration"].Value = voucher.Narration;
                    adapter.InsertCommand.ExecuteNonQuery();

                    connection.Close();
                    MessageBox.Show("Voucher has been Updated!!");
                }
               

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        private void updateVoucherItems()
        {
            try
            {
                VoucherItem voucheritems = new VoucherItem();
                voucheritems.VoucherNo = txtVNumber.Text;
                
                SqlDataAdapter adapter = new SqlDataAdapter();
                string sql = null;
                sql = "Update Voucher_Contents set VoucherNo=@VoucherNo where VoucherNo=@TempV";

                connection.Open();
                adapter.InsertCommand = new SqlCommand(sql, connection);
                adapter.InsertCommand.Parameters.Add("@TempV", SqlDbType.NVarChar, 50);
                adapter.InsertCommand.Parameters["@TempV"].Value = hdnguid.Text;
                adapter.InsertCommand.Parameters.Add("@VoucherNo", SqlDbType.NVarChar, 50);
                adapter.InsertCommand.Parameters["@VoucherNo"].Value = voucheritems.VoucherNo;
                adapter.InsertCommand.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Particulars have been Updated!!");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            
            
            if (keyData == Keys.Enter)
            {
                  
               
                if (ActiveControl is DataGridViewComboBoxEditingControl)
                {
                    #region Combo Box Edit Mode
                    if (grdviewParticulars.CurrentCell.ColumnIndex == 0)
                    {
                        var legdervalue = grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[0].FormattedValue;
                        if (legdervalue != null)
                        {
                            if ((grdviewParticulars.RowCount>1) && (grdviewParticulars.CurrentRow.Index == grdviewParticulars.RowCount - 1))
                            {
                                //if (debittotal != credittotal)  //Calculation before and After Value Change on Previous Row
                                //{
                                //    if (grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex - 1].Cells[2].Value != null)
                                //        debitbalace = decimal.Parse(grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex - 1].Cells[2].Value.ToString());
                                //    decimal correctedvalue = (credittotal - debittotal);
                                //    grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[1].Value = correctedvalue.ToString("0.00");
                                //    //debittotal -= debitbalace;
                                //    debittotal += correctedvalue;
                                //    debitbalace = 0;
                                //    grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[grdviewParticulars.CurrentCell.ColumnIndex + 1];
                                //}
                                grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[grdviewParticulars.CurrentCell.ColumnIndex + 1];
                            }
                            else
                                // GO TO THE FIRST COLUMN, NEXT ROW.
                             grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[grdviewParticulars.CurrentCell.ColumnIndex + 1];
                        }

                    }
                    else
                    {
                        var legdervalue = grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[0].FormattedValue;
                        if (legdervalue != null)
                            grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[grdviewParticulars.CurrentCell.ColumnIndex + 1];
                    }
                    return true;
                    #endregion
                }

                else if (ActiveControl is DataGridViewTextBoxEditingControl)
                {

                    // CHECK IF ITS THE LAST COLUMN.
                    #region Credit Column Edit Mode
                    if (grdviewParticulars.CurrentCell.ColumnIndex == grdviewParticulars.ColumnCount - 1)
                    {
                        decimal cvalue = 0;
                        string columnvalue = grdviewParticulars.CurrentCell.EditedFormattedValue.ToString();
                        if (columnvalue != "")
                        {
                            cvalue = decimal.Parse(columnvalue);
                            grdviewParticulars.CurrentCell.Value = cvalue.ToString("0.00");
                        }
                       
                      if (grdviewParticulars.CurrentCell.ColumnIndex == 2)
                        {
                                decimal difference = beforeupdatevaluecr - cvalue;
                               
                                if (difference != 0)
                                {
                                    credittotal -= beforeupdatevaluecr;
                                    credittotal += cvalue;
                                }
                                else credittotal += 0;

                            beforeupdatevaluecr = 0;
                        }
                       else
                        {
                            debittotal += 0;
                            credittotal += 0;
                        }

                        //Data Entry to VoucherContent Table
                        VoucherItem vitems = new VoucherItem();
                        vitems.LedgerID = int.Parse(grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[0].Value.ToString());
                        if (grdviewParticulars.CurrentCell.ColumnIndex == 1)
                        {
                            vitems.DebitAmount = cvalue;
                            vitems.CreditAmount = 0;
                        }

                        else
                        {
                            vitems.DebitAmount = 0;
                            vitems.CreditAmount = cvalue;
                        }


                        if (txtVNumber.Tag != null)
                        {
                            vitems.VoucherNo = txtVNumber.Text;
                        }
                        else vitems.VoucherNo = hdnguid.Text;

                        
                        vitems.PostingDate = dtpPostingDate.Value;
                        string entryid = "";

                        if (grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[0].Tag != null)
                            entryid = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[0].Tag.ToString();
                        string editmode = "1";
                        if (grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[1].Tag != null)
                            editmode = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[1].Tag.ToString();
                        vitems.VoucherType = Typeid;
                        //Insertion/Update Start
                        grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[0].Tag = SaveVoucherItem(vitems, editmode, entryid);

                        //Insertion Completed
                        // MessageBox.Show(debittotal.ToString() + " " + credittotal.ToString());
                        #region Auto Credit Suggestion and Gridview Leaving Conditions
                        if (editmode == "1")
                        {
                            if (debittotal == credittotal)
                            {
                                if (grdviewParticulars.CurrentRow.Index == grdviewParticulars.RowCount - 2)
                                    txtNarration.Focus();
                                else
                                {
                                    grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[1].Tag = "2";
                                    grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex + 1].Cells[0];
                                }
                            }
                            else
                            {
                                grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[1].Tag = "2";
                                grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex + 1].Cells[0];
                            }
                        }
                        else
                        {
                            if (debittotal != credittotal)
                            {
                                grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[1].Tag = "2";
                                grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex + 1].Cells[0];

                            }
                            else
                            {
                                if (grdviewParticulars.CurrentRow.Index == grdviewParticulars.RowCount - 2)
                                    txtNarration.Focus();
                                else
                                {
                                    grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[1].Tag = "2";
                                    grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex + 1].Cells[0];
                                }
                            }

                        } 
                        #endregion


                       
                        lblDebitTotal.Text = debittotal.ToString("0.00");
                        lblCreditTotal.Text = credittotal.ToString("0.00"); 
                       
                    


                    }
                    #endregion
                    // NEXT COLUMN.
                    #region Debit Column Edit Mode
                    else
                    {

                        decimal cvalue = 0;
                        string columnvalue = grdviewParticulars.CurrentCell.EditedFormattedValue.ToString();
                        if (columnvalue != "")
                        {

                            cvalue = decimal.Parse(columnvalue);
                            grdviewParticulars.CurrentCell.Value = cvalue.ToString("0.00");

                        }
                        else
                        {
                            if (grdviewParticulars.CurrentRow.Index == grdviewParticulars.RowCount - 2)
                            {
                                if (debittotal != credittotal)  //Calculation before and After Value Change on Previous Row
                                {
                                    if (grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[2].Value != null)
                                        creditbalace = decimal.Parse(grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[2].Value.ToString());
                                    decimal correctedvalue = creditbalace + (debittotal - credittotal);
                                    grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[2].Value = correctedvalue.ToString("0.00");
                                    credittotal -= creditbalace;
                                    credittotal += correctedvalue;
                                    creditbalace = 0;
                                }
                            }
                                
                        }


                       if (grdviewParticulars.CurrentCell.ColumnIndex == 1)
                        {
                            decimal difference = beforeupdatevaluedr - cvalue;
                            
                            if (difference != 0)
                                {
                                debittotal -= beforeupdatevaluedr;
                                debittotal += cvalue;
                                }
                             else
                                debittotal += 0;
                            beforeupdatevaluedr = 0;
                        }
                        else
                         {
                                debittotal += 0;
                                credittotal += 0;
                         }
 
                        if (cvalue > 0)
                        {

                            //Data Entry to VoucherContent Table
                            VoucherItem vitems = new VoucherItem();
                            vitems.LedgerID = int.Parse(grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[0].Value.ToString());
                            if (grdviewParticulars.CurrentCell.ColumnIndex == 1)
                            {
                                vitems.DebitAmount = cvalue;
                                vitems.CreditAmount = 0;
                            }

                            else
                            {
                                vitems.DebitAmount = 0;
                                vitems.CreditAmount = cvalue;
                            }


                            if (txtVNumber.Tag != null)
                            {
                                vitems.VoucherNo = txtVNumber.Text;
                            }
                            else vitems.VoucherNo = hdnguid.Text;

                            
                            vitems.PostingDate = dtpPostingDate.Value;
                            string entryid = "";
                            if (grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[0].Tag != null)
                                entryid = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[0].Tag.ToString();
                            string editmode = "1";
                            if (grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[1].Tag != null)
                                editmode = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[1].Tag.ToString();
                            vitems.VoucherType = Typeid;
                            // Voucher Item Insertion
                            grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[0].Tag = SaveVoucherItem(vitems, editmode, entryid);

                            //Insertion Completed

                            #region Auto Credit Suggestion and Gridview Leaving Conditions
                            if (editmode == "1")
                            {
                                if (debittotal == credittotal)
                                {
                                    if (grdviewParticulars.CurrentRow.Index == grdviewParticulars.RowCount - 2)
                                        txtNarration.Focus();
                                    else
                                    {
                                        grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[1].Tag = "2";
                                        grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex + 1].Cells[0];
                                    }
                                }
                                else
                                {
                                    grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[1].Tag = "2";
                                    grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex + 1].Cells[0];
                                }
                            }
                            else
                            {
                                if (debittotal != credittotal)
                                {
                                    grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[1].Tag = "2";
                                    grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex + 1].Cells[0];

                                }
                                else
                                {
                                    if (grdviewParticulars.CurrentRow.Index == grdviewParticulars.RowCount - 2)
                                        txtNarration.Focus();
                                    else
                                    {
                                        grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[1].Tag = "2";
                                        grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex + 1].Cells[0];
                                    }
                                }

                            }
                            #endregion


                            //if (editmode == "1")
                            //{
                            //        grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[1].Tag = "2";
                            //        grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex + 1].Cells[0];
                            //}
                            //else
                            //{
                            //    grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex].Cells[1].Tag = "2";
                            //    grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentCell.RowIndex + 1].Cells[0];
                            //}

                           // debittotal = grdviewParticulars.Rows.Cast<DataGridViewRow>().Sum(t => Convert.ToDecimal(t.Cells[1].Value));
                           // credittotal = grdviewParticulars.Rows.Cast<DataGridViewRow>().Sum(t => Convert.ToDecimal(t.Cells[2].Value));
                            lblDebitTotal.Text = debittotal.ToString("0.00");
                            lblCreditTotal.Text = credittotal.ToString("0.00");
                        } 
                        #endregion
                        else
                        {
                            grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[grdviewParticulars.CurrentCell.ColumnIndex + 1];
                        }
                    }
                    return true;

                }

                else
                {
                    #region Forcing Gridview to Edit Mode
                    if (ActiveControl is DataGridView)
                    {
                        if (grdviewParticulars.CurrentCell.ColumnIndex == 2)
                        {
                            if (grdviewParticulars.CurrentCell.Value != null)
                            {
                                beforeupdatevaluecr = decimal.Parse(grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[2].Value.ToString());
                                //  MessageBox.Show(beforeupdatevalue.ToString());
                                
                                SendKeys.Send("{F2}");


                            }
                            else
                            {
                                if (debittotal != credittotal)
                                {
                                    grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[0];
                                }
                                else
                                {
                                    txtNarration.Focus();
                                }
                            }

                        }
                        else if (grdviewParticulars.CurrentCell.ColumnIndex == 0)
                        {
                            if (grdviewParticulars.CurrentCell.Value != null)
                            {
                                grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[1];
                            }
                            else
                            {
                                grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[0];
                            }
                        }
                        else
                        {
                            grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[1];
                            var cb = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[1].Value;
                            if (cb != null)
                            {
                                beforeupdatevaluedr = decimal.Parse(grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[1].Value.ToString());
                            }

                            SendKeys.Send("{F2}");
                            
                        }
                    } 
                    #endregion
                    else
                    {
                        if (debittotal != credittotal) // Without Debit-Credit Equal can't relaese focus from Gridview
                        {
                            grdviewParticulars.Select();
                            grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[0];
                        }
                        else if (ActiveControl is TextBox) // Tab Going
                        {
                            SendKeys.Send("{TAB}");
                            return true;
                        }
                        else
                        {
                            return base.ProcessCmdKey(ref msg, keyData); //Default Behaviour
                        }
                    }

                }
                return true;
            }
            else if (keyData == Keys.Delete)  //Delete a Row
            {

                DeleteVoucherItem(grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[0].Tag.ToString(), grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[1].FormattedValue.ToString(), grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[2].FormattedValue.ToString());
                grdviewParticulars.Rows.RemoveAt(grdviewParticulars.CurrentRow.Index);
                grdviewParticulars.CurrentCell = grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[grdviewParticulars.CurrentCell.ColumnIndex];
                lblDebitTotal.Text = debittotal.ToString("0.00");
                lblCreditTotal.Text = credittotal.ToString("0.00");
                return true;

            }
            else if (keyData == Keys.Space)
            {
                if (ActiveControl is DataGridViewComboBoxEditingControl)  // Droped down the Combobox
                {
                    SendKeys.Send("{F4}");
                    return true;
                }
                else
                {
                    return base.ProcessCmdKey(ref msg, keyData); //Noraml Acctivities
                }
                
            }
            else
            {
                return base.ProcessCmdKey(ref msg, keyData); // Back to normal Key Command
            }
        }

        //private void CalculateOpenningBalance(VoucherItem vitems, string presentdate, int entryid)
        //{
        //    decimal openningdebit = 0;
        //    decimal openningcredit = 0;
        //    decimal transdebit = 0;
        //    decimal transcredit = 0;
        //    decimal finalopenningdebit = 0;
        //    decimal finalopenningcredit = 0;
        //    decimal ototaldebit = 0;
        //    decimal ototalcredit = 0;
        //    SqlCommand cmd = new SqlCommand("SELECT TOP 1 [LedgerID] ,[DebitAmount],[CreditAmount],[OpenningLDebit],[OpenningLCredit],[PostingDate]  FROM[BasicAccounts].[dbo].[Voucher_Contents]  where LedgerID = @LedgerID and PostingDate <= '"+ presentdate +"' and EntryID>@Entry  order by[EntryID] desc", connection);
        //    connection.Open();
        //    cmd.Parameters.Add("LedgerID", SqlDbType.Int, 32).Value = vitems.LedgerID;
        //    SqlDataReader reader = cmd.ExecuteReader();
        //    if (reader.HasRows)
        //    {
        //        reader.Read();
        //            openningdebit = decimal.Parse(reader["OpenningLDebit"].ToString());
        //            openningcredit = decimal.Parse(reader["OpenningLCredit"].ToString());
        //            transdebit = decimal.Parse(reader["DebitAmount"].ToString());
        //            transcredit = decimal.Parse(reader["CreditAmount"].ToString());
        //        ototaldebit = openningdebit + transdebit;
        //        ototalcredit = openningcredit + transcredit;
        //        if (ototaldebit > ototalcredit)
        //        {
        //            vitems.OpenningLDebit = ototaldebit - ototalcredit;
        //            vitems.OpenningLCredit = 0;
        //        }
        //        else
        //        {
        //            vitems.OpenningLCredit = ototalcredit-ototaldebit;
        //            vitems.OpenningLDebit = 0;
        //        }
        //    }
        //    else
        //    {
        //        reader.Close();
        //        SqlCommand cmd1 = new SqlCommand("SELECT [OpeningDebit],[OpeningCredit]  FROM [Ledgers]  where LedgerID=@LedgerID", connection);
        //        cmd1.Parameters.Add("LedgerID", SqlDbType.Int, 32).Value = vitems.LedgerID;
        //        SqlDataReader reader1 = cmd1.ExecuteReader();
        //        reader1.Read();
        //        finalopenningdebit = decimal.Parse(reader1["OpeningDebit"].ToString());
        //        finalopenningcredit = decimal.Parse(reader1["OpeningCredit"].ToString());
        //        reader1.Close();
        //        vitems.OpenningLDebit = finalopenningdebit;
        //        vitems.OpenningLCredit = finalopenningcredit;
        //    }
        //    connection.Close();

                      
        //}
        private Int64 SaveVoucherItem(VoucherItem vitems,string editmode,string entryid)
        {
            
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            string sqlupdate = null;
            sql = "Insert into Voucher_Contents  (VoucherNo,LedgerID,VoucherType,DebitAmount,CreditAmount,PostingDate) values (@VoucherNo,@LedgerID,@VoucherType,@DebitAmount,@CreditAmount,@PostingDate); SELECT SCOPE_IDENTITY();";

            sqlupdate = "Update  Voucher_Contents set VoucherNo=@VoucherNo,LedgerID=@LedgerID,DebitAmount=@DebitAmount,CreditAmount=@CreditAmount, PostingDate=@PostingDate where EntryID=@EntryID;";
            connection.Open();
            Int64 lastentered = 0;
            if (editmode == "2") //Editing Existing Row
            {
                adapter.InsertCommand = new SqlCommand(sqlupdate, connection);
                adapter.InsertCommand.Parameters.Add("@EntryID", SqlDbType.Int, 64);
                adapter.InsertCommand.Parameters["@EntryID"].Value =Int64.Parse(entryid);
            }
            else  //Insert a new Row
                adapter.InsertCommand = new SqlCommand(sql, connection);

            adapter.InsertCommand.Parameters.Add("@LedgerID", SqlDbType.Int, 32);
            adapter.InsertCommand.Parameters["@LedgerID"].Value = vitems.LedgerID;
            adapter.InsertCommand.Parameters.Add("@VoucherType", SqlDbType.Int, 32);
            adapter.InsertCommand.Parameters["@VoucherType"].Value = vitems.VoucherType;
            adapter.InsertCommand.Parameters.Add("@VoucherNo", SqlDbType.NVarChar, 50);
            adapter.InsertCommand.Parameters["@VoucherNo"].Value = vitems.VoucherNo;
            adapter.InsertCommand.Parameters.Add("@DebitAmount", SqlDbType.Decimal);
            adapter.InsertCommand.Parameters["@DebitAmount"].Value = vitems.DebitAmount;
            adapter.InsertCommand.Parameters.Add("@CreditAmount", SqlDbType.Decimal);
            adapter.InsertCommand.Parameters["@CreditAmount"].Value = vitems.CreditAmount;
            adapter.InsertCommand.Parameters.Add("@PostingDate", SqlDbType.Date);
            adapter.InsertCommand.Parameters["@PostingDate"].Value = vitems.PostingDate;
            if (editmode == "1")
            {
              var last = adapter.InsertCommand.ExecuteScalar();
                lastentered =Int64.Parse(last.ToString());
            }

            else
            {
                lastentered = vitems.EntryID;
                adapter.InsertCommand.ExecuteNonQuery();
            }
               
            connection.Close();
            MessageBox.Show("Inserted/Updated !! ");

            lblDebitTotal.Text = debittotal.ToString("0.00");
            lblCreditTotal.Text = credittotal.ToString("0.00");
            return lastentered;
        }

        private void DeleteVoucherItem(string entryid,string debidcolumnvalue, string creditcolumnvalue)
        {
            DialogResult result = MessageBox.Show("Do You Want to Delete?", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result.Equals(DialogResult.OK))
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                string sql = null;
                sql = "Delete from Voucher_Contents  where EntryID=@EntryID ";

                connection.Open();
                adapter.InsertCommand = new SqlCommand(sql, connection);
                adapter.InsertCommand.Parameters.Add("@EntryID", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@EntryID"].Value = Int64.Parse(entryid);

                adapter.InsertCommand.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Entry have been Deleted !! ");


                decimal cvalue = 0;
                decimal dvalue = 0;

                if (debidcolumnvalue != "")
                    dvalue = decimal.Parse(debidcolumnvalue);
                debittotal -= dvalue;

                if (creditcolumnvalue != "")
                    cvalue = decimal.Parse(creditcolumnvalue);
                credittotal -= cvalue;
            }
  
        }

        private void grdviewParticulars_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > 0)
            {
               
                if (e.ColumnIndex == 0)
                {
                    DataGridViewComboBoxCell cb = (DataGridViewComboBoxCell)grdviewParticulars.Rows[e.RowIndex].Cells[0];
                    if (cb.Value != null)
                    {
                        grdviewParticulars.CurrentCell.Value = cb.Value;
                        //if (cb.Value.ToString() == "1"|| cb.Value.ToString() == "2")
                        //{
                        //   grdviewParticulars.Rows[grdviewParticulars.CurrentRow.Index].Cells[2].Value = debittotal - credittotal;
                            
                        //}
                        // do stuff

                    }
                }
                
                //  grdviewParticulars.Invalidate();
            }
        }

        private void grdviewParticulars_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (this.grdviewParticulars.IsCurrentCellDirty)
            {
               // This fires the cell value changed handler below
                grdviewParticulars.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            decimal debittotal = decimal.Parse(lblDebitTotal.Text);
            decimal credittotal = decimal.Parse(lblCreditTotal.Text);
            if (debittotal == credittotal)
            {
                InsertMainVoucher();
                // Reload the List of Vouchers
                loadvoucherdetails = false;
                LoadVoucherList();
                ClearAll();
            }
            else MessageBox.Show("Debit and Credit Amount doesn't match!");
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do You Want to Delete?", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result.Equals(DialogResult.OK))
            {
                try
                {
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    string sql = null;
                    Voucher voucher = new Voucher();
                    voucher.VoucherNo = txtVNumber.Text;

                    if (txtVNumber.Tag != null)
                    {
                        sql = "Delete from Vouchers where VoucherNo=@VoucherNo; Delete from Voucher_Contents where VoucherNo=@VoucherNo";
                        adapter.DeleteCommand = new SqlCommand(sql, connection);
                        connection.Open();
                        adapter.DeleteCommand.Parameters.Add("@VoucherNo", SqlDbType.NVarChar, 50);
                        adapter.DeleteCommand.Parameters["@VoucherNo"].Value = voucher.VoucherNo;

                        adapter.DeleteCommand.ExecuteNonQuery();

                        connection.Close();
                        MessageBox.Show("Voucher has been Deleted!!");

                        // Reload the List of Vouchers
                        loadvoucherdetails = false;
                        LoadVoucherList();
                        ClearAll();
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }

        private void ListVoucher_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListVoucher.SelectedIndex != -1)
            {
                
                Voucher voucher = new Voucher();
                voucher.VoucherID = (Int32.Parse((ListVoucher.SelectedItem as DataRowView)["VoucherID"].ToString()));
                voucher.VoucherType = (Int32.Parse((ListVoucher.SelectedItem as DataRowView)["VoucherType"].ToString()));
                voucher.VoucherNo = (ListVoucher.SelectedItem as DataRowView)["VoucherNo"].ToString();
                voucher.PostingDate= (DateTime.Parse((ListVoucher.SelectedItem as DataRowView)["PostingDate"].ToString()));
                voucher.Narration= (ListVoucher.SelectedItem as DataRowView)["Narration"].ToString();

                // txtID.Text = (listBox1.SelectedItem as DataRowView)["CategoryID"].ToString();
                txtVNumber.Text = voucher.VoucherNo;
                txtVNumber.Tag = voucher.VoucherID;
                dtpPostingDate.Value = voucher.PostingDate;
                txtNarration.Text = voucher.Narration;
                if (txtVNumber.Tag != null)
                    loadvoucherdetails = true;
                LoadVoucherDetails(voucher.VoucherNo);
               
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        private void ClearAll()
        {
            grdviewParticulars.Rows.Clear();
            LoadVoucherNumber();
            dtpPostingDate.Value = DateTime.Today;
            debittotal = 0;
            credittotal = 0;
            lblDebitTotal.Text = debittotal.ToString("0.00");
            lblCreditTotal.Text = credittotal.ToString("0.00");
            txtNarration.Text = "";
            txtVNumber.Tag = null;
        }
        private void txtVSearch_TextChanged(object sender, EventArgs e)
        {
            ListVoucher.SelectedIndexChanged -= new EventHandler(ListVoucher_SelectedIndexChanged);
            txtVNumber.Tag = null;
            SqlDataAdapter cmd = new SqlDataAdapter("Select * from Vouchers where VoucherType=@VoucherType and VoucherNo like '%" + txtVSearch.Text + "%' order by PostingDate Desc", connection);
            cmd.SelectCommand.Parameters.Add("VoucherType", SqlDbType.Int, 32).Value = Typeid;
            DataSet vouchers = new System.Data.DataSet();
            cmd.Fill(vouchers, "Vouchers");
            DataTable dt = new DataTable();
            dt = vouchers.Tables[0];
            loadvoucherdetails = false;
            ListVoucher.DataSource = dt;
            ListVoucher.SelectedIndexChanged += new EventHandler(ListVoucher_SelectedIndexChanged);
        }

        private void LoadVoucherList()  
        {
            ListVoucher.SelectedIndexChanged -= new EventHandler(ListVoucher_SelectedIndexChanged);
            SqlDataAdapter cmd1 = new SqlDataAdapter("Select * from Vouchers where VoucherType=@VoucherType order by PostingDate Desc", connection);
            cmd1.SelectCommand.Parameters.Add("VoucherType", SqlDbType.Int, 32).Value = Typeid;
            DataSet vouchers = new System.Data.DataSet();
            cmd1.Fill(vouchers, "Vouchers");
            DataTable dt = new DataTable();
            dt = vouchers.Tables[0];
            ListVoucher.DataSource = dt;
            ListVoucher.SelectedIndexChanged += new EventHandler(ListVoucher_SelectedIndexChanged);
        }

        private void grdviewParticulars_Enter(object sender, EventArgs e)
        {
            if (grdviewParticulars.Rows.Count == 1)
            {
                if (txtVNumber.Tag == null)
                {
                    debittotal = 0;
                    credittotal = 0;
                }
            }
         }
    }
}
