﻿using BasicAccounts.AppCodes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasicAccounts
{
    public partial class Main : Form
    {
        static string connetionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connetionString);
        public Main()
        {
            InitializeComponent();
           
        }

        private void naturesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new Nature();
            form.ShowDialog(this);
        }

        private void groupsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new Groups();
            form.ShowDialog(this);
        }

        private void ledgersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new Ledgers();
            form.ShowDialog(this);
        }

        private void Main_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.Alt && e.KeyCode == Keys.W)
            {
               // pnlWelcome.Visible = true;
                
            }
            if (e.KeyCode == Keys.Escape)
            {
                
                Landing objForm = new Landing();
                objForm.TopLevel = false;
                //if (pnlMain.Panel2.Controls.Count > 0)
                //    pnlMain.Panel2.Controls.RemoveAt(0);
                //pnlMain.Panel2.Controls.Add(objForm);
                //objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //objForm.Dock = DockStyle.None;
                //objForm.Show();
            }

        }

        private void Main_Load(object sender, EventArgs e)
        {
            //Vouchers objForm = new Vouchers(1);
            //objForm.TopLevel = false;
            //if (pnlPlayground.Controls.Count > 0)
            //    pnlPlayground.Controls.RemoveAt(0);

            ////pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            //pnlPlayground.Controls.Add(objForm);

            //objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //objForm.Dock = DockStyle.None;
            //objForm.Location = new Point(
            // this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
            // this.pnlPlayground.Height/8 - objForm.Size.Height/8);
            //objForm.Anchor = AnchorStyles.None;
            //objForm.Show();
            LoadCurrentPeriod();
            Groups objForm = new Groups();
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
            
            //PeriodicLedger objForm = new PeriodicLedger(36, DateTime.Parse(lblStartDate.Text), DateTime.Parse(lblEndDate.Text));
            //objForm.TopLevel = false;
            //if (pnlPlayground.Controls.Count > 0)
            //    pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.Controls.Add(objForm);

            //objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //objForm.Dock = DockStyle.None;
            //objForm.Location = new Point(
            // this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
            // this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            //objForm.Anchor = AnchorStyles.None;
            //objForm.Show();

           

        }

        private void LoadCurrentPeriod()
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            sql = "Select StartDate,EndDate  from  PresentPeriod where  ID=@ID";
            connection.Open();
            adapter.SelectCommand = new SqlCommand(sql, connection);
            adapter.SelectCommand.Parameters.Add("ID", SqlDbType.NVarChar, 50).Value = 1;
            SqlDataReader reader = adapter.SelectCommand.ExecuteReader();
            CurrentPeriod period = new CurrentPeriod();

            while (reader.Read())
            {
                period.StartDate = DateTime.Parse(reader["StartDate"].ToString());
                period.EndDate = DateTime.Parse(reader["EndDate"].ToString());
            }
            connection.Close();
            lblStartDate.Text = period.StartDate.ToShortDateString();
            lblEndDate.Text = period.EndDate.ToShortDateString();
        }

        private void payrollReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new Ledgers();
            form.ShowDialog(this);
        }

        private void groupsToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Groups objForm = new Groups();
            objForm.TopLevel = false;
            //if (pnlPlayground.Controls.Count > 0)
            //    pnlPlayground.Controls.RemoveAt(0);
            
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.Bottom;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
            objForm.BringToFront();
        }

        private void ledgersToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Ledgers objForm2 = new Ledgers();
            objForm2.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
            {
                pnlPlayground.Controls.RemoveAt(0);
            }
            
            pnlPlayground.Controls.Add(objForm2);
            objForm2.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm2.Dock = DockStyle.Bottom;
            objForm2.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm2.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm2.Size.Height / 8);
            objForm2.Anchor = AnchorStyles.Top;
            objForm2.Show();
            objForm2.BringToFront();
        }

        private void cashPaymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Vouchers objForm = new Vouchers(1);
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(this.pnlPlayground.Width / 2 - objForm.Size.Width /2, this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void cashReceiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Vouchers objForm = new Vouchers(3);
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void bankPaymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Vouchers objForm = new Vouchers(2);
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void bankReceiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Vouchers objForm = new Vouchers(4);
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void journalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Vouchers objForm = new Vouchers(6);
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void contraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Vouchers objForm = new Vouchers(5);
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void trailBalaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TrialBalance objForm = new TrialBalance();
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void balanceSheetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BalanceSheet objForm = new BalanceSheet();
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void profitLossAcoountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Profit_Loss objForm = new Profit_Loss();
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void cashPaymentToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            VoucherReport objForm = new VoucherReport(1);
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void bankPaymentToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            VoucherReport objForm = new VoucherReport(2);
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void cashReceiptToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            VoucherReport objForm = new VoucherReport(3);
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void bankReceiptToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            VoucherReport objForm = new VoucherReport(4);
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void journalToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            VoucherReport objForm = new VoucherReport(6);
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void contraToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            VoucherReport objForm = new VoucherReport(5);
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void ledgerReportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PeriodicLedger objForm = new PeriodicLedger(36, DateTime.Parse(lblStartDate.Text), DateTime.Parse(lblEndDate.Text));
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();

        }

        private void label1_Click(object sender, EventArgs e)
        {
            //CurrentPeriods objForm = new CurrentPeriods();
            var form = new CurrentPeriods();
            form.ShowDialog(this);
        }

        public DateTime getStartPeriod
        {

          get { return DateTime.Parse(lblStartDate.Text.ToString()); }

          //set { txtChildText.Text = value; }

       }
        public void LoadLedgerReport(int ledgerid,DateTime startdate,DateTime enddate)
        {
            PeriodicLedger objForm = new PeriodicLedger(ledgerid,startdate,enddate);
            objForm.TopLevel = false;
            //if (pnlPlayground.Controls.Count > 0)
            //    pnlPlayground.Controls.RemoveAt(0);

            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }

        private void inventorySetupToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            GroupSummary objForm = new GroupSummary();
            objForm.TopLevel = false;
            if (pnlPlayground.Controls.Count > 0)
                pnlPlayground.Controls.RemoveAt(0);

            //pnlPlayground.GetChildAtPoint(new Point(pnlTopMenu.Location.X+100, pnlPlayground.Location.Y + 500));
            pnlPlayground.Controls.Add(objForm);

            objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            objForm.Dock = DockStyle.None;
            objForm.Location = new Point(
             this.pnlPlayground.Width / 2 - objForm.Size.Width / 2,
             this.pnlPlayground.Height / 8 - objForm.Size.Height / 8);
            objForm.Anchor = AnchorStyles.None;
            objForm.Show();
        }
    }
}
