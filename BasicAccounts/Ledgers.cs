﻿using BasicAccounts.AppCodes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasicAccounts
{
    public partial class Ledgers : Form
    {
        static string connetionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connetionString);
        public Ledgers()
        {
            InitializeComponent();
        }

        private void Ledgers_Load(object sender, EventArgs e)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select GroupName,GroupID from Groups where GroupID>1 and Status=1", connection);
            DataSet groups = new System.Data.DataSet();
            cmd.Fill(groups, "Groups");
            DataTable dt1 = new DataTable();
            dt1 = groups.Tables[0];
            ddlGroups.DataSource = dt1;

            //SqlDataAdapter cmd1 = new SqlDataAdapter("Select LedgerName,LedgerID from Ledgers where  Status=1", connection);
            //DataSet ledgers = new System.Data.DataSet();
            //cmd1.Fill(ledgers, "Ledgers");
            //DataTable dt2 = new DataTable();
            //dt2 = ledgers.Tables[0];
            //ListLedgers.DataSource = dt2;

            ddlDrCr.DisplayMember = "Text";
            ddlDrCr.ValueMember = "Value";
            List<object> DrCr = new List<Object>();
            DrCr.Add(new { Text = "Dr", Value = "Dr" });
            DrCr.Add(new { Text = "Cr", Value = "Cr" });
            ddlDrCr.DataSource = DrCr;

            LoadTreeView();
        }
        private void LoadTreeView()
        {
            
            LedgerTree.Nodes.Clear();
            LedgerTree.BeginUpdate();
            TreeNode assets = new TreeNode();
            TreeNode liabilities = new TreeNode();
            TreeNode income = new TreeNode();
            TreeNode expense = new TreeNode();
            assets.Text = "Assets";
            liabilities.Text = "Liabilities";
            income.Text = "Income";
            expense.Text = "Expense";
            LedgerTree.Nodes.Add(assets);
            LedgerTree.Nodes.Add(liabilities);
            LedgerTree.Nodes.Add(expense);
            LedgerTree.Nodes.Add(income);

            fill_Tree(1);
            fill_Tree(2);
            fill_Tree(4);
            fill_Tree(3);

            LedgerTree.EndUpdate();
            LedgerTree.ExpandAll();
        }
        void fill_Tree(int natureid)

        {

            DataSet PrSet = PDataset("Select GroupID,GroupName,ParentGroupID from Groups where NatureID=" + natureid + " and ParentGroupID=1 and Status=1");

            foreach (DataRow dr in PrSet.Tables[0].Rows)

            {

                TreeNode tnParent = new TreeNode();

                tnParent.Text = dr["GroupName"].ToString();

                tnParent.Tag = dr["GroupID"].ToString();
                tnParent.Name = "Group";
                tnParent.ImageIndex = 0;
                tnParent.SelectedImageIndex = tnParent.ImageIndex;
                 //tnParent.ForeColor = Color.Green;
                LedgerTree.Nodes[natureid - 1].Nodes.Add(tnParent);
                FillChild(tnParent, tnParent.Tag.ToString());
                FillLedger(tnParent, tnParent.Tag.ToString());
            }


        }

        public void FillChild(TreeNode parent, string ParentId)

        {

            DataSet ds = PDataset("Select GroupID,GroupName,ParentGroupID from Groups where ParentGroupID =" + ParentId + " and Status=1");


            foreach (DataRow dr in ds.Tables[0].Rows)

            {

                TreeNode child = new TreeNode();

                child.Text = dr["GroupName"].ToString().Trim();
                child.Tag = dr["GroupID"].ToString().Trim();
                child.Name = "Group";
                child.ImageIndex = 0;
                child.SelectedImageIndex = child.ImageIndex;
              //  child.ForeColor = Color.Blue;
                parent.Nodes.Add(child);
                FillChild(child, child.Tag.ToString());
                FillLedger(child, child.Tag.ToString());
            }

        }
        public void FillLedger(TreeNode parent, string ParentId)

        {

            DataSet ds = PDataset("Select LedgerID,LedgerName,GroupID from Ledgers where GroupID =" + ParentId + " and Status=1");


            foreach (DataRow dr in ds.Tables[0].Rows)

            {

                TreeNode child = new TreeNode();

                child.Text = dr["LedgerName"].ToString().Trim();
                child.Tag = dr["LedgerID"].ToString().Trim();
                child.Name = "Ledger";
                child.ImageIndex = 1;
                child.SelectedImageIndex = child.ImageIndex;
              //  child.ForeColor = Color.Red;
                parent.Nodes.Add(child);
                
            }

        }

        protected DataSet PDataset(string Select_Statement)

        {

            SqlDataAdapter ad = new SqlDataAdapter(Select_Statement, connection);

            DataSet ds = new DataSet();

            ad.Fill(ds);

            return ds;

        }
        private void ClearFiled()
        {
            txtLedgerID.Text = "";
            txtLedgerName.Text = "";
            ddlGroups.SelectedIndex = 0;
            chkFixedAsset.Checked = false;
            chkInventory.Checked = false;
            chkMainTainBill.Checked = false;
            chkRecievable.Checked = false;
            chkServiceAffect.Checked = false;
            chkLedgerStatus.Checked = true;
            txtCreditLimit.Text = "0.00";
            txtOpeningBalance.Text = "0.00";
           
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearFiled();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do You Want to Delete?", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result.Equals(DialogResult.OK))
            {
                try
                {
                    Ledger ledger = new Ledger();
                    ledger.LedgerID = int.Parse(txtLedgerID.Text);
                    ledger.Status = chkLedgerStatus.Checked;
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    string sql = null;
                    sql = "Update Ledgers set Status=@Status where LedgerID=@LedgerID";

                    connection.Open();
                    adapter.InsertCommand = new SqlCommand(sql, connection);

                    adapter.InsertCommand.Parameters.Add("@LedgerID", SqlDbType.Int, 32);
                    adapter.InsertCommand.Parameters["@LedgerID"].Value = ledger.LedgerID;
                    adapter.InsertCommand.Parameters.Add("@Status", SqlDbType.Bit);
                    adapter.InsertCommand.Parameters["@Status"].Value = false;
                    adapter.InsertCommand.ExecuteNonQuery();
                    connection.Close();
                    MessageBox.Show("Ledger have been Deleted !! ");
                    // UpdateTreeView();
                    LoadTreeView();
                    ClearFiled();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                    if (connection.State.ToString() == "open")
                    {
                        connection.Close();
                    }

                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Ledger ledger = new Ledger();
                ledger.LedgerID = int.Parse(txtLedgerID.Text);
                ledger.Status = chkLedgerStatus.Checked;
                ledger.GroupID = int.Parse(ddlGroups.SelectedValue.ToString());
                ledger.LedgerName = txtLedgerName.Text;
                ledger.CreditLimit =decimal.Parse(txtCreditLimit.Text);

                if (ddlDrCr.SelectedValue.ToString() == "Dr")
                {
                    ledger.OpeningDebit = decimal.Parse(txtOpeningBalance.Text);
                    ledger.OpeningCredit = 0;
                }
                else
                {
                    ledger.OpeningDebit = 0;
                    ledger.OpeningCredit = decimal.Parse(txtOpeningBalance.Text);
                }
                ledger.FixedAssetAffect = chkFixedAsset.Checked;
                ledger.InventoryEnabled = chkInventory.Checked;
                ledger.ServiceAffect = chkServiceAffect.Checked;
                ledger.MaintainBalanceBill = chkMainTainBill.Checked;
                ledger.IsRecievable = chkRecievable.Checked;
                ledger.AddDate = dtpAddDate.Value;
                ledger.Address = txtAddress.Text;
                ledger.City = txtCity.Text;
                ledger.ContactPerson = txtCPerson.Text;
                ledger.Phone = txtPhone.Text;
                ledger.Email = txtEmail.Text;

                SqlDataAdapter adapter = new SqlDataAdapter();
                string sql = null;
                sql = "Update  Ledgers set LedgerName=@LedgerName, GroupID=@GroupID,InventoryEnabled=@InventoryEnabled,MaintainBalanceBill=@MaintainBalanceBill, ServiceAffect=@ServiceAffect ,FixedAssetAffect=@FixedAssetAffect,CreditLimit=@CreditLimit,IsRecievable=@IsRecievable, OpeningDebit=@OpeningDebit, OpeningCredit=@OpeningCredit, AddDate=@AddDate,City=@City,Address=@Address,ContactPerson=@ContactPerson,Phone=@Phone,Email=@Email, Status=@Status where LedgerID=@LedgerID";

                connection.Open();
                adapter.InsertCommand = new SqlCommand(sql, connection);
                adapter.InsertCommand.Parameters.Add("@LedgerID", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@LedgerID"].Value = ledger.LedgerID;
                adapter.InsertCommand.Parameters.Add("@LedgerName", SqlDbType.NVarChar, 80);
                adapter.InsertCommand.Parameters["@LedgerName"].Value = ledger.LedgerName;
                adapter.InsertCommand.Parameters.Add("@GroupID", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@GroupID"].Value = ledger.GroupID;
                adapter.InsertCommand.Parameters.Add("@CreditLimit", SqlDbType.Real);
                adapter.InsertCommand.Parameters["@CreditLimit"].Value = ledger.CreditLimit;
                adapter.InsertCommand.Parameters.Add("@OpeningDebit", SqlDbType.Real);
                adapter.InsertCommand.Parameters["@OpeningDebit"].Value = ledger.OpeningDebit;
                adapter.InsertCommand.Parameters.Add("@OpeningCredit", SqlDbType.Real);
                adapter.InsertCommand.Parameters["@OpeningCredit"].Value = ledger.OpeningCredit;
                adapter.InsertCommand.Parameters.Add("@InventoryEnabled", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@InventoryEnabled"].Value = ledger.InventoryEnabled;
                adapter.InsertCommand.Parameters.Add("@MaintainBalanceBill", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@MaintainBalanceBill"].Value = ledger.MaintainBalanceBill;
                adapter.InsertCommand.Parameters.Add("@ServiceAffect", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@ServiceAffect"].Value = ledger.ServiceAffect;
                adapter.InsertCommand.Parameters.Add("@FixedAssetAffect", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@FixedAssetAffect"].Value = ledger.FixedAssetAffect;
                adapter.InsertCommand.Parameters.Add("@IsRecievable", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@IsRecievable"].Value = ledger.IsRecievable;
                adapter.InsertCommand.Parameters.Add("@AddDate", SqlDbType.Date);
                adapter.InsertCommand.Parameters["@AddDate"].Value = ledger.AddDate;
                adapter.InsertCommand.Parameters.Add("@City", SqlDbType.NVarChar, 50);
                adapter.InsertCommand.Parameters["@City"].Value = ledger.City;
                adapter.InsertCommand.Parameters.Add("@Address", SqlDbType.NVarChar, 150);
                adapter.InsertCommand.Parameters["@Address"].Value = ledger.Address;
                adapter.InsertCommand.Parameters.Add("@ContactPerson", SqlDbType.NVarChar, 50);
                adapter.InsertCommand.Parameters["@ContactPerson"].Value = ledger.ContactPerson;
                adapter.InsertCommand.Parameters.Add("@Phone", SqlDbType.NChar, 15);
                adapter.InsertCommand.Parameters["@Phone"].Value = ledger.Phone;
                adapter.InsertCommand.Parameters.Add("@Email", SqlDbType.NVarChar, 80);
                adapter.InsertCommand.Parameters["@Email"].Value = ledger.LedgerName;
                adapter.InsertCommand.Parameters.Add("@Status", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@Status"].Value = ledger.Status;
                adapter.InsertCommand.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Information is Updated !! ");
                // UpdateTreeView();
                LoadTreeView();
                LedgerTree.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                if (connection.State.ToString() == "open")
                {
                    connection.Close();
                }

            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Ledger ledger = new Ledger();
                ledger.Status = chkLedgerStatus.Checked;
                ledger.GroupID = int.Parse(ddlGroups.SelectedValue.ToString());
                ledger.LedgerName = txtLedgerName.Text;
                ledger.CreditLimit = decimal.Parse(txtCreditLimit.Text);
                if (ddlDrCr.SelectedValue.ToString() == "Dr")
                {
                    ledger.OpeningDebit = decimal.Parse(txtOpeningBalance.Text);
                    ledger.OpeningCredit = 0;
                }
                else
                {
                    ledger.OpeningDebit = 0;
                    ledger.OpeningCredit = decimal.Parse(txtOpeningBalance.Text);
                }
                ledger.FixedAssetAffect = chkFixedAsset.Checked;
                ledger.InventoryEnabled = chkInventory.Checked;
                ledger.ServiceAffect = chkServiceAffect.Checked;
                ledger.MaintainBalanceBill = chkMainTainBill.Checked;
                ledger.IsRecievable = chkRecievable.Checked;
                ledger.AddDate = dtpAddDate.Value;
                ledger.Address = txtAddress.Text;
                ledger.City = txtCity.Text;
                ledger.ContactPerson = txtCPerson.Text;
                ledger.Phone = txtPhone.Text;
                ledger.Email = txtEmail.Text;

                SqlDataAdapter adapter = new SqlDataAdapter();
                string sql = null;
                sql = "Insert into  Ledgers (LedgerName, GroupID,InventoryEnabled,MaintainBalanceBill, ServiceAffect ,FixedAssetAffect,CreditLimit,IsRecievable, OpeningDebit, OpeningCredit,AddDate,City,Address,ContactPerson,Phone,Email,Status) values (@LedgerName, @GroupID,@InventoryEnabled,@MaintainBalanceBill, @ServiceAffect ,@FixedAssetAffect,@CreditLimit,@IsRecievable, @OpeningDebit, @OpeningCredit,@AddDate,@City,@Address,@ContactPerson,@Phone,@Email,@Status)";

                connection.Open();
                adapter.InsertCommand = new SqlCommand(sql, connection);
                adapter.InsertCommand.Parameters.Add("@GroupID", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@GroupID"].Value = ledger.GroupID;
                adapter.InsertCommand.Parameters.Add("@LedgerName", SqlDbType.NVarChar,80);
                adapter.InsertCommand.Parameters["@LedgerName"].Value = ledger.LedgerName;
                adapter.InsertCommand.Parameters.Add("@CreditLimit", SqlDbType.Real);
                adapter.InsertCommand.Parameters["@CreditLimit"].Value = ledger.CreditLimit;
                adapter.InsertCommand.Parameters.Add("@OpeningDebit", SqlDbType.Real);
                adapter.InsertCommand.Parameters["@OpeningDebit"].Value = ledger.OpeningDebit;
                adapter.InsertCommand.Parameters.Add("@OpeningCredit", SqlDbType.Real);
                adapter.InsertCommand.Parameters["@OpeningCredit"].Value = ledger.OpeningCredit;
                adapter.InsertCommand.Parameters.Add("@InventoryEnabled", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@InventoryEnabled"].Value = ledger.InventoryEnabled;
                adapter.InsertCommand.Parameters.Add("@MaintainBalanceBill", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@MaintainBalanceBill"].Value = ledger.MaintainBalanceBill;
                adapter.InsertCommand.Parameters.Add("@ServiceAffect", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@ServiceAffect"].Value = ledger.ServiceAffect;
                adapter.InsertCommand.Parameters.Add("@FixedAssetAffect", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@FixedAssetAffect"].Value = ledger.FixedAssetAffect;
                adapter.InsertCommand.Parameters.Add("@IsRecievable", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@IsRecievable"].Value = ledger.IsRecievable;
                adapter.InsertCommand.Parameters.Add("@AddDate", SqlDbType.Date);
                adapter.InsertCommand.Parameters["@AddDate"].Value = ledger.AddDate;
                adapter.InsertCommand.Parameters.Add("@City", SqlDbType.NVarChar, 50);
                adapter.InsertCommand.Parameters["@City"].Value = ledger.City;
                adapter.InsertCommand.Parameters.Add("@Address", SqlDbType.NVarChar, 150);
                adapter.InsertCommand.Parameters["@Address"].Value = ledger.Address;
                adapter.InsertCommand.Parameters.Add("@ContactPerson", SqlDbType.NVarChar, 50);
                adapter.InsertCommand.Parameters["@ContactPerson"].Value = ledger.ContactPerson;
                adapter.InsertCommand.Parameters.Add("@Phone", SqlDbType.NChar, 15);
                adapter.InsertCommand.Parameters["@Phone"].Value = ledger.Phone;
                adapter.InsertCommand.Parameters.Add("@Email", SqlDbType.NVarChar,25);
                adapter.InsertCommand.Parameters["@Email"].Value = ledger.Email;
                adapter.InsertCommand.Parameters.Add("@Status", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@Status"].Value = ledger.Status;
                adapter.InsertCommand.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("New Ledger have been Created !! ");
                // UpdateTreeView();
                LoadTreeView();
               
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                if (connection.State.ToString() == "open")
                {
                    connection.Close();
                }

            }
        }
        private void loadLedgerInfo(int ledgerid)
        {
            SqlCommand cmd = new SqlCommand("Select * from Ledgers where LedgerID=@LedgerID", connection);
            connection.Open();
            cmd.Parameters.Add("LedgerID", SqlDbType.Int, 32).Value = ledgerid;
            SqlDataReader reader = cmd.ExecuteReader();
            Ledger ledger = new Ledger();
            while (reader.Read())
            {
                ledger.LedgerID = ledgerid;
                ledger.GroupID = int.Parse(reader["GroupID"].ToString());
                ledger.LedgerName= reader["LedgerName"].ToString();
                ledger.InventoryEnabled = bool.Parse(reader["InventoryEnabled"].ToString());
                ledger.ServiceAffect = bool.Parse(reader["ServiceAffect"].ToString());
                ledger.MaintainBalanceBill = bool.Parse(reader["MaintainBalanceBill"].ToString());
                ledger.FixedAssetAffect = bool.Parse(reader["FixedAssetAffect"].ToString());
                ledger.IsRecievable = bool.Parse(reader["IsRecievable"].ToString());
                ledger.CreditLimit = decimal.Parse(reader["CreditLimit"].ToString());
                if (decimal.Parse(reader["OpeningDebit"].ToString())>0)
                {
                    ledger.OpeningDebit = decimal.Parse(reader["OpeningDebit"].ToString());
                    ledger.OpeningCredit = 0;
                }
                else
                {
                    ledger.OpeningDebit = 0;
                    ledger.OpeningCredit = decimal.Parse(reader["OpeningCredit"].ToString());
                }
                ledger.Status = bool.Parse(reader["Status"].ToString());
                if(reader["AddDate"].ToString()!="")
                    ledger.AddDate = DateTime.Parse(reader["AddDate"].ToString());
                else
                    ledger.AddDate = DateTime.Today;
                ledger.Address = reader["Address"].ToString();
                ledger.City = reader["City"].ToString(); 
                ledger.ContactPerson = reader["ContactPerson"].ToString();
                ledger.Phone = reader["Phone"].ToString();
                ledger.Email = reader["Email"].ToString();
            }

            connection.Close();
            txtLedgerID.Text = ledger.LedgerID.ToString();
            txtLedgerName.Text = ledger.LedgerName;
            ddlGroups.SelectedValue = ledger.GroupID;
            chkFixedAsset.Checked = ledger.FixedAssetAffect;
            chkInventory.Checked = ledger.InventoryEnabled;
            chkMainTainBill.Checked = ledger.MaintainBalanceBill;
            chkServiceAffect.Checked = ledger.ServiceAffect;
            chkRecievable.Checked = ledger.IsRecievable;
            txtCreditLimit.Text = ledger.CreditLimit.ToString();
            dtpAddDate.Value = ledger.AddDate;
            txtCity.Text = ledger.City;
            txtAddress.Text = ledger.Address;
            txtCPerson.Text = ledger.ContactPerson;
            txtEmail.Text = ledger.Email;
            txtPhone.Text = ledger.Phone;
            if (ledger.OpeningDebit != 0)
            {
                txtOpeningBalance.Text = ledger.OpeningDebit.ToString();
                ddlDrCr.SelectedValue = "Dr";
            }
            else
            {
                txtOpeningBalance.Text = ledger.OpeningCredit.ToString();
                ddlDrCr.SelectedValue = "Cr";
            }
            chkLedgerStatus.Checked = ledger.Status;
        }
       
       

        private void LedgerTree_Click_1(object sender, EventArgs e)
        {
            TreeViewHitTestInfo info = LedgerTree.HitTest(LedgerTree.PointToClient(Cursor.Position));
            if (info != null)
            {
                if (info.Node.Tag != null)
                {
                    if (info.Node.Name == "Ledger")
                        loadLedgerInfo(int.Parse(info.Node.Tag.ToString()));
                    //else MessageBox.Show( info.Node.Text + " is not a Ledger!");
                }
                
               // else MessageBox.Show("This is not a Ledger!");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCreditLimit_Leave(object sender, EventArgs e)
        {
            if (txtCreditLimit.Text != "")
            {
                decimal credit = decimal.Parse(txtCreditLimit.Text);
                txtCreditLimit.Text = credit.ToString("0.00");
            }
            else
            {
                txtCreditLimit.Focus();
            }
           
        }

        private void txtOpeningBalance_Leave(object sender, EventArgs e)
        {
            if(txtOpeningBalance.Text!="")
            { 
            decimal opening = decimal.Parse(txtOpeningBalance.Text);
            txtOpeningBalance.Text = opening.ToString("0.00");
            }
            else
            {
                txtOpeningBalance.Focus();
            }
}
    }
}
