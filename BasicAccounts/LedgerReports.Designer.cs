﻿namespace BasicAccounts
{
    partial class LedgerReports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlMainTrial = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.txtLSearch = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ListLedger = new System.Windows.Forms.ListBox();
            this.sdsLedgers = new System.Windows.Forms.BindingSource(this.components);
            this.dataSetAccounts = new BasicAccounts.DataSetAccounts();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblDebitTotal = new System.Windows.Forms.Label();
            this.lblCreditTotal = new System.Windows.Forms.Label();
            this.grdLedgerReport = new System.Windows.Forms.DataGridView();
            this.clmODebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmOCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmVType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmAccHead = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTDebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblLedgerName = new System.Windows.Forms.Label();
            this.pnlMainTrial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdsLedgers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdLedgerReport)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMainTrial
            // 
            this.pnlMainTrial.BackColor = System.Drawing.SystemColors.Window;
            this.pnlMainTrial.Controls.Add(this.label8);
            this.pnlMainTrial.Controls.Add(this.txtLSearch);
            this.pnlMainTrial.Controls.Add(this.label10);
            this.pnlMainTrial.Controls.Add(this.ListLedger);
            this.pnlMainTrial.Controls.Add(this.label12);
            this.pnlMainTrial.Controls.Add(this.label11);
            this.pnlMainTrial.Controls.Add(this.lblDebitTotal);
            this.pnlMainTrial.Controls.Add(this.lblCreditTotal);
            this.pnlMainTrial.Controls.Add(this.grdLedgerReport);
            this.pnlMainTrial.Location = new System.Drawing.Point(0, 47);
            this.pnlMainTrial.Name = "pnlMainTrial";
            this.pnlMainTrial.Size = new System.Drawing.Size(1141, 546);
            this.pnlMainTrial.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 13);
            this.label8.TabIndex = 105;
            this.label8.Text = "Search by Ledger Name";
            // 
            // txtLSearch
            // 
            this.txtLSearch.BackColor = System.Drawing.SystemColors.Window;
            this.txtLSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLSearch.Location = new System.Drawing.Point(12, 63);
            this.txtLSearch.Name = "txtLSearch";
            this.txtLSearch.Size = new System.Drawing.Size(137, 22);
            this.txtLSearch.TabIndex = 103;
            this.txtLSearch.TextChanged += new System.EventHandler(this.txtLSearch_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 20);
            this.label10.TabIndex = 102;
            this.label10.Text = "Ledger List";
            // 
            // ListLedger
            // 
            this.ListLedger.BackColor = System.Drawing.SystemColors.Window;
            this.ListLedger.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListLedger.DataSource = this.sdsLedgers;
            this.ListLedger.DisplayMember = "LedgerName";
            this.ListLedger.FormattingEnabled = true;
            this.ListLedger.Location = new System.Drawing.Point(12, 92);
            this.ListLedger.Name = "ListLedger";
            this.ListLedger.Size = new System.Drawing.Size(137, 418);
            this.ListLedger.TabIndex = 104;
            this.ListLedger.ValueMember = "LedgerID";
            this.ListLedger.SelectedIndexChanged += new System.EventHandler(this.ListLedger_SelectedIndexChanged);
            // 
            // sdsLedgers
            // 
            this.sdsLedgers.DataMember = "Ledgers";
            this.sdsLedgers.DataSource = this.dataSetAccounts;
            // 
            // dataSetAccounts
            // 
            this.dataSetAccounts.DataSetName = "DataSetAccounts";
            this.dataSetAccounts.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(794, 526);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(322, 13);
            this.label12.TabIndex = 79;
            this.label12.Text = "---------------------------------------------------------------------------------" +
    "------------------------";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(794, 532);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(322, 13);
            this.label11.TabIndex = 78;
            this.label11.Text = "---------------------------------------------------------------------------------" +
    "------------------------";
            // 
            // lblDebitTotal
            // 
            this.lblDebitTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDebitTotal.Location = new System.Drawing.Point(807, 512);
            this.lblDebitTotal.Name = "lblDebitTotal";
            this.lblDebitTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDebitTotal.Size = new System.Drawing.Size(150, 15);
            this.lblDebitTotal.TabIndex = 77;
            this.lblDebitTotal.Text = "0.00";
            this.lblDebitTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCreditTotal
            // 
            this.lblCreditTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreditTotal.Location = new System.Drawing.Point(986, 512);
            this.lblCreditTotal.Name = "lblCreditTotal";
            this.lblCreditTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCreditTotal.Size = new System.Drawing.Size(123, 15);
            this.lblCreditTotal.TabIndex = 76;
            this.lblCreditTotal.Text = "0.00";
            this.lblCreditTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grdLedgerReport
            // 
            this.grdLedgerReport.BackgroundColor = System.Drawing.SystemColors.Window;
            this.grdLedgerReport.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdLedgerReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.grdLedgerReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdLedgerReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmODebit,
            this.clmOCredit,
            this.clmVType,
            this.clmAccHead,
            this.clmAmount,
            this.clmTDebit,
            this.clmTCredit});
            this.grdLedgerReport.GridColor = System.Drawing.SystemColors.Control;
            this.grdLedgerReport.Location = new System.Drawing.Point(155, 15);
            this.grdLedgerReport.Name = "grdLedgerReport";
            this.grdLedgerReport.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grdLedgerReport.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdLedgerReport.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.grdLedgerReport.RowHeadersVisible = false;
            this.grdLedgerReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdLedgerReport.Size = new System.Drawing.Size(974, 497);
            this.grdLedgerReport.TabIndex = 0;
            
            // 
            // clmODebit
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N2";
            this.clmODebit.DefaultCellStyle = dataGridViewCellStyle9;
            this.clmODebit.HeaderText = "Date";
            this.clmODebit.Name = "clmODebit";
            // 
            // clmOCredit
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N2";
            this.clmOCredit.DefaultCellStyle = dataGridViewCellStyle10;
            this.clmOCredit.HeaderText = "Voucher No";
            this.clmOCredit.Name = "clmOCredit";
            this.clmOCredit.Width = 110;
            // 
            // clmVType
            // 
            this.clmVType.HeaderText = "VoucherType";
            this.clmVType.Name = "clmVType";
            // 
            // clmAccHead
            // 
            this.clmAccHead.HeaderText = "Head of Accounts";
            this.clmAccHead.Name = "clmAccHead";
            this.clmAccHead.Width = 262;
            // 
            // clmAmount
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.clmAmount.DefaultCellStyle = dataGridViewCellStyle11;
            this.clmAmount.HeaderText = "";
            this.clmAmount.Name = "clmAmount";
            // 
            // clmTDebit
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "N2";
            this.clmTDebit.DefaultCellStyle = dataGridViewCellStyle12;
            this.clmTDebit.HeaderText = "Debit";
            this.clmTDebit.Name = "clmTDebit";
            this.clmTDebit.Width = 140;
            // 
            // clmTCredit
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "N2";
            this.clmTCredit.DefaultCellStyle = dataGridViewCellStyle13;
            this.clmTCredit.HeaderText = "Credit";
            this.clmTCredit.Name = "clmTCredit";
            this.clmTCredit.Width = 140;
            // 
            // lblLedgerName
            // 
            this.lblLedgerName.AutoSize = true;
            this.lblLedgerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLedgerName.ForeColor = System.Drawing.SystemColors.Window;
            this.lblLedgerName.Location = new System.Drawing.Point(433, 20);
            this.lblLedgerName.Name = "lblLedgerName";
            this.lblLedgerName.Size = new System.Drawing.Size(131, 24);
            this.lblLedgerName.TabIndex = 3;
            this.lblLedgerName.Text = "Ledger Report";
            // 
            // LedgerReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(1141, 593);
            this.Controls.Add(this.lblLedgerName);
            this.Controls.Add(this.pnlMainTrial);
            this.Name = "LedgerReports";
            this.Text = "LedgerReports";
            this.Load += new System.EventHandler(this.LedgerReports_Load);
            this.pnlMainTrial.ResumeLayout(false);
            this.pnlMainTrial.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdsLedgers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdLedgerReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlMainTrial;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblDebitTotal;
        private System.Windows.Forms.Label lblCreditTotal;
        private System.Windows.Forms.DataGridView grdLedgerReport;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtLSearch;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ListBox ListLedger;
        private System.Windows.Forms.BindingSource sdsLedgers;
        private DataSetAccounts dataSetAccounts;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmODebit;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmOCredit;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmVType;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmAccHead;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTDebit;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTCredit;
        private System.Windows.Forms.Label lblLedgerName;
    }
}