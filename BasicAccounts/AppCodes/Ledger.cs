﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.AppCodes;

namespace BasicAccounts.AppCodes
{
    class Ledger
    {
        private int _ledgerID;
        private int _groupID;
        private string _ledgerName;
        private string _city;
        private string _address;
        private string _contactPerson;
        private string _email;
        private string _phone;
        private Boolean _costCentreApplicable;
        private Boolean _inventoryEnabled;
        private Boolean _maintainBalanceBill;
        private Boolean _serviceAffect;
        private Boolean _fixedAssetAffect;
        private decimal _creditLimit;
        private decimal _openingDebit;
        private decimal _openingCredit;
        private DateTime _addDate;
        private Boolean _isRecievable;
        private Boolean _status;

        Validations validation = new Validations();

        public int LedgerID
        {
            get { return _ledgerID; }
            set { validation.RequiredValidation(value, "ID"); this._ledgerID = value; }
        }
        public int GroupID
        {
            get { return _groupID; }
            set { validation.RequiredValidation(value, "Group ID"); this._groupID = value; }
        }

        public string LedgerName
        {
            get { return _ledgerName; }
            set
            {
                validation.RequiredValidation(value, "Ledger Name");
                this._ledgerName = value;
            }
        }
        public decimal OpeningCredit
        {
            get { return _openingCredit; }
            set
            {
                if (value < 0)
                {
                    validation.ValidedDouble(value.ToString());
                    _openingCredit = decimal.Round(value, 2, MidpointRounding.AwayFromZero);
                }
                else _openingCredit = decimal.Round(value, 2, MidpointRounding.AwayFromZero);
            }
        }
        public Boolean CostCentreApplicable
        {
            get { return _costCentreApplicable; }
            set
            {
                if (value)
                {
                    _costCentreApplicable = value;
                }
                else _costCentreApplicable = false;
            }
        }
        public Boolean InventoryEnabled
        {
            get { return _inventoryEnabled; }
            set
            {
                if (value)
                {
                    _inventoryEnabled = value;
                }
                else _inventoryEnabled = false;
            }
        }
        public Boolean MaintainBalanceBill
        {
            get { return _maintainBalanceBill; }
            set
            {
                if (value)
                {
                    _maintainBalanceBill = value;
                }
                else _maintainBalanceBill = false;
            }
        }

        public Boolean ServiceAffect
        {
            get { return _serviceAffect; }
            set
            {
                if (value)
                {
                    _serviceAffect = value;
                }
                else _serviceAffect = false;
            }
        }
        public Boolean FixedAssetAffect
        {
            get { return _fixedAssetAffect; }
            set
            {
                if (value)
                {
                    _fixedAssetAffect = value;
                }
                else _fixedAssetAffect = false;
            }
        }
        public Boolean IsRecievable
        {
            get { return _isRecievable; }
            set
            {
                if (value)
                {
                    _isRecievable = value;
                }
                else _isRecievable = false;
            }
        }

        public decimal CreditLimit
        {
            get { return _creditLimit; }
            set
            {
                if (value<0)
                {
                    validation.ValidedDecimal(value.ToString());
                    _creditLimit = decimal.Round(value, 2, MidpointRounding.AwayFromZero);
                }
                else _creditLimit = decimal.Round(value, 2, MidpointRounding.AwayFromZero);
            }

        }
        public decimal OpeningDebit
        {
            get { return _openingDebit; }
            set
            {
                if (value < 0)
                {
                    validation.ValidedDouble(value.ToString());
                    _openingDebit = decimal.Round(value, 2, MidpointRounding.AwayFromZero);
                }
                else _openingDebit = decimal.Round(value, 2, MidpointRounding.AwayFromZero);
            }

        }

        public string ContactPerson
        {
            get { return this._contactPerson; }
            set
            {
               // validation.RequiredValidation(value, "Contact Person ");
                this._contactPerson = value;
            }
        }
        public string City
        {
            get { return this._city; }
            set
            {
                //validation.RequiredValidation(value, "City ");
                this._city = value;
            }
        }
        public string Email
        {
            get { return this._email; }
            set
            {
               // validation.RequiredValidation(value, "Email ");
               if(this._contactPerson != "")
                    validation.ValidEmail(value);
                this._email = value;
            }
        }
        public string Address
        {
            get { return this._address; }
            set
            {
               // validation.RequiredValidation(value, "Address ");
                this._address = value;
            }
        }

        public string Phone
        {
            get { return this._phone; }
            set
            {
                if (value != null)
                {
                   // validation.PhoneValidation(value);
                    this._phone = value;
                }

            }
        }
        public DateTime AddDate
        {
            get { return this._addDate; }
            set
            {
              //  validation.RequiredValidation(value, "Customaer Add Date ");
                this._addDate = value;
            }
        }

        public Boolean Status
        {
            get { return _status; }
            set
            {
                if (value)
                {
                    _status = value;
                }
                else _status = true;
            }
        }
    }
}
