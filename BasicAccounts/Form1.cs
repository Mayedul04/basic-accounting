﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasicAccounts
{
    public partial class Form1 : Form
    {
        static string connetionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connetionString);
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SqlDataAdapter cmd1 = new SqlDataAdapter("Select LedgerName,LedgerID from Ledgers where  Status=1", connection);
            DataSet ledgers = new System.Data.DataSet();
            cmd1.Fill(ledgers, "Ledgers");
            DataTable dt2 = new DataTable();
            dt2 = ledgers.Tables[0];
            cmb.DataSource = dt2;
            
        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                
                ((ComboBox)e.Control).DropDownStyle = ComboBoxStyle.Simple;
                ((ComboBox)e.Control).AutoCompleteSource = AutoCompleteSource.ListItems;
                ((ComboBox)e.Control).AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
                
            }
        }

      

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > 0)
            {
                //if (e.ColumnIndex == 1)
                //{
                //    DataGridViewTextBoxCell cx = (DataGridViewTextBoxCell)dataGridView1.Rows[e.RowIndex].Cells[1];
                //    if (cx.Value != null)
                //    {
                //        decimal cvalue = decimal.Parse(cx.Value.ToString());
                //        dataGridView1.CurrentCell.Value = cvalue.ToString("0.00");
                //    }
                //}
                if (e.ColumnIndex == 0)
                {
                    DataGridViewComboBoxCell cb = (DataGridViewComboBoxCell)dataGridView1.Rows[e.RowIndex].Cells[0];
                    if (cb.Value != null)
                    {
                        dataGridView1.CurrentCell.Value = cb.Value;
                        // do stuff

                    }
                }
               

               

              //  dataGridView1.Invalidate();
            }
          
        }

        private void dataGridView1_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (this.dataGridView1.IsCurrentCellDirty)
            {
                // This fires the cell value changed handler below
                dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Enter)
            {

                //ON ENTER KEY, GO TO THE NEXT CELL.
                //WHEN THE CURSOR REACHES THE LAST COLUMN, CARRY IT ON TO THE NEXT ROW.

                if (ActiveControl.Name == "dataGridView1")
                {
                    // CHECK IF ITS THE LAST COLUMN
                    if (dataGridView1.CurrentCell.ColumnIndex == dataGridView1.ColumnCount - 1)
                    {
                        // GO TO THE FIRST COLUMN, NEXT ROW.
                        dataGridView1.CurrentCell = dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex + 1].Cells[0];
                    }
                    else
                    {
                        // NEXT COLUMN.
                        dataGridView1.CurrentCell = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[dataGridView1.CurrentCell.ColumnIndex + 1];
                    }

                    return true;
                }
                else if (ActiveControl is DataGridViewComboBoxEditingControl)
                {
                    if (dataGridView1.CurrentCell.ColumnIndex == dataGridView1.ColumnCount - 1)
                    {
                        // GO TO THE FIRST COLUMN, NEXT ROW.
                        dataGridView1.CurrentCell = dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex + 1].Cells[0];
                    }
                    else
                    {

                        dataGridView1.CurrentCell = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[dataGridView1.CurrentCell.ColumnIndex + 1];
                    }
                    return true;
                }
                else if (ActiveControl is DataGridViewTextBoxEditingControl)
                {
                    // CHECK IF ITS THE LAST COLUMN.
                    if (dataGridView1.CurrentCell.ColumnIndex == dataGridView1.ColumnCount - 1)
                    {
                        // GO TO THE FIRST COLUMN, NEXT ROW.
                        string columnvalue = dataGridView1.CurrentCell.EditedFormattedValue.ToString();
                        //if (columnvalue != "")
                        //{
                        decimal cvalue = decimal.Parse(columnvalue);
                        dataGridView1.CurrentCell.Value = cvalue.ToString("0.00");
                        dataGridView1.CurrentCell = dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex + 1].Cells[0];
                    }
                    else
                    {
                        // NEXT COLUMN.
                       string columnvalue = dataGridView1.CurrentCell.EditedFormattedValue.ToString();
                        //if (columnvalue != "")
                        //{
                        decimal cvalue = decimal.Parse(columnvalue);
                        dataGridView1.CurrentCell.Value = cvalue.ToString("0.00");
                        //}

                        dataGridView1.CurrentCell = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[dataGridView1.CurrentCell.ColumnIndex + 1];
                    }
                    return true;

                }

                else
                {
                    SendKeys.Send("{TAB}");
                }
                return true;
            }

            else
            {
                return base.ProcessCmdKey(ref msg, keyData);
            }
        }
    }
}
