﻿namespace BasicAccounts
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataSetAccounts = new BasicAccounts.DataSetAccounts();
            this.sdsLedgers = new System.Windows.Forms.BindingSource(this.components);
            this.cmb = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Amnt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdsLedgers)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cmb,
            this.Amnt});
            this.dataGridView1.Location = new System.Drawing.Point(42, 26);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(244, 150);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            this.dataGridView1.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridView1_CurrentCellDirtyStateChanged);
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            // 
            // dataSetAccounts
            // 
            this.dataSetAccounts.DataSetName = "DataSetAccounts";
            this.dataSetAccounts.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sdsLedgers
            // 
            this.sdsLedgers.DataMember = "Ledgers";
            this.sdsLedgers.DataSource = this.dataSetAccounts;
            // 
            // cmb
            // 
            this.cmb.DataSource = this.sdsLedgers;
            this.cmb.DisplayMember = "LedgerName";
            this.cmb.HeaderText = "Ledger";
            this.cmb.Name = "cmb";
            this.cmb.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cmb.ValueMember = "LedgerID";
            // 
            // Amnt
            // 
            this.Amnt.HeaderText = "Amnt";
            this.Amnt.Name = "Amnt";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(327, 261);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdsLedgers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource sdsLedgers;
        private DataSetAccounts dataSetAccounts;
        private System.Windows.Forms.DataGridViewComboBoxColumn cmb;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amnt;
    }
}