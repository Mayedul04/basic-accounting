﻿namespace BasicAccounts
{
    partial class TrialBalance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlMainTrial = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblDebitTotal = new System.Windows.Forms.Label();
            this.lblCreditTotal = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.grdTrialBalance = new System.Windows.Forms.DataGridView();
            this.clmAccHead = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmODebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmOCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTDebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmCDebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmCCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dtpOpenningDate = new System.Windows.Forms.DateTimePicker();
            this.dtpClosingDate = new System.Windows.Forms.DateTimePicker();
            this.pnlMainTrial.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTrialBalance)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMainTrial
            // 
            this.pnlMainTrial.BackColor = System.Drawing.SystemColors.Window;
            this.pnlMainTrial.Controls.Add(this.label12);
            this.pnlMainTrial.Controls.Add(this.label11);
            this.pnlMainTrial.Controls.Add(this.lblDebitTotal);
            this.pnlMainTrial.Controls.Add(this.lblCreditTotal);
            this.pnlMainTrial.Controls.Add(this.panel2);
            this.pnlMainTrial.Controls.Add(this.panel1);
            this.pnlMainTrial.Controls.Add(this.grdTrialBalance);
            this.pnlMainTrial.Location = new System.Drawing.Point(1, 35);
            this.pnlMainTrial.Name = "pnlMainTrial";
            this.pnlMainTrial.Size = new System.Drawing.Size(1141, 546);
            this.pnlMainTrial.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(802, 531);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(322, 13);
            this.label12.TabIndex = 79;
            this.label12.Text = "---------------------------------------------------------------------------------" +
    "------------------------";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(802, 537);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(322, 13);
            this.label11.TabIndex = 78;
            this.label11.Text = "---------------------------------------------------------------------------------" +
    "------------------------";
            // 
            // lblDebitTotal
            // 
            this.lblDebitTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDebitTotal.Location = new System.Drawing.Point(843, 517);
            this.lblDebitTotal.Name = "lblDebitTotal";
            this.lblDebitTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDebitTotal.Size = new System.Drawing.Size(150, 15);
            this.lblDebitTotal.TabIndex = 77;
            this.lblDebitTotal.Text = "0.00";
            this.lblDebitTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCreditTotal
            // 
            this.lblCreditTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreditTotal.Location = new System.Drawing.Point(1009, 517);
            this.lblCreditTotal.Name = "lblCreditTotal";
            this.lblCreditTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCreditTotal.Size = new System.Drawing.Size(108, 15);
            this.lblCreditTotal.TabIndex = 76;
            this.lblCreditTotal.Text = "0.00";
            this.lblCreditTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(610, -1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(251, 23);
            this.panel2.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(90, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Transactions";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(359, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(251, 23);
            this.panel1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(91, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Opening Balace";
            // 
            // grdTrialBalance
            // 
            this.grdTrialBalance.BackgroundColor = System.Drawing.SystemColors.Window;
            this.grdTrialBalance.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdTrialBalance.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdTrialBalance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdTrialBalance.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmAccHead,
            this.clmODebit,
            this.clmOCredit,
            this.clmTDebit,
            this.clmTCredit,
            this.clmCDebit,
            this.clmCCredit});
            this.grdTrialBalance.GridColor = System.Drawing.SystemColors.Control;
            this.grdTrialBalance.Location = new System.Drawing.Point(12, 22);
            this.grdTrialBalance.Name = "grdTrialBalance";
            this.grdTrialBalance.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grdTrialBalance.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdTrialBalance.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.grdTrialBalance.RowHeadersVisible = false;
            this.grdTrialBalance.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdTrialBalance.Size = new System.Drawing.Size(1117, 492);
            this.grdTrialBalance.TabIndex = 0;
            this.grdTrialBalance.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdTrialBalance_CellContentClick);
            // 
            // clmAccHead
            // 
            this.clmAccHead.HeaderText = "Head of Accounts";
            this.clmAccHead.Name = "clmAccHead";
            this.clmAccHead.Width = 347;
            // 
            // clmODebit
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            this.clmODebit.DefaultCellStyle = dataGridViewCellStyle2;
            this.clmODebit.HeaderText = "Debit";
            this.clmODebit.Name = "clmODebit";
            this.clmODebit.Width = 125;
            // 
            // clmOCredit
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            this.clmOCredit.DefaultCellStyle = dataGridViewCellStyle3;
            this.clmOCredit.HeaderText = "Credit";
            this.clmOCredit.Name = "clmOCredit";
            this.clmOCredit.Width = 125;
            // 
            // clmTDebit
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            this.clmTDebit.DefaultCellStyle = dataGridViewCellStyle4;
            this.clmTDebit.HeaderText = "Debit";
            this.clmTDebit.Name = "clmTDebit";
            this.clmTDebit.Width = 125;
            // 
            // clmTCredit
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            this.clmTCredit.DefaultCellStyle = dataGridViewCellStyle5;
            this.clmTCredit.HeaderText = "Credit";
            this.clmTCredit.Name = "clmTCredit";
            this.clmTCredit.Width = 125;
            // 
            // clmCDebit
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            this.clmCDebit.DefaultCellStyle = dataGridViewCellStyle6;
            this.clmCDebit.HeaderText = "Debit";
            this.clmCDebit.Name = "clmCDebit";
            this.clmCDebit.Width = 125;
            // 
            // clmCCredit
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            this.clmCCredit.DefaultCellStyle = dataGridViewCellStyle7;
            this.clmCCredit.HeaderText = "Credit";
            this.clmCCredit.Name = "clmCCredit";
            this.clmCCredit.Width = 125;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Window;
            this.label1.Location = new System.Drawing.Point(542, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Trial Balance";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Window;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label4);
            this.panel3.Location = new System.Drawing.Point(862, 34);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(251, 23);
            this.panel3.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(76, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Closing Balace";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1054, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Create View";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dtpOpenningDate
            // 
            this.dtpOpenningDate.CustomFormat = "dd-MM-yyyy";
            this.dtpOpenningDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOpenningDate.Location = new System.Drawing.Point(705, 8);
            this.dtpOpenningDate.Name = "dtpOpenningDate";
            this.dtpOpenningDate.Size = new System.Drawing.Size(150, 20);
            this.dtpOpenningDate.TabIndex = 5;
            this.dtpOpenningDate.ValueChanged += new System.EventHandler(this.dtpOpenningDate_ValueChanged);
            // 
            // dtpClosingDate
            // 
            this.dtpClosingDate.CustomFormat = "dd-MM-yyyy";
            this.dtpClosingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpClosingDate.Location = new System.Drawing.Point(880, 8);
            this.dtpClosingDate.Name = "dtpClosingDate";
            this.dtpClosingDate.Size = new System.Drawing.Size(150, 20);
            this.dtpClosingDate.TabIndex = 6;
            // 
            // TrialBalance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(1141, 593);
            this.Controls.Add(this.dtpClosingDate);
            this.Controls.Add(this.dtpOpenningDate);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlMainTrial);
            this.Name = "TrialBalance";
            this.Text = "TrialBalance";
            this.Load += new System.EventHandler(this.TrialBalance_Load);
            this.pnlMainTrial.ResumeLayout(false);
            this.pnlMainTrial.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTrialBalance)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlMainTrial;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView grdTrialBalance;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblDebitTotal;
        private System.Windows.Forms.Label lblCreditTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmAccHead;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmODebit;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmOCredit;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTDebit;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTCredit;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmCDebit;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmCCredit;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dtpOpenningDate;
        private System.Windows.Forms.DateTimePicker dtpClosingDate;
    }
}