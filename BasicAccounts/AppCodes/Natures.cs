﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.AppCodes;

namespace BasicAccounts.AppCodes
{
    class Natures
    {
        private int _natureID;
        private string _nature;
        private Boolean _status;

        Validations validation = new Validations();
        public int NatureID
        {
            get { return _natureID; }
            set { validation.RequiredValidation(value, "ID"); this._natureID = value; }
        }
        public string Nature
        {
            get { return _nature; }
            set
            {
                validation.RequiredValidation(value, "Nature");
                this._nature = value;
            }
        }
        public Boolean Status
        {
            get { return _status; }
            set
            {
                if (value)
                {
                    _status = value;
                }
                else _status = true;
            }
        }
    }
}
