﻿namespace BasicAccounts
{
    partial class VoucherReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlMainTrial = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblDebitTotal = new System.Windows.Forms.Label();
            this.lblCreditTotal = new System.Windows.Forms.Label();
            this.grdVoucherRepors = new System.Windows.Forms.DataGridView();
            this.clmODebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmOCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmAccHead = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTDebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblVoucherType = new System.Windows.Forms.Label();
            this.dtpClosingDate = new System.Windows.Forms.DateTimePicker();
            this.dtpOpenningDate = new System.Windows.Forms.DateTimePicker();
            this.btnPreview = new System.Windows.Forms.Button();
            this.pnlMainTrial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdVoucherRepors)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMainTrial
            // 
            this.pnlMainTrial.BackColor = System.Drawing.SystemColors.Window;
            this.pnlMainTrial.Controls.Add(this.label12);
            this.pnlMainTrial.Controls.Add(this.label11);
            this.pnlMainTrial.Controls.Add(this.lblDebitTotal);
            this.pnlMainTrial.Controls.Add(this.lblCreditTotal);
            this.pnlMainTrial.Controls.Add(this.grdVoucherRepors);
            this.pnlMainTrial.Location = new System.Drawing.Point(0, 47);
            this.pnlMainTrial.Name = "pnlMainTrial";
            this.pnlMainTrial.Size = new System.Drawing.Size(1141, 546);
            this.pnlMainTrial.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(794, 526);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(322, 13);
            this.label12.TabIndex = 79;
            this.label12.Text = "---------------------------------------------------------------------------------" +
    "------------------------";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(794, 532);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(322, 13);
            this.label11.TabIndex = 78;
            this.label11.Text = "---------------------------------------------------------------------------------" +
    "------------------------";
            // 
            // lblDebitTotal
            // 
            this.lblDebitTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDebitTotal.Location = new System.Drawing.Point(807, 512);
            this.lblDebitTotal.Name = "lblDebitTotal";
            this.lblDebitTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDebitTotal.Size = new System.Drawing.Size(150, 15);
            this.lblDebitTotal.TabIndex = 77;
            this.lblDebitTotal.Text = "0.00";
            this.lblDebitTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCreditTotal
            // 
            this.lblCreditTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreditTotal.Location = new System.Drawing.Point(986, 512);
            this.lblCreditTotal.Name = "lblCreditTotal";
            this.lblCreditTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCreditTotal.Size = new System.Drawing.Size(123, 15);
            this.lblCreditTotal.TabIndex = 76;
            this.lblCreditTotal.Text = "0.00";
            this.lblCreditTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grdVoucherRepors
            // 
            this.grdVoucherRepors.BackgroundColor = System.Drawing.SystemColors.Window;
            this.grdVoucherRepors.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdVoucherRepors.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.grdVoucherRepors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdVoucherRepors.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmODebit,
            this.clmOCredit,
            this.clmAccHead,
            this.clmTDebit,
            this.clmTCredit});
            this.grdVoucherRepors.GridColor = System.Drawing.SystemColors.Control;
            this.grdVoucherRepors.Location = new System.Drawing.Point(12, 15);
            this.grdVoucherRepors.Name = "grdVoucherRepors";
            this.grdVoucherRepors.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grdVoucherRepors.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdVoucherRepors.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.grdVoucherRepors.RowHeadersVisible = false;
            this.grdVoucherRepors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdVoucherRepors.Size = new System.Drawing.Size(1117, 497);
            this.grdVoucherRepors.TabIndex = 0;
            // 
            // clmODebit
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.Format = "N2";
            this.clmODebit.DefaultCellStyle = dataGridViewCellStyle14;
            this.clmODebit.HeaderText = "Date";
            this.clmODebit.Name = "clmODebit";
            this.clmODebit.Width = 125;
            // 
            // clmOCredit
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle15.Format = "N2";
            this.clmOCredit.DefaultCellStyle = dataGridViewCellStyle15;
            this.clmOCredit.HeaderText = "Voucher Number";
            this.clmOCredit.Name = "clmOCredit";
            this.clmOCredit.Width = 200;
            // 
            // clmAccHead
            // 
            this.clmAccHead.HeaderText = "Head of Accounts";
            this.clmAccHead.Name = "clmAccHead";
            this.clmAccHead.Width = 462;
            // 
            // clmTDebit
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle16.Format = "N2";
            this.clmTDebit.DefaultCellStyle = dataGridViewCellStyle16;
            this.clmTDebit.HeaderText = "Debit";
            this.clmTDebit.Name = "clmTDebit";
            this.clmTDebit.Width = 155;
            // 
            // clmTCredit
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle17.Format = "N2";
            this.clmTCredit.DefaultCellStyle = dataGridViewCellStyle17;
            this.clmTCredit.HeaderText = "Credit";
            this.clmTCredit.Name = "clmTCredit";
            this.clmTCredit.Width = 155;
            // 
            // lblVoucherType
            // 
            this.lblVoucherType.AutoSize = true;
            this.lblVoucherType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVoucherType.ForeColor = System.Drawing.SystemColors.Window;
            this.lblVoucherType.Location = new System.Drawing.Point(444, 10);
            this.lblVoucherType.Name = "lblVoucherType";
            this.lblVoucherType.Size = new System.Drawing.Size(144, 24);
            this.lblVoucherType.TabIndex = 2;
            this.lblVoucherType.Text = "Voucher Report";
            // 
            // dtpClosingDate
            // 
            this.dtpClosingDate.CustomFormat = "dd-MM-yyyy";
            this.dtpClosingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpClosingDate.Location = new System.Drawing.Point(875, 16);
            this.dtpClosingDate.Name = "dtpClosingDate";
            this.dtpClosingDate.Size = new System.Drawing.Size(150, 20);
            this.dtpClosingDate.TabIndex = 9;
            // 
            // dtpOpenningDate
            // 
            this.dtpOpenningDate.CustomFormat = "dd-MM-yyyy";
            this.dtpOpenningDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOpenningDate.Location = new System.Drawing.Point(713, 16);
            this.dtpOpenningDate.Name = "dtpOpenningDate";
            this.dtpOpenningDate.Size = new System.Drawing.Size(150, 20);
            this.dtpOpenningDate.TabIndex = 8;
            this.dtpOpenningDate.ValueChanged += new System.EventHandler(this.dtpOpenningDate_ValueChanged);
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(1034, 14);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPreview.TabIndex = 7;
            this.btnPreview.Text = "Create View";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // VoucherReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(1141, 593);
            this.Controls.Add(this.dtpClosingDate);
            this.Controls.Add(this.dtpOpenningDate);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.lblVoucherType);
            this.Controls.Add(this.pnlMainTrial);
            this.Name = "VoucherReport";
            this.Text = "VoucherReport";
            this.Load += new System.EventHandler(this.VoucherReport_Load);
            this.pnlMainTrial.ResumeLayout(false);
            this.pnlMainTrial.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdVoucherRepors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlMainTrial;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblDebitTotal;
        private System.Windows.Forms.Label lblCreditTotal;
        private System.Windows.Forms.DataGridView grdVoucherRepors;
        private System.Windows.Forms.Label lblVoucherType;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmODebit;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmOCredit;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmAccHead;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTDebit;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTCredit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpClosingDate;
        private System.Windows.Forms.DateTimePicker dtpOpenningDate;
        private System.Windows.Forms.Button btnPreview;
    }
}