﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("AdjustableHeightTextbox")]
[assembly: AssemblyDescription("A TextBox control that allows you to set the height of the single-line Textbox")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Adrian Hayes")]
[assembly: AssemblyProduct("AdjustableHeightTextbox Control")]
[assembly: AssemblyCopyright("Copyright © Adrian Hayes, 2008")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("645040cb-fe68-4255-9733-09df3e511c8a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.1")]
[assembly: AssemblyFileVersion("1.0.0.1")]
