﻿using BasicAccounts.AppCodes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Web;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace BasicAccounts
{
    public partial class Groups : Form
    {
        static string connetionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connetionString);
        public Groups()
        {
            InitializeComponent();
        }

        private void Groups_Load(object sender, EventArgs e)
        {
           

            SqlDataAdapter cmd1 = new SqlDataAdapter("Select Nature,NatureID from Natures", connection);
            DataSet natures = new System.Data.DataSet();
            cmd1.Fill(natures, "NatureList");
            DataTable dt = new DataTable();
            dt = natures.Tables[0];
            ddlNature.DataSource = dt;

            LoadParents();



            ddlCashFlow.DisplayMember = "Text";
            ddlCashFlow.ValueMember = "Value";
            List<object> cashflows = new List<Object>();
            cashflows.Add(new { Text = "Operating Activities", Value = "Operating Activities" });
            cashflows.Add(new { Text = "Financing Activities", Value = "Financing Activities" });
            cashflows.Add(new { Text = "Investing Activities", Value = "Investing Activities" });
            ddlCashFlow.DataSource = cashflows;

            LoadTreeView();
        }

        private void LoadParents()
        {
            ddlParent.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            ddlParent.AutoCompleteSource = AutoCompleteSource.ListItems;
            SqlDataAdapter cmd = new SqlDataAdapter("Select GroupName,GroupID from Groups where GroupID!=0 and Status=1", connection);
            DataSet groups = new System.Data.DataSet();
            cmd.Fill(groups, "Groups");
            DataTable dt1 = new DataTable();
            dt1 = groups.Tables[0];
            ddlParent.DataSource = dt1;
        }

        private void LoadTreeView()
        {
            GroupTree.Nodes.Clear();
            GroupTree.BeginUpdate();
            TreeNode assets = new TreeNode();
            TreeNode liabilities = new TreeNode();
            TreeNode income = new TreeNode();
            TreeNode expense = new TreeNode();
            assets.Text = "Assets";
            liabilities.Text = "Liabilities";
            income.Text = "Income";
            expense.Text = "Expense";
            GroupTree.Nodes.Add(assets);
            GroupTree.Nodes.Add(liabilities);
            GroupTree.Nodes.Add(expense);
            GroupTree.Nodes.Add(income);

            fill_Tree(1);
            fill_Tree(2);
            fill_Tree(4);
            fill_Tree(3);
            
            GroupTree.EndUpdate();
            GroupTree.ExpandAll();
        }
        void fill_Tree(int natureid)

        {
            
            DataSet PrSet = PDataset("Select GroupID,GroupName,ParentGroupID from Groups where NatureID=" + natureid + " and ParentGroupID=1 and Status=1");
            
            foreach (DataRow dr in PrSet.Tables[0].Rows)

            {

                TreeNode tnParent = new TreeNode();

                tnParent.Text = dr["GroupName"].ToString();

                tnParent.Tag = dr["GroupID"].ToString();
                tnParent.ImageIndex = 0;
                GroupTree.Nodes[natureid-1].Nodes.Add(tnParent);
                FillChild(tnParent, tnParent.Tag.ToString());

            }


        }

        public void FillChild(TreeNode parent, string ParentId)

        {

            DataSet ds = PDataset("Select GroupID,GroupName,ParentGroupID from Groups where ParentGroupID =" + ParentId + " and Status=1");

           
            foreach (DataRow dr in ds.Tables[0].Rows)

            {

                TreeNode child = new TreeNode();

                child.Text = dr["GroupName"].ToString().Trim();
                child.Tag = dr["GroupID"].ToString().Trim();
                child.ImageIndex = 0;
                parent.Nodes.Add(child);
                FillChild(child, child.Tag.ToString());
            }

        }
        
        protected DataSet PDataset(string Select_Statement)

        {

           SqlDataAdapter ad = new SqlDataAdapter(Select_Statement, connection);

            DataSet ds = new DataSet();

            ad.Fill(ds);

            return ds;

        }
        private void ClearFiled()
        {
            txtGroupID.Text = "";
            txtGroupName.Text = "";
            ddlParent.SelectedIndex = 0;
            ddlNature.SelectedIndex = 0;
            ddlCashFlow.SelectedIndex = 0;
            chkAffectGross.Checked = false;
            chkGroupStatus.Checked = true;
        }
        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearFiled();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do You Want to Delete?", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result.Equals(DialogResult.OK))
            {
                try
                {
                    Group group = new Group();
                    group.GroupID = int.Parse(txtGroupID.Text);
                    group.Status = chkGroupStatus.Checked;
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    string sql = null;
                    sql = "Update Groups set Status=@Status where GroupID=@GroupID";

                    connection.Open();
                    adapter.InsertCommand = new SqlCommand(sql, connection);

                    adapter.InsertCommand.Parameters.Add("@GroupID", SqlDbType.Int, 32);
                    adapter.InsertCommand.Parameters["@GroupID"].Value = group.GroupID;
                    adapter.InsertCommand.Parameters.Add("@Status", SqlDbType.Bit);
                    adapter.InsertCommand.Parameters["@Status"].Value = false;
                    adapter.InsertCommand.ExecuteNonQuery();
                    connection.Close();
                    MessageBox.Show("Group have been Deleted !! ");
                    // UpdateTreeView();
                    LoadTreeView();
                    GroupTree.Focus();
                    LoadParents();
                    ClearFiled();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                    if (connection.State.ToString() == "open")
                    {
                        connection.Close();
                    }

                }
            }
            else
            {
            }
           
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Group group = new Group();
                group.GroupID = int.Parse(txtGroupID.Text);
                group.ParentGroupID = int.Parse(ddlParent.SelectedValue.ToString());
                group.GroupName = txtGroupName.Text;
                group.NatureID = int.Parse(ddlNature.SelectedValue.ToString());
                group.CashFlowType = ddlCashFlow.SelectedValue.ToString();
                group.AffectonGrossProfit = chkAffectGross.Checked;
                group.Status = chkGroupStatus.Checked;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string sql = null;
                sql = "Update  Groups set GroupName=@GroupName, ParentGroupID=@ParentGroupID,NatureID=@NatureID,CashFlowType=@CashFlowType, AffectonGrossProfit=@AffectonGrossProfit ,Status=@Status where GroupID=@GroupID";

                connection.Open();
                adapter.InsertCommand = new SqlCommand(sql, connection);
                adapter.InsertCommand.Parameters.Add("@NatureID", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@NatureID"].Value = group.NatureID;
                adapter.InsertCommand.Parameters.Add("@GroupID", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@GroupID"].Value = group.GroupID;
                adapter.InsertCommand.Parameters.Add("@ParentGroupID", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@ParentGroupID"].Value = group.ParentGroupID;
                adapter.InsertCommand.Parameters.Add("@GroupName", SqlDbType.NVarChar, 80);
                adapter.InsertCommand.Parameters["@GroupName"].Value = group.GroupName;
                adapter.InsertCommand.Parameters.Add("@CashFlowType", SqlDbType.NVarChar,50);
                adapter.InsertCommand.Parameters["@CashFlowType"].Value = group.CashFlowType;
                adapter.InsertCommand.Parameters.Add("@AffectonGrossProfit", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@AffectonGrossProfit"].Value = group.AffectonGrossProfit;
                adapter.InsertCommand.Parameters.Add("@Status", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@Status"].Value = group.Status;
                adapter.InsertCommand.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Information is Updated !! ");
                // UpdateTreeView();
                LoadTreeView();
                LoadParents();
                GroupTree.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                if (connection.State.ToString() == "open")
                {
                    connection.Close();
                }

            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Group group = new Group();
                group.ParentGroupID = int.Parse(ddlParent.SelectedValue.ToString());
                group.GroupName = txtGroupName.Text;
                group.NatureID = int.Parse(ddlNature.SelectedValue.ToString());
                group.CashFlowType = ddlCashFlow.SelectedValue.ToString();
                group.AffectonGrossProfit = chkAffectGross.Checked;
                group.Status = chkGroupStatus.Checked;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string sql = null;
                sql = "Insert into Groups  (GroupName,ParentGroupID,NatureID,CashFlowType,AffectonGrossProfit,Status) values (@GroupName,@ParentGroupID,@NatureID,@CashFlowType ,@AffectonGrossProfit,@Status) ";

                connection.Open();
                adapter.InsertCommand = new SqlCommand(sql, connection);
                adapter.InsertCommand.Parameters.Add("@NatureID", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@NatureID"].Value = group.NatureID;
                adapter.InsertCommand.Parameters.Add("@ParentGroupID", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@ParentGroupID"].Value = group.ParentGroupID;
                adapter.InsertCommand.Parameters.Add("@GroupName", SqlDbType.NVarChar, 80);
                adapter.InsertCommand.Parameters["@GroupName"].Value = group.GroupName;
                adapter.InsertCommand.Parameters.Add("@CashFlowType", SqlDbType.NVarChar, 50);
                adapter.InsertCommand.Parameters["@CashFlowType"].Value = group.CashFlowType;
                adapter.InsertCommand.Parameters.Add("@AffectonGrossProfit", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@AffectonGrossProfit"].Value = group.AffectonGrossProfit;
                adapter.InsertCommand.Parameters.Add("@Status", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@Status"].Value = group.Status;
                adapter.InsertCommand.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("New Group have been added !! ");
                // UpdateTreeView();
                LoadTreeView();
                LoadParents();
                GroupTree.Focus();
                ClearFiled();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                if (connection.State.ToString() == "open")
                {
                    connection.Close();
                }

            }
        }

        private void loadParentInfo(int parentid)
        {
            SqlCommand cmd = new SqlCommand("Select NatureID,CashFlowType from Groups where GroupID=@GroupID", connection);
            connection.Open();
            cmd.Parameters.Add("GroupID", SqlDbType.Int, 32).Value = parentid;
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ddlNature.SelectedValue=reader.GetInt32(0);
                ddlCashFlow.SelectedValue = reader.GetString(1);
            }
           
            connection.Close();
        }
        private void loadGroupInfo(int groupid)
        {
            SqlCommand cmd = new SqlCommand("Select * from Groups where GroupID=@GroupID", connection);
            connection.Open();
            cmd.Parameters.Add("GroupID", SqlDbType.Int, 32).Value = groupid;
            SqlDataReader reader = cmd.ExecuteReader();
            Group group = new Group();
            while (reader.Read())
            {
                group.GroupID = groupid;
                group.GroupName = reader["GroupName"].ToString();
                group.ParentGroupID = int.Parse(reader["ParentGroupID"].ToString());
                group.NatureID = int.Parse(reader["NatureID"].ToString());
                group.CashFlowType = reader["CashFlowType"].ToString();
                group.AffectonGrossProfit = bool.Parse(reader["AffectonGrossProfit"].ToString());
                group.Status = bool.Parse(reader["Status"].ToString());
            }

            connection.Close();
            txtGroupID.Text = group.GroupID.ToString();
            txtGroupName.Text = group.GroupName;
            ddlParent.SelectedValue = group.ParentGroupID;
            ddlNature.SelectedValue = group.NatureID;
            ddlCashFlow.SelectedValue = group.CashFlowType;
            chkAffectGross.Checked = group.AffectonGrossProfit;
            chkGroupStatus.Checked = group.Status;
        }
        private void ddlParent_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadParentInfo(int.Parse(ddlParent.SelectedValue.ToString()));
        }

        private void GroupTree_Click(object sender, EventArgs e)
        {
            TreeViewHitTestInfo info = GroupTree.HitTest(GroupTree.PointToClient(Cursor.Position));
            if (info != null)
            {
                if (info.Node.Tag != null)
                    loadGroupInfo(int.Parse(info.Node.Tag.ToString()));
                else MessageBox.Show("This is not a Group!");
            }
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ddlCashFlow_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ddlParent_Enter(object sender, EventArgs e)
        {
            ddlParent.DroppedDown = true;
        }

        private void ddlParent_Leave(object sender, EventArgs e)
        {
            ddlParent.DroppedDown = false;
        }

        private void ddlCashFlow_Enter(object sender, EventArgs e)
        {
            ddlCashFlow.DroppedDown = true;
        }

        private void ddlCashFlow_Leave(object sender, EventArgs e)
        {
            ddlCashFlow.DroppedDown = false;
        }
    }
}
