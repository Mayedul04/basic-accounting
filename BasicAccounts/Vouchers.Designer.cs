﻿namespace BasicAccounts
{
    partial class Vouchers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.txtVSearch = new System.Windows.Forms.TextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.ListVoucher = new System.Windows.Forms.ListBox();
            this.sdsVouchers = new System.Windows.Forms.BindingSource(this.components);
            this.dataSetAccounts = new BasicAccounts.DataSetAccounts();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.hdnguid = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNarration = new System.Windows.Forms.TextBox();
            this.lblDebitTotal = new System.Windows.Forms.Label();
            this.lblCreditTotal = new System.Windows.Forms.Label();
            this.grdviewParticulars = new System.Windows.Forms.DataGridView();
            this.clmnLedgers = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.sdsLedgers = new System.Windows.Forms.BindingSource(this.components);
            this.clmnDebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtVType = new System.Windows.Forms.TextBox();
            this.dtpPostingDate = new System.Windows.Forms.DateTimePicker();
            this.txtVNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.sdsVoucherItem = new System.Windows.Forms.BindingSource(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdsVouchers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetAccounts)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdviewParticulars)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdsLedgers)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdsVoucherItem)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtVSearch);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.ListVoucher);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(0, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1141, 606);
            this.panel1.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(138, 13);
            this.label8.TabIndex = 101;
            this.label8.Text = "Search by Voucher Number";
            // 
            // txtVSearch
            // 
            this.txtVSearch.BackColor = System.Drawing.SystemColors.Window;
            this.txtVSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVSearch.Location = new System.Drawing.Point(14, 61);
            this.txtVSearch.Name = "txtVSearch";
            this.txtVSearch.Size = new System.Drawing.Size(137, 22);
            this.txtVSearch.TabIndex = 9;
            this.txtVSearch.TextChanged += new System.EventHandler(this.txtVSearch_TextChanged);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(188)))), ((int)(((byte)(150)))));
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(753, 552);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "DELETE";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(188)))), ((int)(((byte)(150)))));
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(642, 552);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "CLEAR";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(14, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 20);
            this.label10.TabIndex = 1;
            this.label10.Text = "Voucher List";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(188)))), ((int)(((byte)(150)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(531, 552);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "ACCEPT";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ListVoucher
            // 
            this.ListVoucher.BackColor = System.Drawing.SystemColors.Window;
            this.ListVoucher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListVoucher.DataSource = this.sdsVouchers;
            this.ListVoucher.DisplayMember = "VoucherNo";
            this.ListVoucher.FormattingEnabled = true;
            this.ListVoucher.Location = new System.Drawing.Point(14, 93);
            this.ListVoucher.Name = "ListVoucher";
            this.ListVoucher.Size = new System.Drawing.Size(137, 483);
            this.ListVoucher.TabIndex = 10;
            this.ListVoucher.ValueMember = "VoucherID";
            this.ListVoucher.SelectedIndexChanged += new System.EventHandler(this.ListVoucher_SelectedIndexChanged);
            // 
            // sdsVouchers
            // 
            this.sdsVouchers.DataMember = "Vouchers";
            this.sdsVouchers.DataSource = this.dataSetAccounts;
            // 
            // dataSetAccounts
            // 
            this.dataSetAccounts.DataSetName = "DataSetAccounts";
            this.dataSetAccounts.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Window;
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.hdnguid);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.txtNarration);
            this.panel3.Controls.Add(this.lblDebitTotal);
            this.panel3.Controls.Add(this.lblCreditTotal);
            this.panel3.Controls.Add(this.grdviewParticulars);
            this.panel3.Location = new System.Drawing.Point(165, 59);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(957, 487);
            this.panel3.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(637, 458);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(322, 13);
            this.label12.TabIndex = 75;
            this.label12.Text = "---------------------------------------------------------------------------------" +
    "------------------------";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(637, 464);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(322, 13);
            this.label11.TabIndex = 73;
            this.label11.Text = "---------------------------------------------------------------------------------" +
    "------------------------";
            // 
            // hdnguid
            // 
            this.hdnguid.AutoSize = true;
            this.hdnguid.Location = new System.Drawing.Point(14, 466);
            this.hdnguid.Name = "hdnguid";
            this.hdnguid.Size = new System.Drawing.Size(41, 13);
            this.hdnguid.TabIndex = 72;
            this.hdnguid.Text = "label10";
            this.hdnguid.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(75, 448);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 16);
            this.label6.TabIndex = 69;
            this.label6.Text = ":";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 448);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 16);
            this.label9.TabIndex = 68;
            this.label9.Text = "Narration";
            // 
            // txtNarration
            // 
            this.txtNarration.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNarration.Location = new System.Drawing.Point(92, 450);
            this.txtNarration.Multiline = true;
            this.txtNarration.Name = "txtNarration";
            this.txtNarration.Size = new System.Drawing.Size(511, 34);
            this.txtNarration.TabIndex = 5;
            // 
            // lblDebitTotal
            // 
            this.lblDebitTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDebitTotal.Location = new System.Drawing.Point(637, 444);
            this.lblDebitTotal.Name = "lblDebitTotal";
            this.lblDebitTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDebitTotal.Size = new System.Drawing.Size(150, 15);
            this.lblDebitTotal.TabIndex = 4;
            this.lblDebitTotal.Text = "0.00";
            this.lblDebitTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCreditTotal
            // 
            this.lblCreditTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreditTotal.Location = new System.Drawing.Point(802, 444);
            this.lblCreditTotal.Name = "lblCreditTotal";
            this.lblCreditTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCreditTotal.Size = new System.Drawing.Size(150, 15);
            this.lblCreditTotal.TabIndex = 2;
            this.lblCreditTotal.Text = "0.00";
            this.lblCreditTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grdviewParticulars
            // 
            this.grdviewParticulars.AllowUserToResizeColumns = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.Desktop;
            this.grdviewParticulars.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grdviewParticulars.BackgroundColor = System.Drawing.SystemColors.Window;
            this.grdviewParticulars.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdviewParticulars.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.grdviewParticulars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdviewParticulars.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmnLedgers,
            this.clmnDebit,
            this.clmnCredit});
            this.grdviewParticulars.Cursor = System.Windows.Forms.Cursors.IBeam;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdviewParticulars.DefaultCellStyle = dataGridViewCellStyle6;
            this.grdviewParticulars.GridColor = System.Drawing.SystemColors.Window;
            this.grdviewParticulars.Location = new System.Drawing.Point(0, 3);
            this.grdviewParticulars.Name = "grdviewParticulars";
            this.grdviewParticulars.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(227)))), ((int)(((byte)(188)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdviewParticulars.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.grdviewParticulars.RowHeadersVisible = false;
            this.grdviewParticulars.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightSkyBlue;
            this.grdviewParticulars.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.grdviewParticulars.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdviewParticulars.Size = new System.Drawing.Size(957, 438);
            this.grdviewParticulars.TabIndex = 4;
            this.grdviewParticulars.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdviewParticulars_CellValueChanged);
            this.grdviewParticulars.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdviewParticulars_CurrentCellDirtyStateChanged);
            this.grdviewParticulars.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grdviewParticulars_EditingControlShowing);
            this.grdviewParticulars.Enter += new System.EventHandler(this.grdviewParticulars_Enter);
            // 
            // clmnLedgers
            // 
            this.clmnLedgers.AutoComplete = false;
            this.clmnLedgers.DataSource = this.sdsLedgers;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.clmnLedgers.DefaultCellStyle = dataGridViewCellStyle3;
            this.clmnLedgers.DisplayMember = "LedgerName";
            this.clmnLedgers.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.clmnLedgers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clmnLedgers.HeaderText = "Head of Accounts";
            this.clmnLedgers.Name = "clmnLedgers";
            this.clmnLedgers.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clmnLedgers.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.clmnLedgers.ValueMember = "LedgerID";
            this.clmnLedgers.Width = 657;
            // 
            // sdsLedgers
            // 
            this.sdsLedgers.DataMember = "Ledgers";
            this.sdsLedgers.DataSource = this.dataSetAccounts;
            // 
            // clmnDebit
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.clmnDebit.DefaultCellStyle = dataGridViewCellStyle4;
            this.clmnDebit.HeaderText = "Debit (Tk)";
            this.clmnDebit.Name = "clmnDebit";
            this.clmnDebit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clmnDebit.Width = 150;
            // 
            // clmnCredit
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.clmnCredit.DefaultCellStyle = dataGridViewCellStyle5;
            this.clmnCredit.HeaderText = "Credit (Tk)";
            this.clmnCredit.Name = "clmnCredit";
            this.clmnCredit.Width = 150;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.txtVType);
            this.panel2.Controls.Add(this.dtpPostingDate);
            this.panel2.Controls.Add(this.txtVNumber);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(165, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(957, 48);
            this.panel2.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(770, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 16);
            this.label5.TabIndex = 69;
            this.label5.Text = ":";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(442, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 16);
            this.label1.TabIndex = 68;
            this.label1.Text = ":";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(109, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(11, 16);
            this.label7.TabIndex = 67;
            this.label7.Text = ":";
            // 
            // txtVType
            // 
            this.txtVType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVType.Location = new System.Drawing.Point(127, 10);
            this.txtVType.Name = "txtVType";
            this.txtVType.ReadOnly = true;
            this.txtVType.Size = new System.Drawing.Size(144, 22);
            this.txtVType.TabIndex = 0;
            // 
            // dtpPostingDate
            // 
            this.dtpPostingDate.CustomFormat = "dd-MM-yyyy";
            this.dtpPostingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPostingDate.Location = new System.Drawing.Point(789, 11);
            this.dtpPostingDate.Name = "dtpPostingDate";
            this.dtpPostingDate.Size = new System.Drawing.Size(150, 20);
            this.dtpPostingDate.TabIndex = 3;
            // 
            // txtVNumber
            // 
            this.txtVNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVNumber.Location = new System.Drawing.Point(459, 10);
            this.txtVNumber.Name = "txtVNumber";
            this.txtVNumber.ReadOnly = true;
            this.txtVNumber.Size = new System.Drawing.Size(144, 22);
            this.txtVNumber.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(362, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Voucher No";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Voucher Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(731, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Date";
            // 
            // sdsVoucherItem
            // 
            this.sdsVoucherItem.DataMember = "Voucher_Contents";
            this.sdsVoucherItem.DataSource = this.dataSetAccounts;
            // 
            // Vouchers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(1141, 593);
            this.Controls.Add(this.panel1);
            this.KeyPreview = true;
            this.Name = "Vouchers";
            this.Text = "Vouchers";
            this.Load += new System.EventHandler(this.Vouchers_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdsVouchers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetAccounts)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdviewParticulars)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sdsLedgers)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sdsVoucherItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtVNumber;
        private System.Windows.Forms.DateTimePicker dtpPostingDate;
        private System.Windows.Forms.TextBox txtVType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView grdviewParticulars;
        private System.Windows.Forms.BindingSource sdsLedgers;
        private DataSetAccounts dataSetAccounts;
        private System.Windows.Forms.BindingSource sdsVoucherItem;
        private System.Windows.Forms.Label lblDebitTotal;
        private System.Windows.Forms.Label lblCreditTotal;
        private System.Windows.Forms.TextBox txtNarration;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label hdnguid;
        private System.Windows.Forms.ListBox ListVoucher;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtVSearch;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.BindingSource sdsVouchers;
        private System.Windows.Forms.DataGridViewComboBoxColumn clmnLedgers;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnDebit;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnCredit;
        private System.Windows.Forms.Label label8;
    }
}