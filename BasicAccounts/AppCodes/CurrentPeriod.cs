﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.AppCodes;

namespace BasicAccounts.AppCodes
{
    class CurrentPeriod
    {
        private int _ID;
        private DateTime _startDate;
        private DateTime _endDate;

        Validations validation = new Validations();

        public DateTime StartDate
        {
            get { return _startDate; }
            set { validation.RequiredValidation(value, "Start Date "); this._startDate = value; }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set { validation.RequiredValidation(value, "End Date "); this._endDate = value; }
        }
    }
}
