﻿using BasicAccounts.AppCodes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.AppCodes;
namespace BasicAccounts
{
    public partial class Nature : Form
    {
        static string connetionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        SqlConnection connection = new SqlConnection(connetionString);
        public Nature()
        {
            InitializeComponent();
        }

        private void Nature_Load(object sender, EventArgs e)
        {
            UpdateListView();
        }

        private void UpdateListView()
        {
            SqlDataAdapter cmd= new SqlDataAdapter("Select * from Natures", connection);
            DataSet natures = new System.Data.DataSet();
            cmd.Fill(natures, "Natures");
            DataTable dt = new DataTable();
            dt = natures.Tables[0];
            ListNatures.DataSource = dt;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Natures nature = new Natures();
                nature.NatureID = int.Parse(txtNatureID.Text);
                nature.Nature = txtNature.Text;
                nature.Status = chkNatureStatus.Checked;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string sql = null;
                sql = "Update  Natures set Nature=@Nature,Status=@Status where NatureID=@NatureID";

                connection.Open();
                adapter.InsertCommand = new SqlCommand(sql, connection);
                adapter.InsertCommand.Parameters.Add("@NatureID", SqlDbType.Int, 32);
                adapter.InsertCommand.Parameters["@NatureID"].Value = nature.NatureID;
                adapter.InsertCommand.Parameters.Add("@Nature", SqlDbType.NVarChar, 50);
                adapter.InsertCommand.Parameters["@Nature"].Value = nature.Nature;
                adapter.InsertCommand.Parameters.Add("@Status", SqlDbType.Bit);
                adapter.InsertCommand.Parameters["@Status"].Value = nature.Status;
                adapter.InsertCommand.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Information is Updated !! ");
                UpdateListView();
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                if (connection.State.ToString()=="open")
                {
                    connection.Close();
                }
                
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtNature.Text = "";
            txtNatureID.Text = "";
            chkNatureStatus.Checked = true;
        }

        private void ListNatures_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListNatures.SelectedItem != null)
            {
                Natures nature = new Natures();
                nature.NatureID = (Int32.Parse((ListNatures.SelectedItem as DataRowView)["NatureID"].ToString()));
                nature.Nature = (ListNatures.SelectedItem as DataRowView)["Nature"].ToString();
                nature.Status = (Boolean.Parse((ListNatures.SelectedItem as DataRowView)["Status"].ToString()));

                txtNature.Text = nature.Nature;
                txtNatureID.Text = nature.NatureID.ToString();
                chkNatureStatus.Checked = nature.Status;
            }

       }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Nature_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
               // this.windowsParent.RefreshLocationCombo();
            }

        }
    }
}
